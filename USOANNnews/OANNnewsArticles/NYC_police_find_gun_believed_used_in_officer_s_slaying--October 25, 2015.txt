Search for:
NYC police find gun believed used in officer�s slaying
A worker walks past a makeshift memorial in honor of slain New York City Police (NYPD) officer Randolph in the East Harlem neighborhood of Manhattan, in New York City October 25, 2015. REUTERS/Eduardo Munoz
October 25, 2015
By Laila Kearney
NEW YORK (Reuters) � New York City police divers have found a gun in the Harlem River off Manhattan authorities believe was used in the fatal shooting of an officer last week, police said on Sunday.
Tyrone Howard, 30, is accused of shooting Officer Randolph Holder, 33, in the head on a pedestrian walkway above a highway in East Harlem on Tuesday.
Investigators have been searching for the weapon used in the slaying of Holder, who became the fourth NYPD officer killed in the line of duty in the last 11 months.
The semiautomatic .40-caliber gun was found in the river at about 3 a.m. EDT early Sunday by divers searching the general area of the shooting, police said.
Police will test the weapon for DNA and prints and fire it for ballistic comparisons.
Howard has been charged with murder as well as robbery on suspicion of taking a man�s bicycle at gunpoint just before the shooting.
(Editing by Jeffrey Benkoe)
