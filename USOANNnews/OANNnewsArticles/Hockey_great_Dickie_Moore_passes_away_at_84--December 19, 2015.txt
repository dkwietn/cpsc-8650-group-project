Search for:
Hockey great Dickie Moore passes away at 84
Former Montreal Canadiens teammate Dickie Moore speaks at the funeral for former Montreal Canadiens captain Jean Beliveau at Mary Queen of the World Cathedral in Montreal, December 10, 2014. REUTERS/Paul Chiasson
December 19, 2015
(Reuters) � Hockey Hall of Fame member Dickie Moore, a six-time Stanley Cup winner and of the greatest National Hockey League players of all-time, passed away on Saturday, 18 days before his 85th birthday.
Inducted into the Hockey Hall of Fame in 1974, the Montreal Canadiens left wing twice won Art Ross Trophy as NHL leading scorer and won six Stanley Cups with Montreal, including five in a row from 1956.
�Dickie Moore was a player of great skill and even-greater heart, someone admired on the ice for his will to win and adored in the community for his commitment to good deeds,� NHL commissioner Gary Bettman said in statement.
Moore, a native of Montreal, won scoring titles in 1957-58 and 1958-59, when he set a league record with 96 points in 70 games.
Injuries forced Moore to retire after the 1962-63 season, but he returned to play 38 games for the 1964-65 Toronto Maple Leafs and 27 games for the 1967-68 St. Louis Blues. He helped the Blues advance to the Stanley Cup final against his old team, the Canadiens. In 14 NHL seasons, Moore had 261 goals and 608 points in 719 games. He also had 110 points in 135 playoff games.
The Hockey News magazine ranked Moore number 31 in its list of the 50 greatest hockey players in 1998.
In 2005, Montreal retired No. 12 for Moore as well as fellow Hall of Fame member Yvan Cournoyer.
(Writing by Tim Wharnsby in Toronto. Editing by Steve Keating.)
