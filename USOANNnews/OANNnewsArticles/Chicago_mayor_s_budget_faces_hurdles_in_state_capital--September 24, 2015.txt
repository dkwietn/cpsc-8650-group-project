Search for:
Chicago mayor�s budget faces hurdles in state capital
Chicago Mayor Rahm Emanuel arrives at a town hall meeting on the city budget in Chicago, Illinois, United States, August 31, 2015. REUTERS/Jim Young
September 24, 2015
By Dave McKinney
CHICAGO (Reuters) � Chicago Mayor Rahm Emanuel�s bid to hike property taxes by a record $543 million faces major hurdles in the state capital where the Illinois government is locked in its own budget battle for nearly four months.
Emanuel laid out a 2016 budget plan filled with tax hikes and budget cuts on Tuesday in an attempt to fix a financial crisis linked to the city�s unfunded pensions. The mayor is looking to�the Democrat-controlled legislature to approve his plan that would allow residents with homes worth less than $250,000 to avoid a higher tax.
The Chicago mayor is also hoping Illinois� Republican Governor Bruce Rauner will approve a lower pension payment schedule for the city�s public safety worker retirement systems in an effort to save $247 million next year.
But neither item seems likely to be approved any time soon.
Rauner has shown no signs that he is willing to back the mayor�s priorities. And the top Republican in the Illinois House voiced scepticism about the mayor�s wish list.
�The mayor knows full well the environment is a lot different here than it�s been in a long time. Everything that�s seemingly easy or makes sense can get caught up in this environment,� said House Republican Leader Jim Durkin, who represents a swatch of Chicago�s western suburbs. �It�s not going to be easy.�
According to the Cook County Clerk�s office, the city has until�Dec. 29 to put the first phase of the mayor�s tax hike on property tax bills.
Legislation that must take effect immediately�requires a three-fifths vote for passage before the end of the year, and the state House�s Democratic super majority has proven to be porous.
The second-term mayor has been working with Democrats behind the scenes to draft a plan to shield middle- and lower-income Chicago homeowners from any potential property tax hike. A House hearing dealing with the property tax exemption is scheduled for Thursday.
But any plan likely will run into resistance from Republicans, as well as Democrats, who do not like changes that benefit only Chicago.
�If it�s Chicago only, the roll call becomes exceptionally difficult,� said State Representative Michael Zalewski, a Democrat from southwest-suburban Chicago, who is vice-chairman of the House panel expected to discuss the property-tax plan.
Chicago faces a $745 million budget shortfall, driven largely by increasing pension costs. In his budget address on Tuesday, the mayor called for a massive property tax hike, a first-ever fee on residential garbage collection, new surcharges on ride-sharing and cab companies and a tax on e-tobacco products.
Chicago homeowners currently get a break on their tax bills that shields $7,000 of their property�s assessed valuation from taxation. Emanuel has not said how much that would rise under his plan, or if any relief would be expanded statewide.
Efforts to tinker with the state�s complicated property tax laws could ignite dissension from some Democrats, who have been pressing to overhaul the state�s school funding formula.
Andy Manar, an Illinois state senator who represents a mostly rural region, said any discussion about changing property tax laws must include school funding reforms. The two issues are �absolutely related, and they absolutely have to be part of the same conversation,� said Manar, a Democrat.
Chicago Alderman Scott Waguespack, a member of the city council�s small but vocal Progressive Caucus, acknowledged that Emanuel likely has the votes to win approval for his budget, but a political stalemate over state finances could eliminate the political cover Emanuel has sought for an expanded property tax exemption.
�I just can�t see how he (Rahm) can stand here without giving the aldermen specifics on how he�s going to accomplish that,� Waguespack said on Tuesday, following the mayor�s budget address.
Rauner�s silence on the issue further complicates Emanuel�s effort. Earlier this month, Rauner�s office linked any changes to state property tax laws with the governor�s demand for business-friendly reforms to state workers compensation, prevailing wage and collective-bargaining laws. �
A Rauner spokeswoman declined comment Wednesday on Emanuel�s budget or the mayor�s push for the bigger tax exemption.
Another initiative Emanuel sorely needs is pension-relief legislation that has passed both the House and Senate. Rauner has threatened to veto the bill, so legislative leaders have held it in place through a parliamentary maneuver to keep it from reaching the governor�s desk.
The pension measure would lower the city�s outlay to its underfunded police and fire pensions by $247 million in 2016 � and by $990 million through 2020 � while giving the city an additional 15 years for the pension systems to reach a 90-percent funding level.
(Reporting By Dave McKinney, additional reporting by Karen Pierog and Karl Plume in Chicago; Editing by Diane Craft)
