Search for:
Competency evaluation ordered for accused Planned Parenthood gunman
Robert Lewis Dear, 57, accused of shooting three people to death and wounding nine others at a Planned Parenthood clinic in Colorado last month, attends his hearing to face 179 counts of various criminal charges at an El Paso County court in Colorado Springs, Colorado December 9, 2015. REUTERS/Andy Cross/Pool
December 23, 2015
COLORADO SPRINGS, Colo. (Reuters) � The man accused of shooting three people to death and wounding nine others at a Planned Parenthood clinic in Colorado last month was ordered on Wednesday to undergo a competency evaluation to determine whether he is fit to act as his own lawyer.
El Paso County Judge Gilbert Martinez ordered the evaluation for Robert Lewis Dear, 57, after the defendant insisted during a court hearing that he distrusted his lawyers and wanted to represent himself.
(Reporting by Keith Coffman; Writing by Steve Gorman; Editing by Chris Reese)
