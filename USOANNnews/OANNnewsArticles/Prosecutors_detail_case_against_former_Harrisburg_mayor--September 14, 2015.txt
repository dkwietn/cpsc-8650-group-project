Search for:
Prosecutors detail case against former Harrisburg mayor
Former Harrisburg mayor Stephen Reed listens as his lawyer Henry E. Hockeimer, Jr. (L) speaks to press after the arraignment in Harrisburg, Pennsylvania July 14, 2015. REUTERS/David DeKok
September 14, 2015
By David DeKok
HARRISBURG, PA. (Reuters) � Pennsylvania state prosecutors on Monday unveiled details of the corruption case against Stephen Reed, the long-time former Harrisburg mayor charged with using city money from bond sales to buy thousands of artifacts.
In laying out the nearly 500 counts of theft and other charges, special agent Craig LeCadre told a court that investigators recovered 16,000 items in a storage facility and at Reed�s home. Both places were crammed full to the ceiling with artifacts, including a $14,000 suit of Spanish armor and a $2,500 knife carried at the Battle of the Little Bighorn, prosecutors said.
�It was like (the television show) �Hoarders,� on steroids,� said LeCadre, chief investigator on Reed�s case for the Pennsylvania Attorney General�s office.
At a preliminary hearing in the Dauphin County Courthouse, prosecutors must show Magisterial District Justice Richard Cashman that they have enough evidence to support the charges and move forward to the trial phase. The hearing continues on Tuesday.
Reed, 65, ended his 28-year tenure in 2010 with the state capital near financial ruin. He was charged in July on 499 criminal counts, although prosecutors reduced those counts to 487 on Monday.
Reed�s defense attorneys have asked the court to throw out 338 counts, arguing that they are barred by the statute of limitations. That hearing would proceed at a later date if the case continues.
The allegations stretch over a quarter century to numerous public agencies touched by Reed, including the parking authority, school district, civic baseball club and a local university.
Harrisburg filed for bankruptcy in 2011 just after Reed�s tenure ended. However, the case was thrown out and the city was put into receivership, which it exited last year. It is still under state financial oversight.
Prosecutors have said Reed is largely to blame for the city�s financial woes. On Monday, they showed more than 30 slides of individual artifacts that were supposed to belong to the city but were in Reed�s possession. He previously claimed some were destined for a Wild West Museum, which was never built.
Prosecutors also showed documents that followed the money, from bond issues through city authorities to a so-called �special projects fund,� which Reed used to pay artifact vendors.
Richard House, former City Council president, testified that Reed got him a job with the city�s minor league baseball team in exchange for winning votes in favor of controversial trash incinerator upgrades that left the city insolvent.
(Reporting by David DeKok in Harrisburg; Writing by Hilary Russ in New York; Editing by Richard Chang)
