Search for:
Oregon wildlife managers vote to take wolves off endangered list
A grey wolf is caught on a remote camera in the Siskiyou National Forest in Oregon, in this undated Oregon Fish & Wildlife handout photo. REUTERS/Oregon Fish & Wildlife/Handout/File
November 10, 2015
By Shelby Sebens
PORTLAND, Ore. (Reuters) � Oregon�s population of about 81 gray wolves will soon lose its status as a state-protected endangered species after a decision by state wildlife managers, who came under fire from animal advocates who said politics had won over sound science.
The Oregon Fish and Wildlife Commission voted 4-2 late on Monday to remove the wolves from the list, saying they had recovered in sufficiently healthy numbers that they no longer faced the threat of extinction.
The action was to take effect after documentation to de-list the wolves was submitted to the Oregon secretary of state for review on Tuesday, commission officials said.
�Ultimately, the Commission voted to side with political expediency over sound public and scientific process,� Oregon Wild Executive Director Sean Stevens said.
�While this is certainly a blow for our tiny population of 81 gray wolves and for the legitimacy of wildlife management in Oregon it is not the end of the struggle,� Stevens said.
Oregon wildlife biologists recommended last month that the state remove gray wolves from its list of Endangered Species.
Gray wolves, native to Oregon, were wiped out in the state by an eradication campaign in the early 20th century. They first returned in 2008 and have spread to several parts of the Pacific Northwest state.�
The commission�s action covers all wolves in the state except a portion of the wolves on the west side of Oregon that are still protected under the U.S. Endangered Species Act.
It is estimated that about 81 wolves in 16 packs roam the wilds of Oregon.
In February, gray wolves were returned to endangered or threatened species lists in Minnesota, Wisconsin, Michigan and Wyoming after court orders reversed their delistings from 2012.
Delisting gray wolves in Oregon may usher in wolf hunting, now illegal, even though the wildlife commission also voted on Monday to ask lawmakers to increase penalties for killing a wolf. Currently fines can go up to $6,250 and one year in jail.
Under a phase of the state�s wolf-management plan that is likely to begin in January 2017, the state may sanction a limited wolf hunt in areas where they are deemed to be killing a large number of deer and elk, the commission said.�
More than 100 wildlife advocates and eastern Oregon ranchers packed the all day meeting on Monday to speak on opposite sides of the issue, Commission Chairman Michael Finley said in a statement.
(Reporting by Shelby Sebens in Portland; Editing by Eric M. Johnson, Toni Reinhold)
