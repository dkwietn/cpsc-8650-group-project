Search for:
Appeal in Baltimore officer�s murder trial to be heard in March
Caesar Goodson arrives at the courthouse for the first day of jury selection in Baltimore, Maryland, January 11, 2016. REUTERS/Jose Luis Magana/Pool
January 12, 2016
BALTIMORE (Reuters) � A Maryland appeals court on Tuesday set March 4 for arguments over whether a Baltimore police officer charged in the death of a black detainee must testify against a colleague accused of murder in the same incident.
The scheduling by the Maryland Court of Special Appeals could disrupt the months-long timetable for six Baltimore officers facing trial for the death of Freddie Gray in April. His death from a broken neck suffered in a police van triggered protests and rioting and fed a U.S. debate on race and policing.
The appeals court on Monday ordered a delay in the trial of Officer Caesar Goodson Jr., the van�s driver, while it determined whether Officer William Porter should be compelled to testify against him and Sergeant Alicia White.
Goodson, 46, is charged with second-degree depraved heart murder, the most serious accusation raised in Gray�s death. Porter�s trial on involuntary manslaughter and other charges ended in a hung jury last month, and a retrial is set for June.
Prosecutors want Porter as a witness against Goodson and White. Porter�s lawyers appealed Baltimore City Circuit Court Judge Barry Williams� ruling that forced Porter to testify since he had been offered immunity from prosecution for what he might say on the stand.
Goodson�s trial had been scheduled to begin on Monday, and White�s is set to start on Feb. 8.
With Baltimore police under heightened scrutiny, Police Commissioner Kevin Davis said on Tuesday the department would restructure its internal affairs procedures to make them more transparent. Changes will include public viewing of disciplinary hearings via live video streaming.
The greater public access is in line with recommendations from a state legislative task force on police reforms set up after Gray�s death.
The group completed its report on Monday, the Baltimore Sun reported. The recommendations include more rights for victims of police brutality and trimming of special rights for officers.
(Reporting by Donna Owens in Baltimore and Ian Simpson in Washington; Editing by Cynthia Osterman and David Gregorio)
