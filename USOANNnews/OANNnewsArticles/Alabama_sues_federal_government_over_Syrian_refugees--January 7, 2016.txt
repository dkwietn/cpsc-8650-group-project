Search for:
Alabama sues federal government over Syrian refugees
Alabama Governor Robert Bentley speaks during a news conference in Mobile, Alabama July 2, 2012. REUTERS/Jonathan Bachman
January 7, 2016
By Letitia Stein
(Reuters) � Alabama officials sued the U.S. government on Thursday to force the Obama administration to provide more information on the settlement of refugees from Syria and other countries in the state.
The lawsuit, filed in U.S. district court in Birmingham, accused the Obama administration of failing to consult state officials about any refugees to be settled in the state, in violation of the federal Refugee Act of 1980.
Republican Governor Robert Bentley is among more than two dozen U.S. governors who have sought to block Syrian refugees from the state after the Nov. 13 attacks in Paris for which the group Islamic State claimed responsibility.
The lawsuit follows a legal challenge filed by Texas, another Republican-led state, to stop the immediate entry of nine Syrian refugees. A judge last month dismissed the state�s request for a restraining order, calling the evidence presented �largely speculative hearsay.�
Bentley said the White House has not answered three letters that he sent seeking information about plans to place refugees in Alabama.
�The process and manner in which the Obama administration and the federal government are executing the Refugee Reception Program is blatantly excluding the states,� he said in a statement.
The lawsuit, which names the heads of multiple federal agencies as defendants, seeks to force the government to disclose information on each refugee, including medical history, and to certify that the individual does not pose a security risk.
In the Texas case, the Justice Department said the refugee act requires the government to consult regularly with states about the sponsorship process and distribution among states but that it is not obligated to discuss individual resettlements in advance.
Alabama is intensifying its fight after Republican Governor Nathan Deal in neighboring Georgia on Monday rescinded his own executive order seeking to stop the resettlement of Syrian refugees, because the state�s attorney general ruled he lacked the authority to do so.
The U.S. Department of Justice declined to comment on the Alabama case.
The Southern Poverty Law Center said Bentley lacks the authority to bar the resettlement of refugees, and added that his �grandstanding is fueling xenophobia and helping to create an environment ripe for hate and violence.�
(Reporting by Letitia Stein in Tampa, Florida; editing by Richard Chang and Steve Orlofsky)
