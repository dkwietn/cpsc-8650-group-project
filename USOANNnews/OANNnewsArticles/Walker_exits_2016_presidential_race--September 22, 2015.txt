Search for:
Walker exits 2016 presidential race
U.S. Republican presidential candidate Scott Walker speaks at the the Iowa Faith and Freedom Coalition Forum in Des Moines, Iowa, September 19, 2015. REUTERS/Brian C. Frank
September 22, 2015
By Steve Holland, Ginger Gibson and Erin McPike
WASHINGTON/NEW YORK (Reuters) � Republican Governor Scott Walker of Wisconsin abruptly pulled out of the 2016 U.S. presidential race on Monday, doomed by a lightning-quick collapse from serious contender to also-ran candidate struggling to raise money.
Walker�s departure left his rivals seeking the Republican nomination for the November 2016 presidential election scrambling to appeal to his supporters in the early-voting states of Iowa and New Hampshire as well as elsewhere.
Experts saw potential gain for conservative candidates like Ted Cruz and Mike Huckabee as well as center-right contenders such as Marco Rubio, John Kasich and Jeb Bush.
For Walker, a shaky performance on the campaign trail plus two lackluster performances at Republican debates spooked financial donors. His staff disagreed about how the 47-year-old Walker could regain the star status he held after electrifying conservatives in Iowa in January.
�He started with great expectations and he didn�t meet �em,� said Iowa Republican strategist Doug Gross. �You go down pretty fast when that happens.�
Walker�s departure reflected the difficulty faced by the 15-candidate field in attracting enough financial support from a limited pool of donors. Other low-polling Republicans may well pull out in the weeks ahead.
Walker�s campaign has been in trouble for weeks, but his departure was still a surprise, although there had been talk of a shakeup of his team.
The campaign sought to renew its focus on Iowa given its proximity to Wisconsin, but a CNN/ORC poll on Sunday showed the magnitude of his problems.
Walker had less than 1 percent support, a dramatic slide from one of the front-runners when he announced his campaign in early July.
Some Republicans said that by exiting quickly, four months before Iowa holds the first nominating contest of the 2016 election season, Walker showed a maturity that may help him salvage his reputation should he decide to run again in the future.
�Today, I believe I am being called to lead by helping to clear the race so that a positive conservative message can rise to the top of the field. With that in mind, I will suspend my campaign immediately,� Walker said.
Walker appeared to direct scorn at tough-talking New York billionaire Donald Trump, the party�s front-runner who now leads in Iowa, where Walker had staked a claim. He said the party�s debate has �drifted into personal attacks.�
�In the end I believe that the voters want to be for something and not against someone. Instead of talking about how bad things are, we want to hear how they can make them better for everyone. We need to get back to the basics of our party,� he said.
He called on some of his rivals for the Republican nomination to join him in exiting the race to give voters a chance to rally around a front-runner that can win the election.
SPLIT AMONG TEAM
There were internal disputes within Walker�s team about how to best revive his campaign as he slipped in the polls.
Sources close to the campaign described a division between those who worked with Walker on previous campaigns and had strong Wisconsin ties and those whom he brought in from Washington. Campaign manager Rick Wiley was the target of animosity among Walker�s close advisers.
Others argued that Walker was not being aggressive enough, falling into a stereotype of Midwestern politeness, and that he needed to take a stronger approach to opponents like Trump.
Walker�s lack of experience on the national stage was apparent. He gave shifting answers to questions about illegal immigration and once suggested a wall between the United States and close ally Canada might be in order, in an apparent effort to double down on rivals� calls for a wall on the Mexican border.
Grocery store magnate John Catsimatidis, a billionaire in New York who donated to Walker, said Walker made many mistakes.
�I think the priority is watching out for our southern border and he brought up the Canadian border, and that�s not a priority,� he said.
Several Republican rivals sent complimentary messages to Walker, some in the hope of gaining the support of his backers.
�I know many people are disappointed with Scott�s announcement and I respect what a difficult decision it must have been. He remains one of the best governors in the country,� said Rubio.
Who benefits from Walker�s exit was a subject of debate within Republican circles with no clear answer, since Walker had appealed to both Christian conservatives and establishment Republicans.
Laurence Goldfarb, the owner of a Great Neck, N.Y. commodities company, said there are two candidates who could attract his donations now that Walker has exited the race � Rubio and Carly Fiorina.
Goldfarb said Walker had been unable to break through such a large field. �You�ve got one debate a month. You�ve got to stand out,� he said.
Walker was the second Republican to drop out of the race. He follows former Texas Governor Rick Perry, who dropped out on Sept. 11.
For more on the 2016 presidential race, see the Reuters blog, �Tales from the Trail� (http://blogs.reuters.com/talesfromthetrail/).
(Additional reporting by Brendan O�Brien in Madison, Emily Flitter in New York and Alana Wise in Washington; Editing by Jonathan Oatis and Cynthia Osterman)
