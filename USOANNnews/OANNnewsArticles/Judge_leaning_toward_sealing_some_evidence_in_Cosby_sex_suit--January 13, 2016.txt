Search for:
Judge leaning toward sealing some evidence in Cosby sex suit
Actor and comedian Bill Cosby (C) arrives for his arraignment on sexual assault charges at the Montgomery County Courthouse in Elkins Park, Pennsylvania December 30, 2015. REUTERS/Mark Makela
January 13, 2016
By Scott Malone
(Reuters) � A U.S. judge on Wednesday brokered a compromise on whether to release evidence in a defamation suit brought against comedian Bill Cosby by seven women who accuse him of sexually assaulting them.
Cosby�s attorneys wanted to seal all evidence in the case for at least 14 days. This would likely include depositions by Cosby and his wife Camille on topics covering sexual matters.
U.S. Magistrate Judge David Hennessy rejected their bid. Instead, he said he was inclined to give each side up to seven days to make the case for whether individual pieces of testimony should be sealed.
Tamara Green sued Cosby in December 2014, accusing him of lying when he publicly denied having sexually assaulted her, and six other women have since joined in the lawsuit. Cosby, 78, filed a countersuit in U.S. District Court in Massachusetts last month, accusing the women of defaming him.
The seven are among more than 50 women who have come forward to publicly accuse Cosby of sexually assaulting them after plying them with drugs or alcohol in alleged attacks carried out over a number of decades.
Hennessy said that once he had received a request for deposition evidence to be sealed, the other side will have up to seven days to challenge.
�Traditionally things like pretrial depositions have not been conducted in public,� Hennessy said. �There is the potential for abuse, damaging reputations, revealing private information about people.�
Joseph Cammarata, an attorney for the women, acknowledged that depositions of witnesses including Cosby and his wife, Camille, would delve into personal matters.
�Are we going to talk about the sexual history? Of course. Are we going to talk about the extramarital activity? Yes,� Cammarata said. �We already know these subject matters.�
The accusations toppled Cosby from his pedestal as one of America�s most-admired comedians, who built a long career on family-friendly humor. He was best known for his role as the dad Heathcliff Huxtable in the long-running 1980s television hit, �The Cosby Show.�
Cosby has repeatedly denied wrongdoing. Last month prosecutors in Pennsylvania charged him with a 2004 sexual assault days before the statute of limitations on that alleged crime was to expire.
One of Cosby�s attorneys, Marshall Searcy, noted that those pending criminal charges may well influence how much Cosby says in any deposition.
Cosby�s right to avoid self-incrimination under the Fifth Amendment of the U.S. Constitution will �really come into play,� Searcy said.
Cosby, who was not present at the hearing at federal court in Worcester, Massachusetts, faces several additional civil suits.
