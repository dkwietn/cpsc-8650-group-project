Search for:
Bid to repeal California school vaccination law falls short
A measles vaccine is seen at Venice Family Clinic in Los Angeles, California February 5, 2015. REUTERS/Lucy Nicholson
October 9, 2015
By Sharon Bernstein
SACRAMENTO, Calif. (Reuters) � A plan to ask voters to repeal a new California law tightening vaccination requirements for school-age children has fallen short of signatures needed to put the referendum on the ballot, state data showed on Thursday.
The effort was part of a backlash against a bill signed into law in June by Democratic Governor Jerry Brown that requires pupils to be vaccinated against childhood diseases unless they have a medical reason to refuse. It was passed after a measles outbreak among unvaccinated people at Disneyland last year.
That law, which goes into effect next year, makes California the third state to eliminate religious and other personal exemptions to vaccinations.
Opponents of the new rules, some fearing a long-debunked link between vaccines and autism, and others opposed to the state�s removal of a religious exemption for parents who want to opt out of vaccination, vowed to take the issue to voters.
But a report posted Thursday on the website of Secretary of State Alex Padilla showed the effort had garnered only about 234,000 signatures, well short of the 365,880 signatures needed for a measure to make the November 2016 ballot.
Supporters of the initiative turned in the signatures they had gathered at the county level last week. Thursday was the deadline the state to provide an official count.
Tim Donnelly, a former Republican state assembly member who had spearheaded the effort, did not immediately respond to a request for comment on Thursday.
When it appeared that the measure would fall short last week, he complained in a statement sent to reporters that special interests, including pharmaceutical companies, had �gone to great lengths to thwart campaign efforts.�
State Senator Richard Pan, a pediatrician who faced intense opposition as the author of the new law eliminating the personal beliefs exemption for childhood vaccinations, including death threats and a possible recall effort, welcomed the ballot initiative�s stumble.
�This is a major win for public health as California leads the country in rejecting the unfounded fear and misinformation about vaccines that has put too many people at risk for serious disease,� Pan said.
(Editing by Eric Walsh and Ken Wills)
