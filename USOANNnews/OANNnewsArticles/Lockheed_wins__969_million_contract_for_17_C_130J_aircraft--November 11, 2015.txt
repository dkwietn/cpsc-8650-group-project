Search for:
Lockheed wins $969 million contract for 17 C-130J aircraft
Marillyn Hewson, Chairman, President and CEO of Lockheed Martin, participates in a panel discussion at the 2015 Fortune Global Forum in San Francisco, California November 3, 2015. REUTERS/Elijah Nouvelage
November 11, 2015
WASHINGTON (Reuters) � Lockheed Martin Corp has been awarded a contract worth nearly $969 million to build 17 C-130J military transport aircraft, the Pentagon said on Tuesday.
The work is expected to be completed by April 2020, the Department of Defense said in its daily digest of major contract awards.
Last month, Lockheed announced that it had reached a verbal agreement with the U.S. Air Force for a five-year contract to build up to 83 C-130 J Super Hercules transport planes for the Air Force, Coast Guard and Marine Corps through 2020.
Lockheed has sold C-130J transport planes to 16 countries, and 330 of the turboprop planes have been delivered or are currently on order.
The planes, which are able to touch down on austere landing zones � essentially makeshift runways � are often used for humanitarian relief missions, special operations, aerial refueling, close air support, and search and rescue.
(Reporting by Idrees Ali; Editing by Bill Rigby)
