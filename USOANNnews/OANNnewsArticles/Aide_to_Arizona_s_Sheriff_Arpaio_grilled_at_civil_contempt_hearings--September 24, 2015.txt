Search for:
Aide to Arizona�s Sheriff Arpaio grilled at civil contempt hearings
Maricopa County Sheriff Joe Arpaio addresses the media in Fountain Hills, Arizona, February 9, 2013. REUTERS/Darryl Webb
September 24, 2015
By David Schwartz
PHOENIX (Reuters) � The top aide to Arizona�s controversial lawman Joe Arpaio testified on Thursday that he had been unaware of a federal judge�s ruling in a racial-profiling case that barred deputies from detaining people they suspected of being illegal immigrants.
Arpaio, 83, the six-term sheriff of Maricopa County, his chief deputy Gerard Sheridan and three other aides appeared in federal court in Phoenix for a second round of civil contempt hearings before U.S. District Court Judge Murray Snow.
Under intense questioning from plaintiffs� attorney Cecillia Wang, Sheridan said he could not remember reading memos he was sent, including several from the lawyer representing the sheriff�s office, nor attending meetings regarding the preliminary injunction Snow issued in 2011.
�It is difficult for me to be specific on what I recall,� Sheridan testified.
Arpaio�s deputies continued to detain people solely based on the suspicion that they were in the United States illegally for 18 months after the injunction was issued, according to court papers.
Sheridan and Arpaio, who bills himself as �America�s Toughest Sheriff� and is known for his stance on illegal immigrants, already have admitted committing civil contempt by violating court orders but have said it was not deliberate.
Snow has said he will use the testimony to help decide what to do about non-compliance with his orders, which could include imposing fines, restitution and greater oversight of operations conducted by the sheriff�s office.
Legal experts say the proceedings could take a more serious turn if Snow determines the defendants� misconduct warrants referring the case to prosecutors.
That could open the door for Arpaio, who is running for re-election next year, and the others to face criminal charges.
Snow, who has already found that Arpaio�s office racially profiled Latino drivers and wrongfully detained them, convened the first round of civil contempt hearings in April after growing frustrated with the conduct of sheriff�s officials.
In addition to being accused of violating the 2011 injunction, the officials also are alleged to have failed to turn over evidence to plaintiffs� lawyers.
Arpaio, who was in court on Thursday, declined a request from a reporter to comment on the proceedings.
(Reporting by David Schwartz; Editing by Daniel Wallis and Bill Trott)
