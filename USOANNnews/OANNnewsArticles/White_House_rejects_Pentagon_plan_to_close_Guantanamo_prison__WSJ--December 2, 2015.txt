Search for:
White House rejects Pentagon plan to close Guantanamo prison: WSJ
A U.S. Marine guard tower overlooks the Northeast gate leading into Cuba territory at Guantanamo Bay U.S. Naval Base, March 8, 2013. REUTERS/Bob Strong
December 2, 2015
WASHINGTON (Reuters) � The Obama administration has rejected as too expensive a Department of Defense cost estimate for closing the prison at Guantanamo Bay, Cuba, and building a replacement, and it has asked for revisions, the Wall Street Journal reported on Tuesday, citing officials familiar with the plan.
The Pentagon estimate for closing the prison and building another one in the United States was as high as $600 million, including $350 million in construction costs, the newspaper cited the officials as saying.
(Reporting by Eric Walsh; Editing by Sandra Maler)
