Search for:
U.S. to seek detention of billionaire Macau real estate developer�s aide
Jeff Yin, the assistant to Macau billionaire Ng Lap Seng, is shown in this still image captured from from a video interrogation entered as an exhibit to a court filing by United States Attorney's Office Southern District of New York on September 25, 2015. REUTERS/United States Attorney's Office Southern District of New York/Handout
September 28, 2015
By Nate Raymond
NEW YORK (Reuters) � U.S. prosecutors will go to court on Tuesday to argue that a billionaire Macau developer�s aide presents too much of a flight risk to be released on bail after he was arrested on charges of lying about why he and his boss brought $4.5 million into the United States.
Jeff Yin, assistant to real estate developer Ng Lap Seng, was arrested with his boss on Sept. 19. Yin was set to be released on a $1 million bond and placed under house arrest at his mother�s California home.
But late on Friday, U.S. Magistrate Judge Sarah Netburn delayed Yin�s release until a hearing on Tuesday. Netburn cited a letter from prosecutors contending Yin �lied repeatedly� about his access to passports in order to flee the country.
Prosecutors said authorities discovered a Chinese passport in Yin�s travel bag. He had claimed to have only a U.S. passport, and prosecutors said the undisclosed passport, along with $15,000 and credit cards in the bag, demonstrate that Yin intended to flee the country.
The prosecutors, who belong to Manhattan U.S. Attorney Preet Bharara�s public corruption unit, said their investigation is ongoing and may result in additional charges.
Yin�s lawyer, Sabrina Shroff, declined comment.
Ng, who heads Macau-based Sun Kian Yip Group, earned much of his wealth on developments in Chinese territory. U.S. prosecutors estimate his fortune at $1.8 billion.
Ng, 68, also sits on the Chinese People�s Political Consultative Conference, an adviser to the government.
He and Yin, 29, were arrested on Sept. 19 for falsely claiming $4.5 million in cash they brought into the United States from China between 2013 to 2015 was meant to buy art, antiques or real estate, or be used for gambling.
Prosecutor Daniel Richenthal at a recent hearing said Yin after his arrest revealed money was used to pay people �to engage in unlawful activities.� He did not specify what unlawful activities they are suspected of.
Shroff has disputed that account. Ng�s attorney has called the case a �misunderstanding.�
Ng�s name previously surfaced in U.S. investigations into how foreign money might have been funneled into the Democratic National Committee before the 1996 elections, when it was working to re-elect President Bill Clinton. He was never charged.
In 2014, Ng was subpoenaed in a foreign bribery investigation, a source told Reuters on Friday. The source said the probe came after Ng�s name surfaced in a lawsuit against billionaire Sheldon Adelson�s Las Vegas Sands Corp.
The case is U.S. v. Seng, U.S. District Court, Southern District of New York, No. 15-mj-03369.
(Reporting by Nate Raymond in New York; Editing by David Gregorio)
