Search for:
U.S. to curb smog but stops short of toughest limits
Smoke is released into the sky at a refinery in Wilmington, California March 24, 2012. REUTERS/Bret Hartman
October 1, 2015
By Patrick Rucker
WASHINGTON (Reuters) � The Obama administration on Thursday trimmed the amount of ozone allowed in the air, issuing a regulation to fight smog that will prevent hundreds of thousands of asthma attacks but cost businesses and utilities billions of dollars.
The Environmental Protection Agency set a new standard of 70 parts per billion (ppb) for the amount of ground-level ozone allowed, from the current level of 75 ppb set under former President George W. Bush in 2008.
Ground-level ozone is a main component of smog. The cut will prevent 320,000 childhood asthma attacks a year, the EPA says.
But the new limit, envisioned under the Clean Air Act, is the least restrictive that the agency had been considering, and health experts complained it does not go far enough.
EPA head Gina McCarthy said she had tried to be guided by science in an effort to protect Americans� health.
�I did the best with what I have,� she told reporters.
It was President Barack Obama�s second major initiative in less than two months to protect the environment, after the White House announced a sweeping plan in August to reduce carbon emissions from power plants.
Industry will face costs of $3.9 billion under Thursday�s rules, the agency has estimated.
Business groups say stringent ozone rules will harm the economy by forcing manufacturers and utilities to buy expensive new �scrubbers� and other technology to make sure their plants reduce emissions of toxins.
States will have years to work with power plants, factories and refineries to limit pollutants like nitrogen oxide and volatile organic compounds, components of smog.
The American Chemistry Council, a Washington-based lobbying group, predicted that the rule will increase business uncertainty.
�Today�s action puts $10 billion in chemical industry investment at risk,� it said in a statement. We are very concerned that some projects � new facilities, plant expansions and factory restarts � will remain in limbo until EPA explains how to obtain a permit under the new standards.�
The EPA says the new standards will cut lung ailments and other respiratory illnesses, as well as cardiovascular problems.
LONG CLEAN AIR FIGHT
Obama has long struggled to set tighter smog pollution standards in the face of opposition both from Republicans and businesses, as well as some Democrats and labor unions worried about the impact on jobs.
In 2011, he withdrew a plan to cut smog, citing a U.S. economy that was recovering too slowly from recession.
The EPA had been considering a new range of 65 to 70 ppb before settling on the high number announced on Thursday. The lower cap would have cost industry about $11 billion more than a 70-ppb limit but also prevent 960,000 childhood asthma attacks a year, according to the agency.
The American Academy of Pediatrics called for a limit of 60 ppb to protect the health of children, especially those who suffer from asthma.
Cleaning the air that America breathes has been a long fight.
Washington�s power to control air pollution rests in the Clean Air Act of 1963, which has been expanded over the decades to curtail health-damaging emissions from tailpipes and smokestacks and even limit invisible greenhouse gases blamed for climate change.
The legislation has been an effective tool for curtailing ground-level ozone, with those levels declining 18 percent between 2000 and 2013.
Driving air pollution levels even lower could be more costly and difficult, particularly in areas where forest fires cause spikes in ozone.
Senator Barbara Boxer, a California Democrat, called for stricter regulations.
�Today�s action is a step in the right direction, but I believe following the science is important and I am disappointed that a more protective standard was not set,� she said.
(Reporting by Valerie Volcovici, Timothy Gardner and Patrick Rucker; Editing by Alistair Bell and Jonathan Oatis)
