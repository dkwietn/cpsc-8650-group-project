Search for:
Quebec key to cutting New England power costs: Maine governor
Maine Governor Paul LePage (L) stands with Quebec Premier Philippe Couillard at the 23rd Annual Energy Trade & Technology Conference in Boston, Massachusetts, November 13, 2015. REUTERS/Gretchen Ertl
November 13, 2015
By Richard Valdmanis
BOSTON (Reuters) � Maine Governor Paul LePage on Friday said building high-voltage transmission lines from eastern Canada will be key to helping New England cut its soaring electricity costs, which he said had driven out businesses and cost the region jobs.
�There�s a better future for New England if we open the door to Quebec,� he said at the New England-Canada Business Council energy forum in Boston. �The new horizon is in partnering with Canada, our family to the North, and to make things happen.�
New England has the highest electricity prices in the continental United States, according to the U.S. Department of Energy, a situation that has worsened after the closure of nuclear and coal-fueled power plants in recent years.
Quebec, meanwhile, is the fourth largest hydroelectric power producer in the world. The province supplies about 10 percent of New England�s power needs but is keen to sign bigger, long-term contracts with regional utilities to sate demand.
�HydroQuebec is a natural partner for supplying clean renewable energy to New England,� said Quebec Premier Philippe Couillard in a speech that followed LePage. Couillard met with both LePage and Massachusetts Governor Charlie Baker on the sidelines of the conference.
New England governors have long supported the idea of buying more hydropower from Canada to ease prices and reduce carbon emission, but billions of dollars worth of proposals to build transmission lines have faced local and legislative opposition. Critics say building transmission lines into the region could ruin the aesthetics of rural areas, and stymie local renewable energy efforts.
Quebec�s HydroQuebec power company and New England-based utility Eversource Energy have proposed project called Northern Pass that would bring 1,090 megawatts into New Hampshire, where it would enter the regional grid. Other proposals include lines through Vermont and through Maine, with power either coming from Quebec, or Labrador�s Muskrat Falls project.
Power prices in New England are also high because of a pipeline bottleneck that has limited the supplies of low-cost natural gas from the huge Marcellus shale reserve under Pennsylvania, Ohio and West Virginia.
The New England Coalition for Affordable Energy estimates that overall infrastructure constraints have cost power consumers in the region about $7.5 billion in the past three years. LePage said Maine had been hard hit.
�The state of Maine has lowered its emissions by 30 percent,� he said. �We didn�t do anything. That�s why we lowered emissions: we lost our industrial base.�
(Writing by Richard Valdmanis; Editing by Bill Trott and Grant McCool)
