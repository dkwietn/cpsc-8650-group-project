Search for:
Arizona sheriff denies investigating racial profiling case judge
Maricopa County Sheriff Joe Arpaio addresses the media about a simulated school shooting in Fountain Hills, Arizona, February 9, 2013. REUTERS/Darryl Webb
October 3, 2015
By David Schwartz
PHOENIX (Reuters) � Arizona�s controversial lawman, Joe Arpaio, took the witness stand on Friday during his civil contempt hearing and denied that his office used a confidential informant to investigate the judge presiding over his racial profiling case.
��� During questioning, Arpaio said the probe had stemmed from the informant�s allegations that the federal government had cracked into about 150,000 local bank accounts.
He repeatedly denied his office�s investigation was focused on U.S. District Court Judge Murray Snow, who is overseeing his long-running racial profiling case. The judge was alleged to have had one of those accounts.
�� �I wanted to get to the bottom of this massive infiltration,� Arpaio said during the hearing in U.S. District Court in Phoenix.
The all-day testimony was part of the contempt proceedings against Arpaio and four aides launched by Snow, for violating his orders in a 2007 racial profiling case.
��� Arpaio, the six-term sheriff of Maricopa County, and his chief deputy acknowledge the non-compliance but have said it was unintentional.
��� On Friday, the lawman told plaintiff�s attorney Stanley Young he had been concerned about the informants� allegations that the telephones of Arpaio and his lawyers were tapped, and their emails hacked.
Arpaio and his office face a range of potential sanctions from the judge, including fines, restitution for those harmed by the actions and tighter oversight of daily operations.
��� Arpaio, who has announced he is seeking a seventh term, could also face criminal charges stemming from the violations.
��� His testimony came on the sixth day of the hearings, as part of a lawsuit by Latinos alleging they were racially profiled by deputies during traffic stops.
��� In 2013, Snow found Arpaio�s deputies were guilty of such profiling and unlawfully detaining Latinos, violating their constitutional rights, in a ruling that was a major blow to the self-proclaimed �America�s Toughest Sheriff.�
��� Snow installed a court monitor at the sheriff�s office and ordered other changes to ensure the offenses were not repeated.
��� Snow called for the contempt hearings, which began in April, in response to repeated non-compliance by the sheriff�s office.
����The proceedings were broadened to cover the confidential informant investigation, questions about nearly 1,500 identifications seized by deputies and alleged inadequate internal investigations by sheriff�s officials.
(Reporting by David Schwartz; Editing by Victoria Cavaliere and Clarence Fernandez)
