Search for:
Jury starts deliberations over Baltimore officer charged in man�s death
Baltimore Police officer William Porter approaches the court House in Baltimore, Maryland, November 30, 2015. REUTERS/Patrick Samansky/Pool
December 15, 2015
By Ian Simpson
BALTIMORE (Reuters) � A Baltimore jury on Monday began weighing the fate of a police officer charged in the death of a black detainee that triggered rioting and intensified a U.S. debate on police tactics.
The seven-woman, five-man jury started deliberations in the case of Officer William Porter, 26, after almost two weeks of testimony in Baltimore City Circuit Court. Porter faces involuntary manslaughter and other charges in Freddie Gray�s death from a broken neck suffered in the back of a police van.
About two hours after starting deliberations, jurors sent Judge Barry Williams notes asking for transcripts of radio traffic on April 12, the day Gray was arrested, and of Porter�s statement to investigators on April 17.
They also asked for definitions of �evil motive, bad faith and not honestly,� which is part of a misconduct charge.
In closing arguments, prosecutor Janice Bledsoe said Porter could have prevented Gray�s death by buckling his seat belt and getting medical aid when Gray asked for it. Porter, who is black, is the first of six officers to face trial.
�Click � how long does it take to click a seat belt and click a radio and ask for a medic? Two seconds? Three seconds? Maybe four?� Bledsoe asked jurors, holding a seat belt in her hands.
�Is two, three or maybe four seconds worth a life?�
But defense lawyer Joseph Murtha said Porter had acted as any reasonable officer would have and prosecutors had played on city residents� fears in bringing the charges.
Murtha said that medical experts for both sides had disputed at what point Gray suffered his injury. Officers could not be expected to call for medical assistance every time a detainee wanted help, he said.
�You are judging Officer Porter from a reasonable officer standard,� he said.
Gray�s death in April triggered rioting, arson and protests in the majority black city. It fueled a U.S. debate on police tactics and law enforcement treatment of minorities.
Porter also faces charges of second-degree assault, reckless endangerment and misconduct in office. The charges against the other officers range from second-degree murder to misconduct.
Gray, 25, was arrested after fleeing from police. He was put in a transport van, shackled and handcuffed, but he was not secured by a seat belt despite department policy to do so.
Bledsoe said that Porter had changed his story several times, especially when he denied that he had told an investigator that Gray had told him he could not breathe.
Gray told Porter he needed medical aid and Porter put him on a van bench. According to testimony, Porter told the van driver and a supervisor that Gray had asked for aid, but none was summoned.
(Reporting by Ian Simpson and Donna Owens; Editing by Dan Grebler and Andrew Hay)
