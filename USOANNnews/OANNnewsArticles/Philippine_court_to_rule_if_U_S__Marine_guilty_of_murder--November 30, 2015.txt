Search for:
Philippine court to rule if U.S. Marine guilty of murder
Activists display placards of U.S. Marine Private First Class Joseph Scott Pemberton during a protest outside the presidential palace in Manila October 28, 2014. REUTERS/Romeo Ranoco
November 30, 2015
MANILA (Reuters) � A court in the Philippines will decide on Tuesday whether a U.S. Marine is guilty of murder in the killing of a transgender woman, a case that has reignited debate over the American military presence in the poor Southeast Asian country.
Lance Corporal Joseph Scott Pemberton, who is being held at a U.S. facility at the main army base in Manila, has been on trial for the murder of Jennifer Laude, who was found dead last year in a hotel in Olongapo City, near a former U.S. naval base northwest of the Philippine capital.
�We�ve presented overwhelming evidence for the murder,� Harry Roque, a lawyer for the victim, told Reuters, saying he was confident the court would find Pemberton guilty and sentence him to life imprisonment.
This term of 20 years to life term is the maximum penalty the regional trial court can hand down for the crime, after the Philippines repealed the death penalty in June 2006.
But defense lawyer Rowena Flores said the marine should be found innocent, because the prosecution failed to explain why Laude�s neck bore traces of three kinds of DNA, including those of the victim and the suspect.
The court has not identified the third DNA source, boosting defense lawyers� theory that another person might have killed Laude, said by forensic experts to have died on Oct. 12. Pemberton left the hotel at 11:30 p.m. the previous day.
�Clearly, Joseph Scott Pemberton cannot be found responsible for Laude�s death,� Flores told Reuters. �We believe that Pemberton will be acquitted eventually. If not by the regional trial court, then by the Court of Appeals or the Supreme Court.�
The case has stirred debate over the presence of American soldiers on Philippine soil after Filipino senators voted two decades ago to kick out U.S. bases in the country because of social issues, such as crimes committed by servicemen.
�The only reasonable expectation is that Pemberton would be convicted,� Renato Reyes, secretary-general of left-wing group Bayan (Nation) said in a statement. The group plans to hold protests outside the court and near the U.S. embassy in Manila.
�Should he be acquitted, that would be a great injustice to Laude and the Filipino people. That would be unacceptable,� Reyes said, who wants the Philippines to scrap military pacts with Washington.
The two allies are waiting for the Philippine supreme court to approve a pact allowing the U.S. military to store supplies at Philippine bases for operations related to maritime security, humanitarian assistance and disasters.
(Reporting by Manuel Mogato; Editing by Clarence Fernandez)
