Search for:
Amid chronic U.S. homelessness, Northwest backs �tent cities�
Kadee Ingram, 28, holds her son Sean, 2, at SHARE/WHEEL Tent City 3 outside Seattle, Washington October 13, 2015. REUTERS/Shannon Stapleton
December 17, 2015
By Eric M. Johnson and Shannon Stapleton
SEATTLE (Reuters) � For years, Jammie Nichols struggled with a drug habit that left the Florida mother reeling from blackouts, seizures, depression and poverty � and a decision to give one of her children up for adoption.
Then a friend told her about Tent City 3, a peer-run homeless encampment in the Seattle area with ties to social welfare programs. The police patrolled sporadically but left occupants largely alone, and volunteers often dropped off hot meals.
In June, Nichols bought a bus ticket and arrived in Seattle broke, and four months later, she had a steady boyfriend, had kicked her drug habit and had been elected to the camp�s executive committee, she said.
�I�ve really come out of my shell,� she said over a bowl of rice inside a tarp-covered communal dining area. �Today I filled out my first application for housing.�
Despite the benefits tent cities often provide occupants, and although American cities are grappling with a chronic shortage of affordable housing for the poor and budget constraints on social programs, many municipalities across the United States are clamping down on homeless encampments.
Citywide anti-camping bans have increased by roughly 60 percent since 2011, according to a 2014 report from the National Law Center on Homelessness & Poverty (NLCHP), a research group. The U.S. government tallied more than half a million people living on the streets on a one-night count this year, a quarter of them children.
�Doing nothing, simply moving these people, they are just going to pop up down the street,� said Andrew Heben, who wrote �Tent City Urbanism.� �Not letting people legally exist costs more than giving people places to stay.�
CLAMPDOWN
Reuters interviewed the dozens of people in sanctioned tent cities and nearly all spoke of having found a far more stable, safe, goal-oriented life inside a tight-knit community than they did living on the streets or in a traditional shelter.
�The unique thing about the camp is the sense of the community,� said Matt Mercer, a former tent city dweller who now volunteers at Camp Hope in Las Cruces, New Mexico. �When you are in the shelter system, you don�t see community; people are all just in survival mode.�
Life is not without its tests, though. The roughly 60 residents of Tent City must work unpaid shifts doing security and clean-up, attend weekly meetings, live without heat, and abstain from liquor, drugs and violence.
Tracking tent cities is difficult, but the NLCHP identified more than 100 across 41 U.S. states from 2008-2013, though few were officially sanctioned. Today there are roughly a dozen legal encampments nationwide, with scores of others existing on the margins or facing eviction.
Authorities have cleared well-established camps this year in Honolulu; Washington, D.C.; and, most recently, Boise, Idaho.
�It�s become unhealthy, unsafe and really unsustainable,� Boise Mayor Dave Bieter said earlier this month when he ordered police to evict and clear a camp with some 130 dwellers.
That came two months after a U.S. judge dismissed a 2009 lawsuit brought by homeless people convicted under Boise�s anti-camping laws, despite a U.S. Justice Department brief backing the plaintiffs.
�We aren�t going to have anywhere to go,� said JoJo Valdez, 40, who said she had been among those sobbing as police cleared the camp. �There are places down by the river where people are hiding out.�
�FULL-BLOWN CRISIS�
Oregon cities Portland and Eugene, and Olympia in Washington, are among the few municipalities that have allowed encampments. Clearwater, Florida, also has the church-backed Pinellas Hope village.
Seattle has voted to permit three tent cities, with Mayor Ed Murray declaring homelessness a �full-blown crisis� following the death this year of 66 homeless people on the streets or in illegal campsites, and a 21 percent jump in the King County homeless population since 2014.
By month�s end, there could be at least five well-established encampments on private and public lands in the Seattle area, including two on city property near downtown.
Some tent cities, such Eugene�s Opportunity Village, have been upgraded to �micro houses,� essentially one-window sheds. One such shantytown, on a hillside in Seattle, has given Matt Hannahs and his 6-year-old son refuge from soaring rents and the onset of winter.
�Hopefully within a year from now we�ll be settled into a new place and Devin will have a regular school that he can go to, instead of having to move around to different schools,� Hannahs said. �Hopefully by then we�ll have housing.�
(Reporting by Eric M. Johnson and Shannon Stapleton; Editing by Patrick Enright and Steve Orlofsky)
