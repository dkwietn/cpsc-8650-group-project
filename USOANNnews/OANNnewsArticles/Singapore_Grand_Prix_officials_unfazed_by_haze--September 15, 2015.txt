Search for:
Singapore Grand Prix officials unfazed by haze
A view of the Singapore F1 Grand Prix night race paddock shrouded by haze in Singapore September 14, 2015. REUTERS/Edgar Su
September 15, 2015
SINGAPORE (Reuters) � Organisers of this weekend�s Singapore Grand Prix have played down concerns about the cloud of haze hanging over the city, saying it was not expected to impact on Sunday�s Formula One race.
The city-state has been blanketed by thick smog for the past week, a result of farmers in neighbouring Sumatra burning forests to clear their land for agriculture.
The Pollutant Standards Index (PSI) in Singapore has fluctuated well above 100, levels considered �unhealthy�, for the past few days, and reached as high as 249 on Monday night, putting it in �very unhealthy� territory.
Race officials said they were monitoring the situation and planned to place first-aid stations on standby to treat any possible haze-related conditions over the weekend, as well as selling face masks at cost price.
But the officials said there were no plans to change any of the scheduled events over the race weekend, including the pop concerts held each night at the Marina Bay street circuit.
�Based on the current PSI levels, there are no plans to amend the published racing and entertainment programme,� Singapore GP said in a statement on Tuesday.
�The haze situation is highly changeable not only from day to day, but from hour to hour. Therefore, it is currently not
possible to reliably predict what the PSI level might be over the race weekend.
�We will continue to work closely with all the relevant government authorities to receive the best possible forecasts when they are available.�
The Singapore Grand Prix is the 13th race of the season and defending world champion and last year�s winner Lewis Hamilton will be a strong favourite to extend his championship lead, currently 53 points, with an eighth victory of the campaign.
(Reporting by Julian Linden and Pritha Sarkar)
