Search for:
Dallas boy who was allegedly fed ant poison released from hospital: police
Paw Eh, 31, is shown in this booking photo provided by the Dallas Police Department November 6, 2015. REUTERS/Dallas Police Dept/Handout via Reuters
November 6, 2015
By Lisa Maria Garza
DALLAS (Reuters) � A 4-year-old Dallas boy whose mother is charged with feeding him ant poison and trying to do the same to his siblings has been released from the hospital, police said on Friday.
Child Protective Services placed the boy and his two older siblings in a foster care home, according to Dallas police. The names of the children have not been released.
Their mother, Paw Eh, 31, was arrested last Saturday and charged with three counts of attempted capital murder after her 12-year-old daughter told police she watched her mother mix a tablespoon of poison powder with water and force the young boy to ingest it, according to an arrest report.
The girl and her 7-year-old brother ran for help when Eh tried to get them to swallow the poison, police said.
Eh had previously told the children that she was going to kill them and herself, the report said.
A lawyer for Eh was not immediately available for comment. She remains in Dallas County jail on a $500,000 bond, according to online jail records.
(Reporting by Lisa Maria Garza; Editing by Ben Klayman and Frances Kerry)
