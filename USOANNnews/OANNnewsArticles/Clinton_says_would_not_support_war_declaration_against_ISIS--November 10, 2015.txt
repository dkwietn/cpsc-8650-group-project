Search for:
Clinton says would not support war declaration against ISIS
U.S. Democratic presidential candidate Hillary Clinton listens to a question from the audience during a veterans roundtable discussion with the Truman National Security Project at the VFW Hall in Derry, New Hampshire November 10, 2015. REUTERS/Brian Snyder
November 10, 2015
DERRY, N.H. (Reuters) � Democratic presidential candidate Hillary Clinton said on Tuesday she was not ready to support a formal declaration of war against Islamic State militants, although she said the United States needs to improve its efforts to fight the group.
To have a declaration of war, she said, requires understanding the resources available and the goals involved.
�If you have a declaration of war, you�d better have a budget that backs it up,� said Clinton, who was campaigning in New Hampshire.
�I do think that we have to do a better job of understanding the threat that is posed by radical Islamic jihadist groups,� added Clinton, who is seeking the Democratic nomination for the November 2016 presidential election.
Because Islamic State, or ISIS, is so diffuse across the Middle East, she said, a declaration of war might not be the right way to fight such a group, calling ISIS �the first Internet terrorist network.�
Islamic State controls swathes of Iraq and Syria and is battling the Egyptian army in the Sinai Peninsula.
The group has recently said it was responsible for the crash of a Russian airliner over Egypt�s Sinai peninsula.
(Reporting by Luciana Lopez; Editing by David Gregorio)
