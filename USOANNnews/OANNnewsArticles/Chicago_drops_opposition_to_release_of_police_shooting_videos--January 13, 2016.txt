Search for:
Chicago drops opposition to release of police shooting videos
Protesters lie down on Chicago's Michigan Avenue in front of the Disney Store during a protest march against police violence in Chicago, Illinois December 24, 2015. REUTERS/Frank Polich
January 13, 2016
By Mary Wisniewski
CHICAGO (Reuters) � The city of Chicago, which has seen weeks of protests calling for Mayor Rahm Emanuel�s resignation over police shootings of black citizens, said on Wednesday it had dropped its opposition to the release of videos showing the killing of a black teenager.
The videos, which could be released on Thursday, show the shooting death of Cedrick Chatman, 17, in January 2013. Chatman�s mother sued the city in federal court, and a judge was set to rule on Thursday on whether or not to allow the release of the videos.
Emanuel and the city�s police have been under sharp criticism since the November release of a video showing a Chicago police officer shooting to death Laquan McDonald, 17, in October 2014 while he appeared to be walking away. McDonald was black.
Protesters have questioned why it took more than a year to release the video and to charge Officer Jason Van Dyke, who is white, with murder. Chicago police have since come under a federal civil rights investigation into its use of force.
The Chatman videos are under a protective order, and the city filed a court motion on Wednesday to vacate it. City attorney Steven Patton said in a statement that the city is �working to be as transparent as possible.�
Use of force by law enforcement officers has become a focus of national attention due to a series of high-profile police killings of black men, mostly by white officers in U.S. cities.
Lawyers for Chatman have argued that the videos of his shooting contradict statements by police that Chatman, a carjacking suspect, had pointed a dark object at them. The suit said that Chatman posed no threat to officers.
Andy Hale, an attorney for Chicago police officers Kevin Fry and Lou Toth, said in an email that the videos will back up their version of the story. Hale said a nationally-recognized expert on police use of force has produced a report concluding that the shooting was justified.
The city�s law department said the protective order was originally entered as part of the city�s longstanding policy to not publicly release videos and other evidence related to alleged police misconduct until investigations are complete.
Patton said the city recognizes its policy �needs to be updated,� and is waiting for guidance from a newly formed police accountability task force.
(Reporting by Mary Wisniewski; Editing by Andrew Hay)
