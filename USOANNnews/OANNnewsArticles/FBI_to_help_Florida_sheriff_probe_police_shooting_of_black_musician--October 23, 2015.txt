Search for:
FBI to help Florida sheriff probe police shooting of black musician
Corey Jones, 31, a professional drummer, is shown in this photo released by Florida State University National Black Alumni, Inc. on October 20, 2015. REUTERS/Florida State University National Black Alumni, Inc./Handout
October 23, 2015
By David Adams and Zachary Fagenson
MIAMI/PALM BEACH GARDENS, Fla. (Reuters) � The Florida sheriff investigating a plainclothes police officer�s fatal shooting of a black musician asked for federal assistance on Friday to �ensure the highest level of scrutiny and impartiality.�
The Palm Beach Sheriff�s Office, facing public criticism for its handling of prior officer-involved shootings, said in a statement that the Federal Bureau of Investigation had accepted the request in the case of Corey Jones, a 31-year-old drummer who was shot early Sunday.
It said the decision was made �to provide the family of Corey Jones and the community with a�thorough and accurate investigation.�
Local politicians and activists say the Palm Beach Sheriff�s Office has lost the public�s trust and that the FBI was already looking into one 2013 incident.
They are calling for the U.S. Department of Justice or the Florida Department of Law Enforcement to investigate the killing of Jones, shot dead by a Palm Beach Gardens police officer after his car broke down on a highway exit ramp as he drove home from a gig.
�There are so many questions, lots of holes. People want an independent investigation and not locally done,� said Priscilla Taylor, a Palm Beach County commissioner who has led calls for community oversight of the police.
Palm Beach Sheriff Ric Bradshaw announced the FBI�s review of an unspecified case in May, after the Palm Beach Post and WPTV NewsChannel 5 revealed that, following a year-long investigation, the law enforcement agency had determined that all but one of 45 deadly deputy-involved shootings since 2000 were justified.
The case under FBI review is believed to be that of an officer of Asian descent who shot an unarmed black man riding a bike in 2013.
In that case, video shows Dontrell Stephens running and falling to the ground as, off camera, four shots are fired and an officer comes into the frame standing over him with a gun.
His injuries left Stephens paralyzed from the waist down. The officer, Adams Lin, was cleared of any wrongdoing in the incident and promoted to sergeant in June.
Bradshaw has said he welcomed the federal inquiry but has also dismissed critics of his office.
�This is a simple equation,� he said in a video address to a police gala in July. �If you don�t try to shoot us, if you don�t try to stab us, you don�t try to run over us with a car, and you don�t try to beat us up, then everything�s going to be fine.�
Jones� death was the latest in a string of fatal incidents involving police and black men across the United States that have sparked outrage and fueled questions about excessive use of force by officers.
In its announcement on Friday, the Palm Beach County Sheriff�s Office recognized the uneasy national climate. �There have been many lessons learned from the tragic events that have occurred across the United States and there is nothing more important, now, than a comprehensive investigation process so we can ensure justice is served,� it said.
Police say there was a confrontation with Jones, who was carrying a licensed handgun, after officer Nouman Raja, 38, pulled up in an unmarked van.
Raja shot six times, hitting Jones with three bullets, while Jones did not fire his weapon, the drummer�s lawyers told reporters Thursday after meeting the state attorney who is investigating the incident.
Bradshaw last spring announced efforts to improve his agency�s internal reporting procedures for officer-involved shootings �to provide greater accountability.� Bradshaw also created Citizen Advisory meetings to bring together police and community leaders.
A group of mostly black Palm Beach County lawyers said, however, that any findings in the Jones case by the sheriff�s office would be tainted by its handling of past cases.
�We know the history and we don�t like it,� said Byrnes Guillaume, a former prosecutor and president of the F. Malcolm Cunningham, Sr. Bar Association.
(Writing by David Adams; Editing by Colleen Jenkins and Grant McCool)
