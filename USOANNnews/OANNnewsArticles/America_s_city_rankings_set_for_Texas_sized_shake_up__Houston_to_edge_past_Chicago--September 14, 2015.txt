Search for:
America�s city rankings set for Texas-sized shake up; Houston to edge past Chicago
Houston Mayor Annise Parker speaks at her office in Houston City Hall on August 24, 2015. REUTERS/Jon Herskovitz
September 14, 2015
By Jon Herskovitz
HOUSTON (Reuters) � Hidden in the haze of the petrochemical plants and beyond the seemingly endless traffic jams, a Texas city has grown so large that it is poised to pass Chicago as the third biggest in the United States in the next decade.
Houston has been one of the fastest-growing U.S. cities for years, fueled by an energy industry that provided the backbone of the economy, low taxes and prospects of employment that have attracted job seekers.
But Houston also embodies the new, urban Texas, where political views have been drifting to the left, diversity is being embraced and newer residents are just as likely to drive a hybrid as a pickup truck.
Houston�s move is also indicative of demographic shifts unfolding in the United States that will increase the population and political clout of the Lone Star State over the next several decades.
Within eight to 10 years, Houston is forecast by demographers in the two states to pass Chicago, which has seen its population decline for years, as the third-largest city.
Houston is projected to have population of 2.54 million to 2.7 million by 2025 while Chicago will be at 2.5 million, according to official data from both states provided for their health departments. New York and Los Angeles are safe at one and two respectively.
Houston has long been associated with the risk takers in the oil industry and more recently as one of the better cities to find a job.
�Texas has a long tradition, and Houston has it in spades, that we are not so much interested in where you are from. We want to know what you can do,� Houston Mayor Annise Parker said in an interview with Reuters.
Chicago officials were not immediately available for comment.
Apart from domestic migration, about one in five Houstonians is foreign born and more than 90 languages are spoken in the city.
�We have that international mindset that the rest of the United States never saw,� said Parker, a former oil executive and city controller who has a collection of urban achievement awards and rodeo belt buckles in her office.
On Houston�s fringes are petrochemical plants that fuel the economy, space agency NASA that attracts aerospace jobs and a port that handles more foreign tonnage than any other in the United States.
In between is a mass of relatively unplanned urban sprawl, strip malls, ethnic enclaves, trendy restaurants and burgeoning green spaces lying under an umbrella of oppressive heat that lasts more than half the year.
ROLLER COASTER ECONOMY
The energy industry, which accounts for about 40 percent of Houston�s economy, has sent the fortunes of the city on a roller coaster ride for decades.
With oil currently at around $45 a barrel, the brakes have been slammed on job growth, and a slight chill has entered into the booming construction sector.
Since 1969, Houston has been one of the most successful major U.S. cities in terms of per capita personal income growth. Since about 2003, about 650,000 jobs have been created in the Houston area, according to the University of Houston.
The Houston-area unemployment rate has remained below the national average for years, according to government data, while the Chicago-area has recently been above it.
However, Houston�s growth, coming with few zoning restrictions and a loose regulatory system in Texas, has led to persistent problems in air quality and traffic congestion.
�HOW THE HELL DID THAT HAPPEN?�
Mayor Parker, who is leaving office after six years due to term limits, made headlines when she was elected the first open lesbian to run a major U.S. city. She capitalized on the media attention as a chance to promote the city as a good place to do business, she said.
The city purchases more renewable energy than any other in the United States, said Parker, who has launched a $250 million project to put bike and hike trials along the bayous, or small rivers, that run through the city like veins.
On social issues, residents in one of the most racially diverse U.S. cities are seen as �tolerant traditionalists� who espouse conservative values and open minds when it comes to social issues, according to a poll from the Kinder Institute for Urban Research at Houston�s Rice University.
Residents generally have a positive view of immigrants, favor same-sex marriage and are more progressive than the state�s socially conservative Republican leadership, it said.
The city ranks near the top in the United States in terms of resettling refugees from abroad and when the price of oil picks up again, it will see a fresh wave of migration from those seeking employment.
�If you see the way the crowd is going, you might as well jump in front of it and make it a parade,� Parker said.
(This story has been filed to fixe typo in 13th paragraphs on word �restaurants�)
(Reporting by Jon Herskovitz; Additional reporting by Terry Wade; Editing by Cynthia Osterman)
