Search for:
Spooked by sugar, Americans search for healthier Halloween treats
A boy collects candy as he goes trick-or-treating for Halloween in Santa Monica, California, October 31, 2012. REUTERS/Lucy Nicholson
October 30, 2015
By Luc Cohen
NEW YORK (Reuters) � The so-called war on sugar has a new battleground: Halloween.
Some Americans are so spooked about the harmful effects of sugar they are finding healthier ways to indulge during the holiday, without disappointing children by handing out apples.
Sugary chocolates still dominate Halloween candy handed out and provide an annual sales treat for companies like Hershey Co, privately held Mars and Nestle SA.
But as more consumers demand healthier candies, manufacturers could see the so-called �War on Sugar� scare away some of the $2.1 billion that the National Retail Federation says Americans will spend on Halloween candy this year.
The World Health Organization has linked sugar intake with chronic diseases including diabetes and heart disease, a finding disputed by the Sugar Association, a U.S. trade group.
Some companies, like upstart candy maker Unreal, privately owned health products maker Xlear Inc and Kosher Foods producer Kayco, have developed low-sugar candies from unusual concoctions, like puffed quinoa and cabbage, to win over health-conscious, sugar-wary shoppers.
The data may give some a fright. Nearly one-fourth of Americans say they are buying healthier candy like dark chocolate or chocolates with added fruits or nuts for seasonal occasions like Halloween than five years ago, National Confectioners Association (NCA) data shows.
In addition, one in five say they are more likely to buy chocolates or candies in a smaller portion size.
Confectionery makes up about 13 percent of Americans� 10.8 million tonne-per-year sugar consumption, and the candy industry says that Americans know there is sugar in candy and moderate their intake accordingly.
TOIL AND TROUBLE
Still some shoppers are trying different brews this year.
Kevin Schiffman, a self-described �health freak,� bought a can of Unreal chocolates for $20 during a recent trip to Whole Foods, opting to spend significantly more on the treats he plans to hand out to trick-or-treaters than last year.
The treats, which are made from fair trade cocoa and cane sugar, puffed quinoa, and cabbage, carrots and beets for coloring, contain around 5 grams of sugar per serving, compared with over 20 grams for many traditional candy bars.
In prior years, he said he handed out assorted chocolates like Reese�s and Kit Kats, wary that health-minded people sometimes draw groans and eye-rolls from trick-or-treaters by giving out apples or toothbrushes.
�There wasn�t really much out there that you could choose from unless you�re giving out fruit,� said the 35-year-old Boston-area resident, who works in sales at WikiFoods, a Cambridge, Massachusetts, food company.
�We don�t want to get eggs on the house.�
Xlear hopes to win over customers with its Sparx brand of candies that use xylitol, a fibrous sugar found in birch trees and corn cobs that it says is safe for diabetics and lower in calories than cane sugar.
At the moment, they are only sold at health food stores and distributed to dental offices to give to kids as part of Halloween candy buyback programs, though the company is planning to expand, said Shad Slaughter, a consultant at Xlear.
Kayco has introduced Chocolate Leather, a chewier, lower-sugar chocolate bar.
�With the growing awareness of parents who would like their kids to eat less sugar, this is going to grow,� said Glenn Schacher, a research and development specialist in New York who developed the product of Kayco
The Obama administration has pushed for food companies to include added sugar content on their labels, which has drawn sharp rebukes from the sugar lobby.
For the moment, most Americans will still hand out candy and chocolates filled with sugar to trick-or-treaters on Saturday and use the holiday as an excuse to indulge.
�I can be a pig four times a year,� said Tom Cardamone, a 46-year-old Brooklyn, New York, resident, who said he is not normally much of a candy eater. �Every holiday, I know it�s candy, and I have my addictions.�
Graphic on sugar demand over Halloween�http://link.reuters.com/fux85w
(Editing by Josephine Mason and Lisa Shumaker)
