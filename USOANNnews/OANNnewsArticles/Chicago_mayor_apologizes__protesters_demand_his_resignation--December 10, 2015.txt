Search for:
Chicago mayor apologizes, protesters demand his resignation
Chicago Mayor Rahm Emanuel listens to remarks at a news conference in Chicago, Illinois, United States, December 7, 2015. REUTERS/Jim Young
December 10, 2015
By Mary Wisniewski and Justin Madden
CHICAGO (Reuters) � Chicago Mayor Rahm Emanuel, under heavy criticism for his handling of the fatal police shooting of a black teen, gave an emotional apology on Wednesday, but angry crowds closed city streets to demand his resignation hours later.
In a special address to the City Council, the mayor said �I�m sorry� and promised �complete and total reform of the system.�
Emanuel�s speech was met with applause from the City Council, but protesters said the city�s actions do not go far enough. Hundreds of mostly young demonstrators filled downtown on Wednesday, temporarily shutting down some streets and chanting �no more killer cops� and �Rahm must go.�
�This system is designed for us to be dead or in jail and we�re tired,� said protester Jamal Wayne, 20.
Emanuel�s speech followed two weeks of protests in Chicago after the release of a 2014 police dashboard video showing officer Jason Van Dyke shooting 17-year-old Laquan McDonald 16 times. Van Dyke, who is white, was charged with first-degree murder late last month.
High-profile killings of black men by mostly white police officers in U.S. cities have prompted a national debate and protests about the use of excessive force by police.
With his voice occasionally breaking, the mayor of the nation�s third-largest city reiterated reform steps he has already promised. These include setting up a task force to review police accountability, the appointment of a new head of the agency that investigates police misconduct and searching for a new police superintendent.
Among problems with police, Emanuel aimed particular criticism at the �code of silence� that keeps police officers from reporting misconduct by fellow officers. He also has criticized the agency that investigates police misconduct for finding almost all police shootings justified.
�We have a trust problem,� said Emanuel, who stated last week that he had no plans to resign.
A poll over the weekend for the Illinois Observer showed 51 percent of Chicagoans think the mayor should resign, compared with 29 percent who think he should not. Twenty percent were undecided. The survey of 739 respondents had a margin of error of plus or minus 3.68 percent.
Civil rights leader Jesse Jackson, in a brief phone interview with Reuters, said the mayor was facing a �sea of distrust� with his constituents, in large part because of his handling of the McDonald shooting.
On Wednesday evening, about 200 people gathered at a Police Board meeting where they held up signs such as �Black Lives Matter� and �Rahm You Have Blood On Your Hands.�
Protesters have also called for the resignation of Cook County State�s Attorney Anita Alvarez, who has been criticized for taking more than a year to charge Van Dyke. The protesters were mostly in their teens and 20s � and three teenagers were arrested on unknown charges.
Protester Aaron Clay, 34, said that while Emanuel�s speech may have been emotional, �I don�t think it was an apology to the community.�
Representative La Shawn Ford, a Chicago member of the Illinois legislature�s black caucus, filed a bill in Springfield on Wednesday to allow voters to recall Emanuel.
The U.S. Justice Department said on Monday it will launch a civil rights investigation into the city�s police department, examining its use of deadly force among other issues.
Also on Wednesday, a federal judge said he would rule by Jan. 14 on whether to release video in the shooting death of another black teen. The mother of Cedrick Chatman, 17, has sued the city over Chatman�s death on Jan. 7, 2013. The city has opposed release of video in the case.
(Additional reporting by Renita Young in Chicago and Alex Dobuzinskis in Los Angeles,; Editing by Matthew Lewis, Tom Brown and Leslie Adler)
