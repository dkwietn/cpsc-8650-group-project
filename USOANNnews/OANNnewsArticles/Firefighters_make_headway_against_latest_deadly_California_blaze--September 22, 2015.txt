Search for:
Firefighters make headway against latest deadly California blaze
A home burns as the Butte Fire rages near Mountain Ranch, California in this September 11, 2015 file photo. REUTERS/Noah Berger/Files
September 22, 2015
By Steve Gorman
(Reuters) � Firefighters have gained significant ground against the latest deadly blaze to inflict mounting property losses in California while chasing thousands of residents from their homes in a wildfire season shaping as one of the worst on record.
Containment of the so-called Tassajara Fire, a measure of how much of its perimeter has been enclosed within buffer lines carved through vegetation by ground crews, stood at 50 percent on Monday. More than 1,000 evacuees were allowed to return home on Sunday.
The blaze erupted on Saturday in the Carmel Valley of Monterey County, 125 miles south of San Francisco, and has scorched more than 1,000 acres while destroying or damaging 10 homes and claiming this year�s eighth California wildfire fatality.
The circumstances of the latest death, that of an unidentified civilian on Saturday, remained under investigation, fire officials said.
The Tassajara Fire pales in comparison to two larger blazes that have preoccupied fire crews farther north over the past two weeks, together laying waste to more than 1,400 homes and nearly 900 other structures.
Firefighters have largely curtailed those conflagrations, dubbed the Valley Fire and the Butte Fire, which rank as the most destructive among thousands of blazes that have raged across the drought-stricken western United States this summer.
Evacuation orders were being lifted in both fire zones in recent days for many of the estimated 20,000 people displaced at the height of the threat.
California Governor Jerry Brown on Monday requested federal funds to support fire victims and ongoing recovery efforts, his office said.
The Valley Fire, which ignited Sept. 12 in the hills north of Napa County�s wine-producing region, has blackened nearly 76,000 acres and reduced 888 homes to ruins but was listed as 75 percent contained on Monday.
The Butte Fire has charred almost 71,000 acres and leveled 545 dwellings since it broke out on Sept. 9 in the historic Gold Rush country of the Sierra Nevada foothills, but was nearly 75 percent contained as well.
Five people lost their lives in the two Northern California infernos, including two people who authorities said defied evacuation orders in the Butte Fire and an elderly disabled woman trapped in her home by the Valley Fire.
Two firefighters were killed battling two other California wildfires in August, and three more died together in an initial assault against a major wildfire in August in Washington state.
(Reporting by Steve Gorman in Los Angeles; Editing by Grant McCool)
