Search for:
Harrisburg, Pennsylvania�s ex-treasurer avoids prison in embezzlement plea
John Campbell poses at the entrance to the municipal office building in Harrisburg, Pennsylvania, January 18, 2012. REUTERS/Tim Shaffer
September 15, 2015
By David DeKok
HARRISBURG, Pa. (Reuters) � The former treasurer of financially troubled Harrisburg, Pennsylvania, received probation on Tuesday after paying restitution for embezzling from three non-profit groups for which he once worked.
John Campbell, 27, avoided prison time by handing a cashier�s check for $26,230.05, the amount of restitution he owed, to the clerk of Dauphin County Court of Common Pleas in Harrisburg on Tuesday.
Campbell, who now lives in Washington, D.C., was sentenced by Judge Scott Evans to 36 months of supervised probation and fined $1,050. He was barred from working for any Pennsylvania charitable organization.
�I just want to apologize,� Campbell told Evans. �I understand my actions were selfish.�
He pleaded guilty in July to two counts of felony theft and one count of charitable fraud, a misdemeanor.
Campbell was elected city treasurer of Harrisburg, a part-time position that paid $20,000 a year, in November 2012. He resigned after his indictment in 2014. A subsequent audit did not find any missing city funds. Harrisburg is the capital of Pennsylvania.
Campbell admitted stealing money from the three non-profits: Historic Harrisburg Association, where he had been executive director; Lighten Up Harrisburg, which collected donations, including from a 5-km run, to replace burned-out city streetlights; and the Capital Region Stonewall Democrats.
Campbell told investigators he used the stolen money to pay student loan and medical debts.
Matthew Krupp of Lighten Up Harrisburg told the court on Tuesday that Campbell�s theft made their job �a whole lot harder. John�s actions hurt our reputation and our donors.�
Evans said Campbell�s crimes �will follow him throughout his life. It was a completely self-destructive act that had rippling effects.�
Assistant District Attorney Joel Hogentogler said he had agreed to recommend probation for Campbell if he made full restitution by his sentencing date.
(Editing by Frank McGurty and Peter Cooney)
