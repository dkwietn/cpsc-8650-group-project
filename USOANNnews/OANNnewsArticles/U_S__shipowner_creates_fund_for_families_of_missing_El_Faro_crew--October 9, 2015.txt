Search for:
U.S. shipowner creates fund for families of missing El Faro crew
The El Faro is shown in this undated handout photo provided by Tote Maritime in Jacksonville, Florida, October 2, 2015. REUTERS/Tote Maritime/Handout via Reuters
October 9, 2015
MIAMI (Reuters) � The owners of the cargo ship El Faro that sank after it was trapped in the path of Hurricane Joaquin off the Bahamas last week announced the creation of a family relief fund on Friday to support the 33 mostly-American families of the crew.
�Over the last few days we have had hundreds of employees, mariners, customers and individuals from around the country inquire about where to donate in support of the families,� Anthony Chiarello, chief executive of New Jersey-based Tote Inc, owners of the El Faro, said in a statement.
The U.S. Coast Guard on Wednesday called off its search for survivors of what authorities have called the worst cargo shipping disaster involving a U.S.-flagged vessel in more than 30 years.
The body of only one presumed crew member was found during the nearly week-long search. El Faro was carrying 28 U.S. crew members and five Polish contractors when it disappeared.
The family relief fund will be administered by the Seamen�s Church Institute, North America�s largest mariners� service agency, Tote said.
�We continue to keep the families and loved ones of the crew of the El Faro in our thoughts and prayers,� Chiarello said.
Tote said it will also establish an education fund for the children of the El Faro crew members.
The 790-foot (240 meter) container ship vanished in ferocious winds and seas up to 50-feet (15 meters) high on Oct. 1 while on a regular weekly cargo run between Jacksonville, Florida, and Puerto Rico, the Coast Guard said.
In a final distress call, before all communication with the ship was lost, El Faro reported that it had lost propulsion, was taking on water and listing. No reason was given for the loss of power.
(Reporting by David Adams; Editing by Tom Brown)
