Search for:
Cosby lawyers ask to delay Camille Cosby�s deposition
Actor and comedian Bill Cosby (C) arrives for his arraignment on sexual assault charges at the Montgomery County Courthouse in Elkins Park, Pennsylvania December 30, 2015. REUTERS/Mark Makela
January 5, 2016
By David Ingram
(Reuters) � Lawyers for Bill Cosby and his wife, Camille, asked a federal judge on Monday to delay a deposition by her scheduled for Wednesday in a defamation lawsuit brought by seven women who allege the entertainer sexually abused them.
The lawyers said in papers filed in U.S. district court in Massachusetts that the sworn interview should be delayed pending an appeal on whether Camille Cosby could be required to testify at all.
On Dec. 31, a federal magistrate judge in Massachusetts rejected arguments by Camille Cosby, the comedian�s wife of almost 52 years and his business manager, that the deposition would represent an �undue burden.�
Cosby lawyers argued the intimate nature of the questions expected during the deposition justified a delay, noting that their ability to object to such evidence at trial would not spare their client embarrassment.
�The Magistrate has suggested that the plaintiffs may ask Mrs. Cosby about the most intimate details of her marital life, including her husband�s sexual �proclivities,'� Camille Cosby�s attorneys wrote. �A trial objection will do Mrs. Cosby little good if the private and intimate details of her marital life are discussed during deposition and released to the media thereafter.�
The Dec. 31 ruling came a day after Cosby, 78, was charged with sexually assaulting a woman who prosecutors say the comedian plied with drugs and alcohol in 2004, the only criminal case filed against the entertainer.
More than 50 women have come forward to accuse Cosby, best known for his role in the 1980s television hit �The Cosby Show� of sexually assaulting them after plying them with drugs or alcohol. Cosby has repeatedly denied wrongdoing and many of the allegations involve decades-old incidents.
Cosby is free on $1 million bail, and his lawyer has said the entertainer is not guilty and will not consider a plea bargain.
The Massachusetts civil lawsuit against Cosby was filed in December 2014 by Tamara Green, later joined by six other women, who contend that Cosby sexually assaulted or abused and then defamed them by calling each a liar, court documents said.
Cosby last month counter-sued the women, claiming that they had defamed him by accusing him of sexual assault.
U.S. Magistrate Judge David Hennessy also rejected arguments by Camille Cosby that she lacked any first-hand knowledge of the events at issue, and that the court should protect her from �unnecessary harassment� by limiting the scope of the subpoena.
(Reporting by David Ingram in New York; Additional reporting by Scott Malone in Boston; Editing by Steve Orlofsky and Sandra Maler)
