Search for:
Americans stock up on weapons after California shooting
Mourners gather around a makeshift memorial in honor of victims following Wednesday's attack in San Bernardino, California, December 5, 2015. REUTERS/Sandy Huffaker
December 7, 2015
By Rich McKay and Daina Beth Solomon
ATLANTA/SAN BERNARDINO, Calif. (Reuters) � At a gun range in Atlanta on Sunday, four days after the deadliest Islamic State-inspired attack on American soil, Brandon Langley practiced firing his AR-15 semiautomatic assault rifle.
�If people were armed, it would have changed the outcome totally,� Langley said of Wednesday�s assault by a heavily armed husband and wife that killed 14 people and wounded 21 in San Bernardino, California. �Instead of 14 victims, there would have been zero, except for those two (attackers).�
Many Americans agree and are stocking up on weapons after the country�s worst mass shooting in three years. Gun retailers are reporting surging sales, with customers saying they want to keep handguns and rifles at hand for self-defense in the event of another attack.
�Everyone is reporting up, every store, every salesman, every distributor,� said Ray Peters, manager of Range, Guns & Safes, a company that sells firearms and safes in Atlanta with an indoor firing range. �People are more aware of the need to protect themselves.�
Peters usually carries a pistol with him. But since last week�s shooting, he says he�s added a Ruger semiautomatic rifle.
In a country where more people own more guns than anywhere else in the world, the shooting has reignited a long-running national debate over Americans� constitutional right to bear arms and whether gun ownership should be curbed or expanded as a way to stop even more bloodshed.
A recent spate of mass shootings, capped by Wednesday�s massacre in San Bernardino, has pushed those issues to the fore in the presidential campaign.
Wednesday�s shooting follows an attack that killed three on Nov. 27 at a Colorado Planned Parenthood clinic and an Oct. 1 rampage by a gunman who killed 10 at an Oregon college, prompted Hillary Clinton, the leading candidate for the Democratic nomination, to renew her call to �stop gun violence now� with new firearm purchase restrictions.
Conversely, those who top the polls for the Republican nomination, Donald Trump and Ben Carson, insist the answer to gun violence is to empower citizens to thwart such attacks by making it easier, not harder, to buy and carry weapon.
That viewed was echoed in the showroom of Turner�s Outdoorsman, a San Bernardino firearms dealer just a few blocks from where Syed Rizwan Farook, 28, and Tashfeen Malik, 29, gunned down their victims at a center for people with disabilities.
At Turner�s, business has been brisk since the shooting with about 40 shoppers gathering on Sunday morning, far busier than usual, clerks said.
Shivneel Singh, 27, said the shooting prompted him to buy a gun on Saturday. He already owned three handguns, a shotgun and a rifle. But he said he felt he needed another gun after the San Bernardino assault.
�I want to have it in the car, so it�s always ready,� said Singh, as he shopped at Ammo Brothers in Riverside, about 15 miles (24 km) from San Bernardino. He said he had several friends and neighbors planning to buy their first guns.
�I just want to feel safe.�
�FREE SOCIETY�
Gun sales have spiked after previous mass shootings, as reflected in Federal Bureau of Investigation data on background checks for people seeking to purchase guns.
The week the agency performed the most background checks since 1998 came immediately after the December 2012 school shooting in Newtown, Connecticut, that killed 27 people. Potential gun buyers that week numbered 953,600.
Gun sales were already on the rise this year. On Black Friday, the popular shopping day on Nov. 27 after the U.S. Thanksgiving Day holiday, a total of 185,345 applicants were processed through the FBI�s National Instant Criminal Background Check System, a 5.5 percent increase from the year before.
The Pew Research Center found last December that 57 percent of Americans say they believe owning a gun helps protect people from crime, up from 48 percent in 2012. The rest said owning a gun would put personal safety at risk.
Critics of America�s gun laws point to an array of statistics highlighting the risks of widespread gun ownership.
This includes data compiled by the Brady Campaign to Prevent Gun Violence, an advocacy group in Washington, that shows, on average, 89 people die each day from gun violence in the United States and 32,514 people are killed on average each year.
President Barack Obama repeated on Sunday his call for tighter gun purchase controls after the San Bernardino shooting, urging in a prime-time address that Congress pass provisions banning assault weapons and gun sales to people on a terrorist no-fly list.
But gun advocates say new regulations would not stop people determined to commit mass murder.
Langley, at Stoddard�s Range and Guns in Atlanta, said guns are needed in �a free society� � a view also expressed by Jasneil Singh, 19, as he shopped at Ammo Brothers in Riverside, California.
�It�s not the guns who do damage, it�s the people,� he said.
(Editing by Jason Szep, Jonathan Oatis and Andrew Hay)
