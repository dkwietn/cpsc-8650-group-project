Search for:
U.S. judge agrees to delay deposition of Cosby�s wife
Actor and comedian Bill Cosby (C) arrives for his arraignment on sexual assault charges at the Montgomery County Courthouse in Elkins Park, Pennsylvania December 30, 2015. REUTERS/Mark Makela
January 5, 2016
By Scott Malone
BOSTON (Reuters) � A U.S. magistrate judge on Tuesday agreed to delay the deposition of Bill Cosby�s wife by lawyers for women who have accused the comedian of sexual assault and sued him for defamation for calling them liars.
Cosby lawyers on Monday asked Magistrate Judge David Hennessy to delay the deposition, which was scheduled for Wednesday, saying there was a risk to her reputation if intimate details of the couple�s relationship were revealed.
Hennessy agreed to delay the deposition while lawyers for each side argue over whether Camille Cosby, the entertainer�s spouse of almost 52 years and business manager, can be compelled to give evidence against her husband.
The deposition was intended to gather evidence in Massachusetts civil suit, one of a series of legal actions Cosby, 78, is facing over claims by more than 50 women that the actor sexually assaulted them after plying them with drugs and alcohol, in alleged instances that played out over decades.
Cosby�s lawyers had argued that Camille Cosby could be embarrassed if her responses to questions about �the most intimate details of her marital life� were released publicly.
Lawyers for the seven women argued that the same judge�s order on Monday to seal the deposition eliminated any need to delay it.
�It would not serve (and in fact would offend) the �interest of justice� for this court to deny the motion for a stay, and thereby effectively deny Mrs. Cosby any right to appeal,� Hennessy wrote in his order.
People magazine on Tuesday reported Camille remained firm in her support of her husband, citing an unnamed source close to the family.
�They remain steadfast and resolute about working through this together,� People quoted the source as saying.
A spokesman for Cosby did not immediately respond to a request for comment on the report.
Prosecutors in Pennsylvania last week charged Cosby, best known for his role as the father character in the 1980s television hit �The Cosby Show,� with sexually assaulting a women in 2004. That is the only criminal case filed against Cosby, who has repeatedly denied wrongdoing.
Cosby is free on $1 million bail and his lawyer has said the entertainer is not guilty and will not consider a plea bargain.
The Massachusetts lawsuit was filed in December 2014 by Tamara Green, later joined by six other women, who contend Cosby sexually assaulted or abused and defamed them by calling them liars.
(Reporting by Scott Malone; Editing by Bill Trott and Cynthia Osterman)
