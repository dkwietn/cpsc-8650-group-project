Search for:
Sandusky move for new trial puts Pennsylvania AG Kane in spotlight
Jerry Sandusky (C) leaves the Centre County Courthouse after his sentencing in his child sex abuse case in Bellefonte, Pennsylvania October 9, 2012. REUTERS/Pat Little
October 29, 2015
By David DeKok
BELLEFONTE, Pa. (Reuters) � Pennsylvania Attorney General Kathleen Kane was ordered Thursday to turn over evidence that she says proves that a judge had leaked secret information to a reporter about a grand jury investigating Jerry Sandusky for child sex abuse.
The order came during a hearing into defense moves to win a new trial for Sandusky, the former assistant football coach at Penn State University, who was convicted in 2012 of molesting 10 boys over 15 years.
Sandusky lawyer Alexander Lindsay maintains that the grand jury initially heard from only two victims and the case was falling apart, but the judge�s leaks generated publicity that prompted other victims to come forward and helped prosecutors shore up their case and win indictments.
Sandusky, 71, is serving 30 to 60 years in the state�s �Supermax� prison in Waynesburg. Wearing a red prison jumpsuit, his wrists cuffed in front, he was escorted from a van into Centre County Court, where his wife Dottie sat near the front of the courtroom.
The hearing was called after Sandusky asked Senior Judge John Cleland, who presided at the 2012 trial, to allow him to research whether his rights were violated so that he could appeal the conviction.
Cleland has yet to rule on Sandusky�s request for permission to research people connected to his case, including former state Attorney General Tom Corbett and Sara Ganim, the Harrisburg Patriot-News reporter who broke the Sandusky story in 2011 and won a Pulitzer Prize.
Kane, who separately faces criminal charges for leaking grand jury information to the media, earlier this week released emails from now-retired judge Barry Feudale, who oversaw the Sandusky grand jury.
She said the emails show �Judge Feudale�s overriding concern was how to leak sealed Supreme Court documents without getting caught.�
Cleland ordered Kane to turn over by Nov. 4 any evidence supporting her contention or appear in court on Nov. 5.
�We intend to comply with Judge Cleland�s order,� Kane�s office said in an email.
Lindsay cheered the order, saying, �If there was prosecutorial abuse, the remedy is to dismiss the case� I�m going to say he�s a free man.�
The hearing took place a day after a different judge ruled that the statute of limitations did not bar prosecutors from considering possible new charges against Sandusky now that another man had come forward to say he was molested at a football camp in 1988.
(Editing by Barbara Goldberg and Bernadette Baum)
