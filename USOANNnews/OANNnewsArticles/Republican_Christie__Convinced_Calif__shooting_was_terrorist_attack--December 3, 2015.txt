Search for:
Republican Christie: Convinced Calif. shooting was terrorist attack
U.S. Republican presidential candidate and New Jersey Governor Chris Christie speaks at an event to announce a coalition of law enforcement officials supporting his campaign in Concord, New Hampshire November 30, 2015. REUTERS/Brian Snyder
December 3, 2015
WASHINGTON (Reuters)  U.S. Republican presidential hopeful Chris Christie said on Thursday he was convinced the shooting in San Bernardino, California, was a terrorist attack.
If a center for the developmentally disabled in San Bernardino, California, can be a target for a terrorist attack, then every place in America is a target for a terrorist attack, Christie, who is the governor of New Jersey, said before a forum at the Republican Jewish Coalition in Washington.
(Reporting by Emily Stephenson; Editing by Jeffrey Benkoe)
