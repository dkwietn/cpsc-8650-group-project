Search for:
Kentucky governor to dismantle state�s health insurance exchange: newspaper
Matt Bevin (R-KY) speaks to a gathering at FreePAC Kentucky in Louisville, Kentucky, April 5, 2014. REUTERS/John Sommers II
January 12, 2016
(Reuters) � Kentucky Governor Matt Bevin has notified U.S. authorities that he plans to dismantle the state�s health insurance exchange created under the Obama Administration�s Affordable Care Act, the Courier-Journal reported on Monday.
Bevin�s office was not immediately available for comment.
Bevin, a Republican, has said his goal is to complete the transition of the state�s system, known as �kynect,� by the end of 2016 to the federal system, the Louisville newspaper said.
Bevin had pledged to dismantle the health exchange authorized by executive order of his predecessor, Democrat Steve Beshear. Bevin�s letter said he wants the transition to the federal site to occur �as soon as is practicable,� the newspaper said.
(Reporting by Jon Herskovitz; Editing by Leslie Adler)
