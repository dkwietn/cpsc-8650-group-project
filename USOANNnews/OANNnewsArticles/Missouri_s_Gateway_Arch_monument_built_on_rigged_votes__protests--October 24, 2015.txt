Search for:
Missouri�s Gateway Arch monument built on rigged votes, protests
The Gateway Arch is seen in St. Louis, Missouri in this October 25, 2011 file photo. REUTERS/Jim Young/Files
October 24, 2015
By Greg Bailey
ST. LOUIS (Reuters) � When the last section of St. Louis� Gateway Arch, an American landmark as iconic as the Statue of Liberty, was hoisted into place on Oct 28, 1965, it was the concluding act of a story that began more than 30 years earlier.
From rigged votes in a bond election to fund the project, to a famous father-son mix-up when the winning architect was alerted, to a civil rights protest high above ground, some of the Arch�s rich, complicated history during those three decades has largely been forgotten as the 630-foot-tall, stainless steel structure nears its 50th anniversary.
The monument, meant to celebrate America�s western expansion, has taken on different interpretations over its lifetime.
Bob Moore, resident historian at the Arch, called the structure �an icon of the modern age,� but said it also represented the taking of land from Native Americans and Hispanic people.
While today�s television viewers may know the Arch better as the centerpiece of the SyFy series �Defiance,� the seed for its creation was planted in 1933, when attorney Luther Ely Smith, returning by train to his home town, saw the blight of the riverfront and proposed building the structure.
Two years later, city leaders, eager for jobs and federal money, asked voters to approve a bond issue in an election which had an unusually high turnout and approval rate. The St. Louis Post-Dispatch investigated the results and found the vote was fraudulent, winning a Pulitzer Prize for its revelation.
Starting in 1939, 37 blocks of the city�s riverfront were demolished, but wartime priorities left the space unused until a design competition following World War II.
CONFUSION
The competition opened in 1947, attracting 172 entries with ideas ranging from abstract sculpture to a covered wagon. Entries came from Finnish-American architect Eero Saarinen and separately from his noted father Eliel, both living in the Detroit area.
The committee picked Eero�s design of a catenary arch as the winner, but mistakenly alerted Eliel he had won. Eero and his team took home a combined $90,000 for a design that Smith called a �brilliant forecast into the future.� A catenary is a chain that supports its own weight.
Still, Saarinen�s design quickly came under fire, with some saying it looked too much like the arch planned for the 1942 Rome Exposition in Fascist Italy, while others described it as a giant croquet wicket.
Design critics didn�t slow the plans, but a lack of funding did as the start of construction was delayed until 1963, two years after Saarinen died from a brain tumor.
Lawsuits also delayed the building of the Arch, which ended up costing about $13 million.
Construction of the constantly curving structure, the world�s tallest in stainless steel, was a challenge as the bases of the two legs could not be off even as little as 1-64th of an inch or the final pieces would not fit together properly.
Underwriters had predicted more than 10 people would die during construction, but in fact, no one did.
Civil rights activists were unhappy with the limited number of minorities working on the project, given the involvement of federal funds.
On July 14, 1964, Percy Green and another man climbed 125 feet up the unfinished north leg to protest the exclusion of African Americans in the construction work force.
Speaking about his protest at an event in September, Green said only a few token blacks were hired, but climbing the Arch was �an embarrassment to the establishment.�
The Arch grounds and underground museum are now undergoing major renovations, with $380 million in public and private funding.
For the 50th anniversary, the National Park Service, which oversees the monument, plans to quietly celebrate with 1,000 free cupcakes and $1 rides to the top of the Arch, the original price.
(Reporting by Greg Bailey; Editing by Ben Klayman and Bernadette Baum)
