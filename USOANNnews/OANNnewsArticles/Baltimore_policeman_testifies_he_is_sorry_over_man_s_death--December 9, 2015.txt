Search for:
Baltimore policeman testifies he is sorry over man�s death
Baltimore Police officer William Porter (R) approaches the court House in Baltimore, Maryland, November 30, 2015. REUTERS/Rob Carr/Pool
December 9, 2015
By Donna Owens
BALTIMORE (Reuters) � A Baltimore police officer charged in the April death of a black man from a spinal injury suffered in custody said at his trial on Wednesday that he was sorry about the death.
Officer William Porter, 26, who faces manslaughter and other charges in the death of Freddie Gray, said during more than four hours on the witness stand that he had become acquainted with Gray while patrolling the man�s crime-ridden neighborhood.
Asked by defense lawyer Gary Proctor if he was sorry about Gray�s death, Porter told a packed courtroom, �Absolutely. Sorry to see that, any loss of life.�
Gray�s death triggered rioting in Baltimore and fueled concern about police tactics in the United States. Porter is the first of six officers, three of them black, to face trial in Baltimore City Circuit Court.
Porter, who is black, testified he had seen no injuries when Gray, 25, told him in the back of a police van that he needed medical help. Gray was unable to give him a reason to seek aid, he said.
Officers arrested Gray after a foot chase. He was bundled into a transport van while in handcuffs and shackles and was not secured with a seat belt. Gray died a week later.
In a point of contention with prosecutors, Porter said Gray never told him that he could not breathe.
Police investigator Syreeta Teel testified that Porter told her a few days after the injury that Gray had said he could not breathe.
But Porter did not say that in a later statement to investigators, according to testimony. Under cross-examination, Porter rejected Teel�s account.
Porter was present at five of the six stops the van made, including one in which he lifted Gray onto the van�s bench. Gray was not breathing at the last stop, a police station.
Porter said he had not been shown at the police academy how to put a seat belt on detainees. He said it was not common practice to put belts on.
A defense witness, Officer Zachary Novak, who helped place Gray in the van and administered first aid at the station, testified that van seat belts were only used about 10 percent of the time.
The defense has said Porter did not believe Gray was seriously injured until the final stop. His lawyers have suggested that other officers were responsible for seeking help.
Charges against the other officers range from misconduct to second-degree murder.
(Reporting by Ian Simpson and Donna Owens; Editing by Jonathan Oatis and Frances Kerry)
