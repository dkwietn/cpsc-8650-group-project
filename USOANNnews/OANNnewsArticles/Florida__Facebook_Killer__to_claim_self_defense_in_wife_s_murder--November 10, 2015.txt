Search for:
Florida �Facebook Killer� to claim self defense in wife�s murder
Derek Medina appears in court for a hearing in Miami, Florida October 15, 2013. REUTERS/Emily Michot/Pool
November 10, 2015
By Zachary Fagenson
MIAMI (Reuters) � A Florida man accused of shooting his wife and posting an image of her lifeless body on Facebook was acting in self defense, his lawyer plans to argue at the start of his Miami murder trial on Tuesday.
Derek Medina, 33, has pleaded not guilty to first degree murder and faces life in prison if convicted of the August 2013 shooting.
During the trial, expected to last two weeks, defense attorney Saam Zangeneh said he will introduce evidence that his wife used drugs, regularly fought with Medina, and may have been involved in Satanic worship.
Prosecutors say Medina was a boxer who should not have been afraid of his wife, according to court documents.
Before turning himself in after the killing, Medina wrote on Facebook: �I�m going to prison or death sentence for killing my wife. Love you guys, Miss you guys � My wife was punching me, and I am not going to stand any more with the abuse so I did what I did. I hope u understand me.�
In an arrest affidavit police say Medina confessed to shooting then 27-year-old Jennifer Alfonso multiple times after a verbal dispute turned violent when she began kicking him and punching him with a closed fist.
Medina also told investigators that Alfonso had armed herself with a knife during their argument, but said that was after he had gone to his bedroom for a gun with which he confronted her.
A medical examiner�s report released in 2013 found Medina shot Alfonso several times at a downward angle at point blank range.
Medina authored six online books on topics that include marriage counseling, and had been employed on the front desk at a luxury Miami-area condominium. The couple married in 2010, divorced in early 2012, then remarried a few months later.
Defense attorneys and prosecutors spent five days questioning more than 150 potential jurors on their familiarity with the case, and opinions on gun ownership.
Lawyers for Medina, who has cut his long black hair and beard ahead of the trial, prodded them on whether a man could be a domestic violence victim.
Zangeneh said the lawyers did not seek to move the trial�s location despite intense local media coverage.
(This version of the story corrects last paragraph to show lawyers did not seek to relocate trial)
(Editing by David Adams)
