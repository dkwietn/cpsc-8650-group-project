Search for:
Suspects in California yoga teacher�s death to be charged in second murder
A combination photo of Sean Angold, 24, Lila Alligood, 18, and Morrison Lampley, 23, are shown in these police booking photos provided by Multnomah County Sheriff's Office in Portland, Oregon October 9, 2015. REUTERS/Multnomah County Sheriff's Office/Handout
October 9, 2015
By Curtis Skinner
SAN FRANCISCO (Reuters) � Three drifters � two men and a woman � arrested as suspects in this week�s killing of a yoga teacher on a hiking trail also will be charged in the shooting death of a Canadian woman in San Francisco�s Golden Gate Park last week, authorities said.
Lila Allgood, 18, Morrison Lampley, 23, and Sean Angold, 24, will be charged with murder and robbery in the killing of Audrey Carey, 23, from the Canadian province of Quebec, San Francisco police spokeswoman Grace Gatpandan said late on Thursday.
Carey�s body was found by a passerby last Saturday morning in Golden Gate Park ahead of the second day of the three-day Hardly Strictly Bluegrass music festival taking place there, police said.
The suspects were found in possession of Carey�s property when they were arrested on Tuesday in Portland, Oregon in connection with the shooting death of tantric yoga teacher Steve Carter, 67, on a hiking trail north of San Francisco, the Marin County Sheriff�s Office said.
The sheriff�s office said the weapon used in Carter�s shooting in Fairfax, California was recovered at the time of their arrest. A surveillance camera caught images of the three suspects at a gas station putting fuel in Carter�s car, which had been stolen from the crime scene, authorities said.
The Chronicle reported that Carter had recently returned to the United States from Costa Rica to care for his wife, who is undergoing treatment for an aggressive form of breast cancer.
Carter�s dog, a brown doberman pincher, was wounded in the shooting but survived, authorities said.
(Reporting by Curtis Skinner in San Francisco; Editing by Will Dunham)
