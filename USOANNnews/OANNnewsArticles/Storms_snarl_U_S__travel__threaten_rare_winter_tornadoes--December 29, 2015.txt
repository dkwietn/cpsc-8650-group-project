Search for:
Storms snarl U.S. travel, threaten rare winter tornadoes
Law enforcement has the bridge closed while city workers place sand bags on the bridge to stop seepage in Elba, Alabama, December 26, 2015. REUTERS/Marvin Gentry
December 29, 2015
By Mary Wisniewski
CHICAGO (Reuters) � Snow, sleet and hail snarled transportation across swaths of the United States on Monday during one of the busiest travel weeks, after dozens died in U.S. storms that were part of a wild worldwide weather system seen over the Christmas holiday period.
More than 40 people were killed by tornadoes and floods in the United States during the holiday season, where rare winter tornado warnings were issued in Alabama on Monday.
Alabama, Mississippi and the Florida panhandle were expected to bear the brunt of the of the day�s strongest storms, AccuWeather senior meteorologist Michael Leseney said.
About 2,900 flights had been canceled at U.S. airports by 11 p.m. EST on Monday, according to FlightAware.com, while another 4,800 delays were reported.
Chicago-area airports were worst hit with hundreds of flights canceled as the city was swept by sleet and hail and United Airlines granted exemptions from fees for some travelers impacted by the storms.
More than a foot (30 cm) of snow was forecast for southwestern Wisconsin and southeastern Minnesota, and snow was also falling in Iowa, Nebraska and Missouri.
A flash flood warning was in effect in eastern Missouri and Southern Illinois, the National Weather Service said. Thirteen people died in flash floods in those two states during the weekend, including four international soldiers training at a military base in Missouri, the Army said.
The storms came as other countries struggled with extreme weather and stressed holiday infrastructure.
In Britain, hundreds of troops were deployed and a government agency said a �complete rethink� of flood defenses was needed after swathes of northern England were inundated by rivers that burst their banks.
Severe weather also hit parts of Australia, where more than 100 homes were lost in Christmas Day brushfires.
Then on Sunday a freight train carrying sulphuric acid derailed in the Outback, and a Queensland Rail spokeswoman told local media that floods had stopped crews reaching the scene. (video: http://reut.rs/1R3QYwT)
LIVES AND HOMES LOST
The bad weather caused two candidates for the Republican presidential nomination, New Jersey Governor Chris Christie and U.S. Senator Marco Rubio, to cancel campaign events in Iowa.
In Arkansas, a 31-year-old man drowned in a flood-swollen creek about 65 miles northwest of Little Rock, authorities said on Monday. Six tornadoes were reported on Sunday � three in Arkansas, one in Texas, and two in Mississippi.
U.S. President Barack Obama, on vacation in Hawaii, called Texas Governor Gregg Abbott on Monday to receive an update and to offer his administration�s continued support after weekend tornadoes that killed at least 11 people in the Dallas area and damaged about 1,600 structures and homes.
One twister in the city of Garland, Texas, had winds of up to 200 miles per hour (322 km per hour) and killed eight people, including a 30-year-old woman and her year-old son.
�We are very blessed that we didn�t have more injuries and more fatalities,� Garland�s Mayor Douglas Athas told CNN.
�RIPPED OUR WORLD APART�
In the Dallas suburbs of Garland and Rowlett, which were devastated by tornadoes on Saturday, many residents turned to social media to tell stories of survival and to ask for help finding lost pets.
Briana Landrum posted a photo of her living room couch surrounded by wreckage where her house once stood in Rowlett. Her two cats are missing, she wrote, and the freezing rain has made searching for her �sweet babies� difficult.
�The roof fell on us one second and the next, it was gone,� she wrote. �The tornado ripped our world apart.�Ten deaths and 58 injuries were reported in Mississippi, and hundreds of homes were damaged, authorities said. One man recounted how a tornado swept his adult son through treetops before dropping him hundreds of feet away.
In flooded southern Missouri, dozens of adults and children forced from their homes took refuge at Red Cross shelters. Four international soldiers receiving training at the Fort Leonard Wood military base drowned when their vehicle was swept into flood waters on Sunday, the Army said in a statement. Their identities and home countries have not yet been released.
Red Cross spokeswoman Julie Stolting said there was no telling when displaced people might be able to return home. �But we�re feeding them, we�re sheltering them, we�re providing health services,� she said.
In Oklahoma, Governor Mary Fallin extended a state of emergency for all 77 counties on Monday after freezing rain, ice and sleet left nearly 200,000 homes without power.
(Reporting by Mary Wisniewski in Chicago, Heide Brandes in Oklahoma City, Jon Herskovitz in Austin, Texas, Letitia Stein in Tampa, Florida, Lisa Maria Garza in Dallas, Laila Kearney in New York, Sara Catania and Dan Whitcomb in Los Angeles, Emily Stephenson, and Jeff Mason in Hawaii; Writing by Mary Wisniewski and Daniel Wallis; Editing by Bill Trott and Diane Craft)
