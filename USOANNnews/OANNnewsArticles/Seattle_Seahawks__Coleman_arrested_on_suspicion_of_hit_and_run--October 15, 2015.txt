Search for:
Seattle Seahawks� Coleman arrested on suspicion of hit-and-run
Sep 4, 2014; Seattle, WA, USA; Seattle Seahawks fullback Derrick Coleman (40) beats Green Bay Packers cornerback Tramon Williams (38) to the end zone for a 15-yard touchdown reception during the fourth quarter at CenturyLink Field. Joe Nicholson-USA TODAY Sports
October 15, 2015
By Eric M. Johnson and Steve Ginsburg
(Reuters) � Seattle Seahawks fullback Derrick Coleman has been arrested on suspicion of vehicular assault and hit-and-run, both felonies, police said on Thursday, and the NFL team has suspended him indefinitely.
Coleman, 24, was being held without bail pending a court hearing in Seattle�s King County Correctional Facility as of Thursday afternoon, according to jail records.
Police in Bellevue, a Seattle suburb, said Coleman�s fast-moving pickup truck collided with the back of a car on Wednesday evening, shoving the car up an embankment and onto a retention wall, where it came to a rest upside down.
The car�s driver was transported to an area hospital and treated for serious but non-life-threatening injuries.
After his truck came to rest on its side, police said a shoeless Coleman fled the scene on foot and was tracked down a number of blocks from the crash scene.
�He left the scene of an accident where a person was injured and he had a duty under Washington law to stop and render aid,� Bellevue Police Chief Steve Mylett told a news conference.
�He also had the duty to provide his personal information, his driver�s license number and insurance, and he failed to do that,� Mylett said.
The Seahawks suspended Coleman indefinitely pending further information, the team said.
Coleman is used primarily as a blocker and has one run and one catch this season for Seattle. He has played in 22 games since joining the Seahawks in 2013, with six starts, two this year. Coleman missed 11 games last season with a broken foot.
He played on the Seattle team that won the Super Bowl in 2014.
According to his personal website, Coleman is the only hearing-impaired player currently in the National Football League, the only legally deaf player in the history of the NFL, and he spends time supporting deaf youth.
Mylett said Coleman was cooperative with police, did not report any injuries and was arrested on felony charges of vehicular assault and hit-and-run. The results of a blood test to determine whether he had taken drugs or alcohol were pending.
A representative for Coleman said in a statement that he may have fallen asleep behind the wheel on the way home from a Seahawks facility in the Seattle area.
(Reporting by Eric M. Johnson in Seattle and Steve Ginsburg in Washington; Editing by Daniel Wallis and Eric Beech)
