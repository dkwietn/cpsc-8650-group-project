Search for:
Once �King,� cotton farming on a long decline in U.S. south
An empty and decaying home sits next to a field of cotton in Danville, Georgia October 28, 2015. REUTERS/Brian Snyder
November 17, 2015
By Chris Prentice
CLARKSDALE, Miss. (Reuters) � Fields along the Mississippi River Delta once gleamed white in the autumn with acre upon acre of cotton ready to be picked.
But to see the decline of a cash crop once nicknamed �King Cotton� one need look no further than the 300 acres (121 hectares) that Michael Shelton farms in Clarksdale, Mississippi, about 75 miles (120 km) down river from Memphis.
The fields were recently cleared of wheat and soybeans, and just one long row of cotton, which Shelton, 65, said he planted �for memory.�
�I wanted to say I planted cotton every year,� said Shelton, who is black and whose property includes the 40 acres (16 hectares) his ancestors acquired in the late 19th century, not long after the abolition of slavery.
With cotton prices near their lowest in six years, Shelton is far from alone in cutting back on the crop.
U.S. farmers this year planted the fewest acres of cotton since 1983, according to U.S. Department of Agriculture data. In the southern states of Mississippi, Tennessee, Alabama and Arkansas, once the heart of cotton country, growers expect to harvest some of their smallest crops since the year after the U.S. Civil War ended, according to the oldest government data available.
It�s not just low prices driving down cotton planting. This year marks the first the U.S. cotton farmers are getting by without a subsidy program that had long been the subject of a trade dispute between Washington and Brazil.
For Shelton, the only one of eight siblings to go into farming, that is just the latest in a long line of hits his business has taken.
�One thing I�ve found lately is it�s become an expensive business,� Shelton said.
�NOT YOUR FATHER�S FARM BILL�
Many of the United States� remaining 18,000 cotton farms � a number that�s fallen by half in less than 20 years � see themselves on the losing end of that long skirmish at the World Trade Organization.
Washington paid $300 million to Brazil to settle the subsidy squabble and agreed to stop subsidy payment programs to cotton farms that totaled about $576 million in fiscal 2013, according to Congressional Budget Office estimates.
The farm law that passed last year phased out payments to farmers of many crops, leaving growers more exposed to market conditions.
U.S. Senator Debbie Stabenow of Michigan of the Senate Committee on Agriculture and Nutrition described the shift as �not your father�s farm bill.�
�The political climate has changed, that�s hurt cotton�s standing,� said John Robinson, an agricultural economist with Texas A&M University. �The old world doesn�t exist. That�s gone with the wind.�
It has left some farmers feeling exposed to low prices and the potential of rising debt levels, ultimately raising the prospect of further exodus from the fiber in areas where growers can grow food crops such as corn, wheat and soybeans.
Don Shurley, a cotton economist with the University of Georgia said the problem was an economic one: �Can cotton remain profitable to keep farmers growing it and stay in business?�
With the price of cotton running at about 60 cents per pound, down 35 percent from 2014 highs, many farmers are finding it is costing them more to grow the crop than they earn.
Falling demand has also taken a toll, with global consumption down 9 percent from a peak of 122.5 million bales nine years ago.
The industry�s most pervasive worry ultimately is not cost, but consumption. Cotton has struggled to recover demand lost amid price spikes in 2008, 2010, and 2011, which drove consumers towards clothes made of other fibers, such as polyester and nylon.
�SMALL GUY CAN�T SURVIVE�
Other Southern cotton farmers have coped with the long slide in prices by planting ever more acres, following the lead of their Midwestern counterparts who plant corn and wheat, and eke out slightly more profit from larger-scale operations.
Ronnie Lee, of Dawson, Georgia, has taken the large-scale route. Since starting out farming 35 acres (14 hectares) in 1987, he and now his three sons have steadily grown their operation to 30 times the size of Shelton�s.
Today they plant some 9,000 acres (3,642 hectares) of owned and leased land, as well as picking cotton for others with their three $600,000 mechanical pickers, operating a crop-dusting service and running a gin that will pack as many as 90,000 bales of cotton this year.
Despite the size, Lee emphasizes that his business is still a family farm.
�We�re a true family farming operation,� he said during a rare lunch break on a recent rainy afternoon. �Why do I have to be small to be a family operation? A small guy today can�t survive.�
(This story has been corrected to fix miles/kilometers conversion in paragraph 2)
(Reporting by Chris Prentice; Editing by Scott Malone and Chizu Nomiyama)
