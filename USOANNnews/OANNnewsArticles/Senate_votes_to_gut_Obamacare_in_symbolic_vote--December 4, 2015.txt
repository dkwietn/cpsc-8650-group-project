Search for:
Senate votes to gut Obamacare in symbolic vote
Cathey Park of Cambridge, Massachusetts wears a cast for her broken wrist with "I Love Obamacare" written upon it prior to U.S. President Barack Obama's arrival to speak about health insurance at Faneuil Hall in Boston October 30, 2013. REUTERS/Kevin Lamarque
December 4, 2015
WASHINGTON (Reuters) � After five years of failed attempts, U.S. Senate Republicans on Thursday passed a symbolic bill to gut President Barack Obama�s signature healthcare reform law, but Obama has vowed to veto the measure.
The Republican-controlled Senate voted 52-47 to repeal several core Obamacare provisions under special budget procedural rules that allow for passage with a simple majority rather than the 60-vote threshold needed for most legislation in the Senate.
(Reporting by David Lawder; Editing by Eric Walsh)
