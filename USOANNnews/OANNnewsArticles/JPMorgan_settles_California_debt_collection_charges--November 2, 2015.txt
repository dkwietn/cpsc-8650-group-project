Search for:
JPMorgan settles California debt collection charges
A view of the exterior of the JP Morgan Chase & Co. Corporate headquarters in the Manhattan borough of New York City, May 20, 2015. REUTERS/Mike Segar
November 2, 2015
By Jonathan Stempel
(Reuters) � JPMorgan Chase & Co will pay $50 million to settle charges that it engaged in abusive credit card debt collection practices against tens of thousands of people in California, state Attorney General Kamala Harris said on Monday.
California was one of three U.S. states, along with Mississippi and Wyoming, that did not join JPMorgan�s $216 million settlement in July of related charges by the federal government, the other 47 states and Washington, D.C.
The California settlement includes a $5 million fine plus $45 million to be used at Harris� discretion, court papers show.
It resolves claims that JPMorgan tried to collect incorrect sums, sold bad credit card debt, engaged in �robosigning� of thousands of court documents it never reviewed, and improperly obtained default judgments against military personnel.
The accord would end a lawsuit filed against the largest U.S. bank in the Los Angeles County Superior Court in May 2013. Court approval is required.
JPMorgan did not admit wrongdoing in agreeing to the settlements. It has said it stopped filing lawsuits to collect credit card debt in 2011.
A bank spokesman said the Mississippi and Wyoming cases have not been resolved. The attorneys general for those states did not immediately respond to requests for comment.
The July settlement included payments of $136 million to the U.S. Consumer Financial Protection Bureau, $30 million to the U.S. Office of the Comptroller of the Currency and $50 million in consumer refunds. It also required JPMorgan to stop trying to collect on more than 528,000 consumer accounts.
California�s share of the refunds is about $10 million, Harris said.
(Reporting by Jonathan Stempel in New York; Editing by David Gregorio)
