Search for:
Georgia grand jury probes police killing of unarmed, naked black man
People protest over the police shooting of a man named Anthony Hill as they carry his photo in Decatur, Georgia March 11, 2015. REUTERS/Tami Chappell
October 22, 2015
By David Beasley
ATLANTA (Reuters) � A Georgia grand jury on Thursday reviewed the shooting death of an unarmed, naked black man by a white police officer at a suburban Atlanta apartment complex last March, prosecutors and the family�s attorney said.
The death of Anthony Hill, 27, came as a spate of killings in the United States have raised questions about excessive use of force by police, particularly against black men.
No charges have been brought so far against the officer, Robert Olsen of the DeKalb County police department, who has been on administrative leave since the incident.
Hill was a U.S. Air Force veteran who suffered from bipolar disorder, according to his relatives. He was shot on March 9 after police found him crawling naked, knocking on apartment doors and �acting deranged,� DeKalb County police said.
Hill ran toward the officer, who ordered him to stop before shooting him twice, according to police. Hill�s hands were outstretched, but not raised, before he was shot in the chest, Hill�s family attorney, Christopher Chestnut, said on Thursday.
Some witnesses initially thought his hands were up.
The officer testified before the grand jury for about an hour Thursday, saying he felt threatened by Hill, according to Chestnut. Olsen cited cases of police shootings involving nude suspects who were on drugs and attacked police.
But Hill was not on drugs, nor threatening, Chestnut said.
�Anthony was nude but he was happy,� he said, noting that Olsen had other options such as using an electric stun gun, �but he chose not to use them.�
Relatives said Hill may have stopped using some of the seven medications he was taking for his condition after his medical discharge from the military in 2013.
Thursday�s grand jury does not have the authority to indict Olsen, but can recommend whether the case should proceed to a criminal grand jury, said Ryan Julison, spokesman for Hill�s family attorney.
Through a spokesman, DeKalb County District Attorney Robert James declined to comment before the grand jury deliberations. It could be a week or more before the grand jury recommendation is released, said his spokesman, Erik Burton.
(Editing by Letitia Stein and Eric Walsh)
