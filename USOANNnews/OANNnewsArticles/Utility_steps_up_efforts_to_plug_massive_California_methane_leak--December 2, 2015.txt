Search for:
Utility steps up efforts to plug massive California methane leak
SoCal Gas President and CEO Dennis Arriola apologizes during his statements as the Los Angeles City Council conducts testimony on the ongoing natural gas leak in the Porter Ranch area of Los Angeles, California December 1, 2015. REUTERS/Gus Ruelas
December 2, 2015
By Diana Crandall
LOS ANGELES (Reuters) � The head of Southern California Gas Co said it would take at least three more months to plug a massive underground leak of natural gas that has been seeping into the air since mid-October and now accounts for a quarter of the state�s entire methane emissions.
The utility�s president and chief executive officer, Dennis Arriola, also said on Tuesday the company would begin this week drilling a relief well designed to intersect the damaged pipeline hundreds of feet beneath the surface and inject it with fluids and cement.
The utility�s latest strategy and time frame for addressing the stench of gas fumes that have sickened nearby residents for weeks and led to the temporary relocation of 200 families was laid out during a Los Angles City Council hearing.
SoCal Gas, one of the biggest gas utilities in the nation, is owned by San Diego-based Sempra Energy. Its leaking storage field at Aliso Canyon, just outside the northern Los Angeles community of Porter Ranch, is the second largest such facility in the Western United States by capacity, after a field in Montana.
The company pumps gas into storage wells some 8,500 feet below the site during the summer and draws on those supplies to meet higher energy demand in the winter.
The leak, detected on Oct. 23, is believed to have been caused by a broken injection-well pipe several hundred feet beneath the surface of the 3,600-acre field.
Vast amounts of methane, the principal component of natural gas and a far more potent greenhouse gas than carbon dioxide, have been seeping through the soil to the surface since then, despite several failed attempts by the company to halt the flow.
The scope and complexity of the natural gas leak at Aliso Canyon is unprecedented in California, said Tim O�Connor, state oil and gas program director for the Environmental Defense Fund.
The state Air Resources Board has estimated that methane is escaping into the environment at the rate of 50,000 kilograms per hour, representing roughly one fourth of all methane emissions throughout California.
To date, the gas field has belched the equivalent of 800,000 metric tons of carbon dioxide, about the same emissions from driving 160,000 cars for a year, according to the agency.
Arriola said it could take three to four months more to halt the flow but that once the leak is sealed off the crippled well will be permanently abandoned. Residents living about a mile from Aliso Canyon want the gas field shut down altogether.
The South Coast Air Quality District said it has received nearly 1,000 complaints about the persistent rotten-egg smell of mercaptan, a chemical added to the normally odorless gas to help detect leaks. Many also have reported suffering from nausea, nose bleeds, headaches, breathing difficulties and other ill effects.
In addition to households the company has already paid to temporarily relocate, 500 more families are waiting to be moved, said Stephanie Saporito, spokeswoman for City Councilman Mitchell Englander, whose district includes Porter Ranch.
The utility insists that the leak, while a major public nuisance, poses no immediate public safety threat because the gas dissipates outdoors. But county health officials said long-term health effects from exposure to the gas remain unknown.
Environmentalists have seized on the leak as evidence of an aging fossil fuel energy infrastructure that has led to thousands of smaller gas leaks throughout the system that have gone largely unaddressed.
(Reporting by Diana Crandall; Writing and additional reporting by Steve Gorman; Editing by Leslie Adler)
