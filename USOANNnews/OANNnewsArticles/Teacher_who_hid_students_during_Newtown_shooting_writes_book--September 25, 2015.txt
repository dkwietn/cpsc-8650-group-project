Search for:
Teacher who hid students during Newtown shooting writes book
Sandy Hook teacher Kaitlin Roig-DeBellis is pictured during an interview in her home in Greenwich, Connecticut December 4, 2013. REUTERS/Michelle McLoughlin
September 25, 2015
By Richard Weizel
NEWTOWN, Conn. (Reuters) � The Sandy Hook Elementary School teacher who saved 15 first-grade students during one of America�s worst mass shootings has written a book about the experience, hoping it will inspire readers to stay positive through life�s worst challenges.
In excerpts from the book �Choosing Hope:�Moving Forward from Life�s Darkest Hours,� Kaitlin Roig-Debellis describes quietly leading her students into�a�tiny�bathroom at�the school�as gunfire erupted around them, and waiting until the attack that ultimately killed 26 people ended.
The book will be published Oct. 6.
The day of the shooting, Dec 14, 2012, �started out as a typically happy one,� Roig-DeBellis wrote in an excerpt published on Friday in People Magazine.�She played �Oh, What a Beautiful Mornin�,� and greeted the children gathered for their morning meeting with the words �Good morning fantastic friends!�
�The theme that day was Our Holiday Traditions. [The students] talked about making cookies and visiting Santa and listening for reindeer,� she wrote.
Minutes later the tranquil morning was shattered by the sound of gunshots. She�closed the door of the classroom,�turned off the lights and packed the 15 first graders into a�small adjacent bathroom, fearing death.
�I tell them how much they have meant to me. �I need you to know that I love you all very much,� I say. �Anyone who believes in the power of prayer needs to pray right now,� I say, �and anyone who does not, needs to think really happy thoughts,'� she wrote in the excerpt.
She said she used a storage unit to barricade the door.
On that day, former student Adam Lanza killed 20 pupils and six staff, after first shooting to death his mother. Lanza, 20, later turned the gun on himself as police arrived at the school.
All of�Roig-DeBellis� pupils survived.
Roig-Debellis said she wrote the book as a way to put that traumatic day behind her, and to help others to overcome their own difficult moments in life.
�Life is all about choice,� she told People Magazine. �The choice is each of ours alone to make. That�s really powerful, especially when dealing with the hard stuff.��
(Editing by Richard Valdmanis and Lisa Lambert)
