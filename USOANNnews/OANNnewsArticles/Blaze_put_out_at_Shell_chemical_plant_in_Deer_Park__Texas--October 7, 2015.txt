Search for:
Blaze put out at Shell chemical plant in Deer Park, Texas
The logo of Shell is pictured at the 26th World Gas Conference in Paris, France, June 2, 2015. REUTERS/Benoit Tessier
October 7, 2015
HOUSTON (Reuters) � A small fire was quickly extinguished Tuesday night at a Hexion Inc [MOMEPM.UL] resin processing plant within Royal Dutch Shell Plc�s refining and petrochemical complex in Deer Park, Texas, a Hexion spokesman said.
There were no injuries, he added.
A Shell spokesman said operations at the 316,000 barrel-per-day (bpd) Deer Park refinery and the adjoining chemical plant were not affected by the fire.
�Shell emergency response personnel assisted this evening with a fire at the Hexion chemical facility in Deer Park, Texas,� said Shell spokesman Ray Fisher. The fire was quickly extinguished with no reported impact to people or the community. Hexion is a third party facility not owned or operated by Shell.
Sources familiar with Deer Park operations said the Hexion plant takes resins from Shell�s chemical plant and processes them into products for other customers.
The Hexion plant is not involved in operations at the refinery, which is a joint venture between Shell and Mexico�s national oil company Petroleos Mexicanos (Pemex) [PEMX.UL].
Shell is the managing partner of the refinery, according to the sources.
(Reporting by Erwin Seba; Editing by Eric Walsh and Himani Sarkar)
