Search for:
New York gas blast and fire kill one, injure three
New York Police Department (NYPD) officers and firefighters respond to an explosion and house fire the Brooklyn borough of New York, October 3, 2015. REUTERS/Rashid Umar Abbasi
October 4, 2015
NEW YORK (Reuters) � A gas explosion and fire that ripped off the facade of a three-story building in New York on Saturday, killing one person and injuring three, might have been caused by a tenant moving out a stove, fire officials said.
More than 200 firefighters battled the four-alarm blaze in the borough of Brooklyn after it was reported to the New York Fire Department at 1:05 p.m., Mayor Bill de Blasio and Fire Commissioner Daniel Nigro told a news conference.
Police said first responders found an unidentified woman dead in the stairwell of the building. Three people on the sidewalk outside, men aged 34 and 27 and a nine-year-old boy, were also injured, officials said.
The injured were taken to hospital, one in critical condition, and the others with minor injuries.
Although severe structural damage had prevented fire officials from entering the building, Nigro said he thought the fire originated on the second floor.
�We have some indication that there was a stove exchange for the tenant. So that�s what we believe was involved.�
Nigro said the second-floor tenant had bought a high-end stove and was moving out of the apartment, a procedure that involved disconnecting the gas line.
The move had been approved, he added.
The New York fire department would make a full investigation into the cause of the explosion, de Blasio said.
In a statement, Governor Andrew Cuomo said he would direct state authorities to investigate the blast, which he likened to fatal gas explosions at buildings in Manhattan�s East Village and Harlem in recent years.
Area gas provider National Grid vowed to make the situation safe. �National Grid � is working under the direction of the Fire Department of New York to shut off the gas and make the situation safe,� spokeswoman Karen Young said in an email.
(Reporting by Katie Reilly and Laila Kearney; Editing by Richard Chang and Clarence Fernandez)
