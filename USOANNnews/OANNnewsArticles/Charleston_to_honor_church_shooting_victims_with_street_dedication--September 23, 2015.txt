Search for:
Charleston to honor church shooting victims with street dedication
Pall bearers release white doves over the casket of mass shooting victim Ethel Lance as she is buried at the Emanuel African Methodist Episcopal Church cemetery in North Charleston, South Carolina in a June 25, 2015 file photo. REUTERS/Brian Snyder/files
September 23, 2015
CHARLESTON, S.C. (Reuters) � A South Carolina city on Wednesday is honoring the victims of a shooting massacre by renaming part of the street in front of the historic black church where they died the �Mother Emanuel Way Memorial District.�
Nine people were shot and killed at Emanuel African Methodist Episcopal church in Charleston when a gunman opened fire during a June 17 Bible study session.
African Methodist Episcopal bishops planned to join hands in front of the church, nicknamed Mother Emanuel, to dedicate a sign over the street the church faces for the new district, city council member William Dudley Gregorie said in a statement.�
The Charleston City Council unanimously approved Gregorie�s recent resolution to rename two long blocks on Calhoun Street for the slain members of the congregation.
Their lives �give us a lasting legacy and living legacy of forgiveness that will guide not only the Mother Emanuel AME Church but also the citizens of the city, the state of South Carolina and the nation,� Gregorie said.
Charleston leaders have said they will announce plans on Thursday about how more than $2 million in donations the city collected after the shooting for the victims and the church will be distributed.
Dylann Roof, 21, has been charged with murder in the slayings, and state prosecutors have said they intend to seek the death penalty. Roof also faces 33 federal hate crime and weapons charges that could result in a death sentence as well.
(Reporting by Harriet McLeod; Editing by Colleen Jenkins and Eric Walsh)
