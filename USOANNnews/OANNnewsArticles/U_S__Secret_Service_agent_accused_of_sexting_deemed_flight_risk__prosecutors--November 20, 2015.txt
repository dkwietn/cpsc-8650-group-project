Search for:
U.S. Secret Service agent accused of sexting deemed flight risk: prosecutors
Lee Robert Moore is pictured in this undated booking photo provided by the Delaware Department of Justice. Moore, a U.S. Secret Service officer assigned to the White House, was arrested this week after he sent naked pictures of himself to someone he thought was a 14-year-old girl, according to a criminal complaint. REUTERS/Delaware Department of Justice/Handout via Reuters
November 20, 2015
(Reuters) � A White House Secret Service officer accused of sending naked pictures of himself to an undercover police officer posing as a 14-year-old girl should be held until trial, prosecutors said on Thursday.
The suspect, Lee Robert Moore, 37, of Church Hill, Maryland,
has admitted to police and prosecutors that he sent sexually charged online chats while working at the White House, according to documents filed in the U.S. District Court in Delaware.
Prosecutors asked that Moore be held until trial because he was a flight risk, a danger to the public and his crimes involved a minor. His lawyers asked that he be released to the custody of his family.
A detention hearing that had been set for Thursday was rescheduled for Dec. 2. Moore also waived a preliminary hearing to determine probable cause, the court documents said.
Charges against Moore include solicitation of a minor and attempting to transfer obscene material to minor. His arrest last week was the latest blow to the Secret Service after a series of scandals and security lapses have rocked the agency.
Court documents detail a series of explicit, pornographic online chats starting in late August between Moore and a Delaware State Police detective posing as a 14-year-old girl.
Moore sent naked photos of himself to the undercover officer and asked to meet to have sex, according to the documents. He also sent one video taken while wearing his uniform and working at the White House.
Moore communicated with as many as 30 teenage girls, which he believed were minors, from home and the White House for about a year, prosecutors said.
The Secret Service, the agency that protects the president, has placed Moore on administrative leave.
(Reporting by John Clarke; Editing by Sandra Maler)
