Search for:
Former U.S. President Carter responding well to cancer treatment
Former U.S. President Jimmy Carter arrives to speak during an event honoring former U.S. Vice President Walter Mondale hosted by the Humphrey School of Public Affairs at the University of Minnesota in Washington October 20, 2015. REUTERS/Joshua Roberts - RTS5DEJ
November 10, 2015
By David Beasley
ATLANTA (Reuters) � Former U.S. President Jimmy Carter is responding well to cancer treatment and there is no evidence of new malignancy, the Carter Center said on Tuesday.
Carter, 91, started treatment in August for melanoma that had spread from his liver to his brain.
After recent tests, doctors at Emory University�s Winship Cancer Institute gave Carter �good news,� the Carter Center said in a statement.
�They tell him that recent tests have shown there is no evidence of new malignancy, and his original problem is responding well to treatment,� the statement said. �Further tests will continue.�
Carter was planning to travel to Nepal this month on a home-building trip with the non-profit group Habitat for Humanity, but the trip was canceled because of �civil unrest� in the country.
Carter, president from 1977-1981, participated in a Habitat home-building event in Memphis, Tennessee, last week, the Carter Center said.
Carter played a key role in Middle East peace negotiations during his presidency. He won a Nobel Peace Prize in 2002.
(Reporting by Mohammad Zargham in Washington; Editing by David Adams and Eric Beech)
