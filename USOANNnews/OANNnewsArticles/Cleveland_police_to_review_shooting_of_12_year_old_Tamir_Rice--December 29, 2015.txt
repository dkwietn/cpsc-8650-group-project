Search for:
Cleveland police to review shooting of 12-year-old Tamir Rice
People take part in a protest against the police in Manhattan, New York, December 28, 2015 after a grand jury cleared two Cleveland police officers on Monday in the November 2014 fatal shooting of 12-year-old Tamir Rice. Rice was brandishing a toy gun in a park, and a prosecutor said there were a series of mistakes but no criminal activity. REUTERS/Eduardo Munoz
December 29, 2015
By Kim Palmer
CLEVELAND (Reuters) � Cleveland police will review from start-to-finish the fatal shooting of 12-year-old Tamir Rice to determine if the two officers involved or others should face disciplinary action in the November 2014 incident, officials said on Tuesday.
A grand jury on Monday declined to bring criminal charges against the officers in the death of Rice, who was brandishing a replica gun in a park before an officer shot him, drawing a protest on Tuesday afternoon in downtown Cleveland.
�People are very upset about it and I believe legitimately and rightfully so,� Cleveland Mayor Frank Jackson said of Rice�s shooting and other police-involved incidents around the United States.
�A 12-year-old lost his life. We take this seriously. We do soul searching,� Jackson told a news conference.
Earlier on Tuesday, a Chicago police officer pleaded not guilty to murder charges in the shooting of a black teenager that was captured on video and has led to calls for Chicago Mayor Rahm Emanuel to resign. [L1N14I0WO]
About 75 protesters escorted by police gathered at barriers in front of a Cleveland courthouse and then marched around downtown chanting �Whose streets? Our streets.� Officers blocked them from entering a divided highway and they sat down, blocking a major intersection.
The shooting of Rice was one of several incidents nationwide that have fueled scrutiny of the use of excessive force by police, particularly against minorities. The officers in the Cleveland case are white and Rice was black.
The Cleveland committee will review grand jury proceedings and all reports in Rice�s shooting, officials said.
Officer Timothy Loehmann shot Rice within seconds of reaching the park in response to reports of a person with a gun. Loehmann was in a patrol car driven by his partner Frank Garmback. Both have been on restricted duty since the shooting.
Rice was holding a replica of a .45-caliber handgun that fires plastic pellets and is sold with an orange tip on it. The gun Rice held did not have an orange tip.
Cuyahoga County Prosecuting Attorney Timothy McGinty said the failure of radio personnel to convey that a caller to the 911 emergency number had said the suspect was probably a juvenile and the gun may not be real was a substantial factor in the shooting.
Rice�s family, which has filed a civil lawsuit in his death, has asked the U.S. Justice Department to review McGinty�s handling of the grand jury, which they believe was manipulated to exonerate the officers, attorney Subodh Chandra said.
�She has been cheated twice, first by the loss of her boy and second by the prosecutor,� Chandra said of Rice�s mother, Samaria Rice.
(Reporting by Kim Palmer; Writing by David Bailey; Editing by Toni Reinhold and Dan Grebler)
