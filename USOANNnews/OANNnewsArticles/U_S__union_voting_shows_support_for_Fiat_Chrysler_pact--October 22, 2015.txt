Search for:
U.S. union voting shows support for Fiat Chrysler pact
Fiat Chrysler Autos assembly workers leave the Fiat Chrysler Automobiles (FCA) Warren Truck Assembly plant during a work shift change in Warren, Michigan October 7, 2015. REUTERS/Rebecca Cook
October 22, 2015
By Bernie Woodall
DETROIT (Reuters) � United Auto Workers union members showed overwhelming support in early voting returns on Wednesday night for a proposed four-year labor contract with Fiat Chrysler Automobiles NV <FCAU.N> <FCHA.MI>, according to local union hall websites.
The UAW is expected to issue on Thursday the final results of the ratification vote on the deal, which will set the tone for upcoming negotiations with General Motors Co <GM.N> and Ford Motor Co <F.N>.
Large local union halls in Kokomo, Indiana, and Belvidere, Illinois, supported the deal by more than 80 percent, according to their respective websites. Together, the plants in Kokomo and Belvidere have nearly a third of the 40,000 Fiat Chrysler UAW members.
These results, combined with results from local halls in Michigan, Ohio, Illinois and Indiana reported by the Detroit Free Press and Detroit News showed widespread and overwhelming support for the new Fiat Chrysler contract.
Last month, a previous proposed contract was rejected by 65 percent of Fiat Chrysler workers who voted.
If the contract is ratified, the UAW is expected to soon announce whether it will try to reach the next deal with either GM or Ford.
�I voted for it. The membership told (UAW leaders) to address the wage and equity issues and they did, which surprised me,� said Bill Parker, a worker at the suburban Detroit plant in Sterling Heights, Michigan.
The new contract offers a clearer path to top pay for so-called �second-tier� workers in a two-tier wage system established in 2007 that pays newer workers less than those hired before 2007.
The new contract calls for the reshaping of worker pay so that a new hire will make $17 per hour and advance to top pay, which under the new pact is about $30 per hour, over eight years. Since 2007, new hires have not had a clear path toward top pay.
Top pay for those hired after 2007 was raised to nearly $30 per hour in the new deal, up from $25 per hour in the rejected contract.
The new contract also eliminated a proposed healthcare pool of workers of all three Detroit automakers that many workers found confusing.
The union truncated voting to two days rather than have it extend over a week or more, and it repackaged the proposed contract with the help of a public relations firm.
(Reporting by Bernie Woodall; Editing by Lisa Shumaker)
