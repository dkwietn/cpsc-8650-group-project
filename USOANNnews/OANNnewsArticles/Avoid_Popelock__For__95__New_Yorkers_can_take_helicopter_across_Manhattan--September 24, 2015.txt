Search for:
Avoid Popelock! For $95, New Yorkers can take helicopter across Manhattan
The Manhattan skyline is seen during sunset from Arthur Ashe Stadium as Roger Federer of Switzerland faces Novak Djokovic of Serbia during their men's singles final match at the U.S. Open Championships tennis tournament in New York, September 13, 2015. REUTERS/Lucas Jackson
September 24, 2015
By Barbara Goldberg
NEW YORK (Reuters) � Daunted by the prospect of apocalyptic traffic snarls accompanying Pope Francis� visit, some busy New Yorkers were preparing to pay $95 for a five-minute helicopter ride from Manhattan�s East Side to its West Side on Friday.
Customers asked and they shall receive, said Cristina Gibson, a spokeswoman for flight company Blade, which works like vehicle ride service Uber. The company received a wave of requests from many of the 40,000 users who have installed the Blade app.
The pope, on a six-day trip to the United States, was traveling to New York from Washington later on Thursday and leading evening prayer services at St Patrick�s Cathedral.
Traffic was expected to jam tight on Friday, as Francis will be in the city along with President Barack Obama and some 200 foreign leaders attending the United Nations General Assembly. Customers expected �a security-based traffic lock down in the center of Manhattan,� Gibson said in an email.
The $95 is the cost of a seat on a Blade helicopter shuttling between the East Side Heliport, at East 34th Street and FDR Drive, and the West Side Heliport at 30th Street and the West Side Highway.
Helicopters will not fly directly across Manhattan but will be routed around the island�s southern tip in a ride lasting five to eight minutes.
Francis� schedule on Friday includes an address to the U.N. General Assembly, an inter religious service at the 9/11 Memorial and Museum, a visit to a Catholic school in East Harlem, a processional in Central Park and Mass at Madison Square Garden. He leaves the city on Saturday for Philadelphia.
Blade declined to predict how many seats it will sell for its trans-Manhattan service, set to run for just Friday during the morning and evening rush hours, but said it had already received bookings.
�Some had multiple appointments they didn�t want to be late for and others wanted convenient departure points to leave the city,� Gibson said.
(Reporting by Barbara Goldberg; Editing by Frances Kerry)
