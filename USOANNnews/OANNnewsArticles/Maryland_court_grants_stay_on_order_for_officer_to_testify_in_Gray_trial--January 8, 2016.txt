Search for:
Maryland court grants stay on order for officer to testify in Gray trial
Baltimore Police Officer William Porter (R) and his attorneys Joseph Murtha (L) and Gary Proctor arrive at the courthouse for pretrial hearings in the case of Caeser Goodson in Baltimore, Maryland, January 6, 2016. REUTERS/Bryan Woolston
January 8, 2016
BALTIMORE (Reuters) � A Baltimore police officer facing manslaughter charges in connection with the death of a black man while in police custody will not immediately have to testify against a fellow officer charged with murder following an appeals court ruling on Friday.
The Maryland Court of Special Appeals agreed to stay, or temporarily halt, a lower-court judge�s order for William Porter to take the stand in the trial of Caesar Goodson, who was charged with second-degree depraved heart murder in the April death of Freddie Gray.
Porter�s lawyers on Thursday appealed a Baltimore City Circuit Court�s order that their client take the witness stand in Goodson�s trial, which is scheduled to begin on Monday. Both Porter and Goodson are black.
Goodson was the driver of the police van where Gray, 25, sustained the broken neck that killed him after his arrest. He faces the most serious of the criminal charges filed against the six officers involved in Gray�s arrest.
Porter�s trial ended last month in a mistrial after the jury was unable to reach a verdict. He is scheduled to be retried in June.
(Reporting by Donna Owens; Writing by Scott Malone; Editing by Lisa Von Ahn)
