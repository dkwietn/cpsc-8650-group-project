Search for:
Judge authorizes search after scent of slain Virginia woman is found
Hannah Graham, 18, is shown in this handout photo provided by the City of Charlottesville Police Department in Charlottesville, Virginia, October 18, 2014. REUTERS/City of Charlottesville Police Department/Handout via Reuters
January 12, 2016
By Gary Robertson
CHARLOTTESVILLE, Va. (Reuters) � Police had probable cause to search the apartment of a former hospital worker accused of murdering university student Hannah Graham in 2014 after a tracking dog picked up the scent of the woman, a judge ruled on Monday.
The police tracking dog found the student�s scent at the apartment and on the passenger side of Jesse Matthew Jr.�s car, a former detective testified on Monday.
Judge Cheryl Higgins denied a motion from defense lawyers to suppress evidence found in the search.
The disappearance and murder of Graham, an 18-year-old University of Virginia sophomore, shocked the campus and drew national headlines in late 2014. Matthew was the last person seen with her and could face the death penalty if convicted.
Stuart Garner Jr., a former Louisa County detective and dog handler, testified in a pretrial hearing that his dog alerted him to Graham�s scent in several areas of Matthew�s apartment during a massive search for the student.
Garner sparred with defense attorney Douglas Ramseur about whether he had told Ramseur that he believed that Graham had never been in Matthew�s apartment.
�Didn�t you say Hannah Graham had never been present in Jesse Matthew�s apartment?� Ramseur asked, reading from an affidavit he had submitted to the court.
�I don�t know,� Garner said. �I found that her scent was present.�
The pretrial hearing, in Albemarle Circuit Court, offered a rare window into forensic evidence linking Matthew to
Graham. Matthew�s trial is scheduled to begin July.
Graham disappeared in September 2014 and her remains were found about five weeks later. Surveillance video taken in Charlottesville�s downtown mall showed Matthew and Graham walking together.
Matthew also has been indicted for the 2009 disappearance and murder of Virginia Tech student Morgan Harrington. He is serving three life sentences for a 2005 sexual assault in Fairfax, Virginia.
(Writing by Ian Simpson and Jon Herskovitz; Editing by Steve Orlofsky and Stephen Coates)
