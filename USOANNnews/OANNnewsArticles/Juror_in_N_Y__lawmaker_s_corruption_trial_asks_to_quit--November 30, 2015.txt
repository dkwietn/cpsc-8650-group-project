Search for:
Juror in N.Y. lawmaker�s corruption trial asks to quit
Former New York State Assembly Speaker Sheldon Silver arrives at the Manhattan U.S. District Courthouse in New York, November 23, 2015. REUTERS/Brendan McDermid
November 30, 2015
By Nate Raymond and Joseph Ax
NEW YORK (Reuters) � A juror considering whether to convict one of New York�s most powerful politicians of corruption on Monday asked to be removed from the jury, the second such request made to the judge in the high-profile trial over the past week.
New York State Assemblyman Sheldon Silver, who for decades wielded enormous influence as the chamber�s speaker, is charged with collecting some $4 million in illegal kickbacks in exchange for official acts.
Jurors began deliberations on his fate last Tuesday, and one juror sent a note to U.S. District Judge Valerie Caproni on Monday morning.
�I no longer wish to participate as a juror on this case,� the note read. �I believe there is a conflict of interest that I just learned about.�
Questioned later by Caproni about the possible conflict, the juror said he was a taxi driver who leases a medallion required by cab drivers to operate in the city from an individual who �associates� with Silver.
Caproni subsequently told the juror that �no one can ever retaliate against you for your jury service� and asked him to continue to deliberate.
�Well, I�ll do my best or whatever,� the juror replied. �I�d really prefer not to be here anymore, but if you order that, that�s what I�ve got to do.�
The request was the second from a juror since deliberations began following a three-week trial.
Just hours after the jury began considering charges of fraud, extortion and money laundering, a juror sent Caproni a note complaining that she felt �pressured� and �stressed out� by the other jurors and asked to be released.
The judge sent the jurors back after urging them to respect each other�s opinions.
Silver, a 71-year-old Democrat, relinquished his leadership post after his January arrest but continues to hold his seat in the legislature.
The former majority leader of the state Senate, Dean Skelos, is also on trial for corruption in a federal court in Manhattan along with his son, Adam.
Like Silver, Skelos, a Republican, stepped down from his post following his May arrest but remains in the Senate.
Silver and Skelos comprised two-thirds of the �three men in a room,� along with the governor, who control key legislation in the state capital of Albany.
The two trials are the highest-profile cases in a string of scandals that have roiled Albany in recent years and forced dozens of lawmakers from office.
(Editing by Scott Malone and Jonathan Oatis)
