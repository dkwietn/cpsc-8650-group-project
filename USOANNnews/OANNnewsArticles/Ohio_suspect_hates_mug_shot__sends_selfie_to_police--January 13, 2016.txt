Search for:
Ohio suspect hates mug shot, sends selfie to police
Donald "Chip" Pugh is pictured in this undated booking photo provided by the Lima Police Department, in Lima, Ohio. Pugh, who is wanted on a drunk driving charge, was so dissatisfied with the mug shots that police put on their Facebook page, he posted a "better photo" of himself in a car wearing shades. REUTERS/Lima Police Department/Handout via Reuters
January 13, 2016
By Kim Palmer
CLEVELAND (Reuters) � An Ohio man wanted for drunk driving, unsatisfied with his police mug shot, sent them a photo he felt was more flattering.
Donald �Chip� Pugh, 45, posted a photo of himself wearing sunglasses in a car after he saw the two mug shots the Lima, Ohio, police department had posted on its Facebook page.
�Here is a better photo that one is terrible,� Pugh wrote when he sent in the selfie.
The police last week posted the mug shots after an arrest warrant was issued following Pugh�s failure to appear in court on a misdemeanor drunk driving charge. Pugh is a person of interest in several other cases including an arson and vandalism.
Pugh�s post quickly captured the attention of police.
�This photo was sent to us by Mr. Pugh himself. We thank him for being helpful, but now we would appreciate it if he would come speak to us at the LPD about his charges,� the police said on Facebook.
Police said Pugh, who could not be reached for comment, is still at large.
(Additional reporting by Fiona Ortiz in Chicago; Editing by Phil Berlowitz and Matthew Lewis)
