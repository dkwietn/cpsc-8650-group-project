Search for:
South Carolina church massacre detailed in newly released documents
A cross bearing notes of condolence is pictured outside of Emanuel African Methodist Episcopal Church in Charleston, June 21, 2015. REUTERS/Carlo Allegri
October 29, 2015
By Harriet McLeod
CHARLESTON, S.C. (Reuters) � Police logs released on Thursday revealed a chilling account of the June 17 massacre at a historic black church in Charleston, South Carolina, that killed nine people.
The accounts cited calls made from inside Emanuel African Methodist Episcopal Church, which is the oldest African-American congregation in the southern United States.
�People shot down � Shot pastor � Man is still here,� an entry in the police communications log said.
An entry one minute later said, �Young white male � Male is reloading.
�Shots have been fired. The number of shots fired is: SO MANY.�
According to the report, authorities quickly had a description of the suspect � a 21-year-old male in boots, jeans and a T-shirt � and they knew he had fled out the church�s back door.
Dylann Roof has been charged with murder and state prosecutors are seeking the death penalty. He also faces hate crime and weapons charges in what authorities say was a racially motivated rampage.
The heavily redacted police reports and photographs from the Charleston Police Department make up some of the evidence against Roof. Some media organizations who sued to get the documents made public said police went too far in blacking out names, comments and key details.
A state judge this month lifted a gag order placed on the evidence but decided that graphic crime scene photographs, audio of 911 emergency phone calls and other sensitive materials would remain sealed to protect the victims.
Charleston police said in a letter to the media that they did not see a public interest in releasing information that could harm their case, nor a benefit in �the release of gory and disturbing videos and photographs.�
After a massive hunt for Roof, a driver in North Carolina called police the morning after the massacre to say she had spotted the suspect in his car, according to the released documents.
Roof was quiet and compliant when officers pulled over his car at about 10:30 a.m., a report said, and acknowledged he was coming from Charleston.
Asked if he knew why he was detained, he nodded his head. A sergeant asked Roof if police should know about anything in his car.
�Yeah, there�s a gun in the backseat,� Roof said, according to the report.
Officers later found with Roof�s driver�s license a movie ticket receipt, from the day before the shootings, for the action thriller �Redemption.�
(Reporting by Harriet McLeod; Writing by Letitia Stein; Editing by Colleen Jenkins and Eric Beech)
