Search for:
New York, other U.S. cities beef up security after Paris attacks
Police stand outside the Stade de France where explosions were reported to have detonated outside the stadium during the France vs German friendly soccer match near Paris, November 13, 2015. REUTERS/Gonazlo Fuentes
November 14, 2015
By Laila Kearney
NEW YORK (Reuters) � New York, Boston and other cities in the United States bolstered security on Friday night after deadly gun and bomb attacks on civilians in Paris, but law enforcement officials said the beefed-up police presence was precautionary rather than a response to any specific threats.
The New York Police Department said officers from its Counterterrorism Response Command and other special units were deployed in areas frequented by tourists, and at the French Consulate in Manhattan.
�Teams have been dispatched to crowded areas around the city out of an abundance of caution to provide police presence and public reassurance as we follow the developing situation overseas,� the NYPD said in a statement.
New York, the site of the Sept. 11, 2001, hijacked plane attacks that killed nearly 3,000 people and destroyed the World Trade Center�s twin towers, is considered a top target for potential attacks by Islamist militants. The top of the Empire State Building and the spire at One World Trade Center were lit up Friday night with blue, white and red, the colors of the French flag.
The nearly simultaneous gun and bomb attacks in Paris killed at least 120 people in various places across the French capital and wounded many others
The NYPD did not say how many extra officers were sent to guard the areas of concern nor did it specify the areas where the extra officers were sent.
�Every time we see an attack like this, it is a reminder to be prepared, to be vigilant,� New York Mayor Bill de Blasio told ABC 7 television.
New York Governor Andrew Cuomo said he directed state law enforcement officials to monitor the Paris situation for any implications for New York state and to remain in constant communication with their local and federal partners.
The Port Authority of New York and New Jersey said police were on heightened alert at all of the agency�s bridges, tunnels and rail facilities, as well as at the World Trade Center in lower Manhattan. It said it was increasing patrols and checking of buses and trains and passengers� bags.
U.S. Department of Homeland Security Secretary Jeh Johnson said in a statement that �we know of no specific or credible threats of an attack on the U.S. homeland of the type that occurred in Paris tonight.�
The National Basketball Association, which had 11 games on the schedule Friday night, said it was increasing security at each of the venues. The most popular sport, American football, would not have any games until Sunday as previously scheduled.
�Security at our games is always at a heightened state of alert,� National Football League spokesman Brian McCarthy said.
Outside of New York, law enforcement and transportation agencies said they were also on high alert.
The U.S. Capitol Police in Washington boosted patrols around the Capitol complex, a spokeswoman said. �There is currently no known threat to the Capitol Complex,� she said in an email.
The Metropolitan Police Department in Washington had deployed additional law enforcement resources to French-owned sites and other high-profile locations as a precaution but there was no imminent threat, said Officer Sean Hickman.
At the French Embassy in Washington, D.C., a fashion show went ahead as scheduled, with hosts pausing the event for a moment of silence to honor the victims in Paris.
In Boston, the police department said it deployed additional resources and was working closely with federal authorities but saw no credible threat in the city, where Islamist militant sympathizers set off home-made bombs at the Boston Marathon finish line in April 2013. Massachusetts State Police said they took �several actions�, including bolstering security around the State House in Boston.
The St. Louis Police Department said it added an extra layer of security for the World Cup soccer qualifying match between the United States and St. Vincent on Friday night.
Chicago police were following developments in France to determine whether to bolster city security but was not aware of any immediate threats.
�Tonight the City of Chicago stands shoulder to shoulder with the City of Paris in the wake of today�s despicable and horrifying attacks,� Mayor Rahm Emanuel said.
San Francisco police officers have been told to maintain high visibility and increase patrols in areas of high public traffic, such as bus and train stations, said Sgt. Michael Andraychak. Police have been in contact with the French Consulate.
In Pittsburgh, which was hosting a National Hockey League game on Friday, a police spokeswoman said public safety personnel were working with intelligence authorities to identify any indications of local threats.
Amtrak, the U.S.�passenger train service, said there were no specific or credible threats against the railway.
(Additional reporting by Ian Simpson and Tom Ramstack in Washington, Mary Wisniewski in Chicago and Victoria Cavaliere in Los Angeles; Writing by Frank McGurty; Editing by Grant McCool, Diane Craft and Ken Wills)
