Search for:
Muslim Americans fear demonization of Islam after mass shooting
File photo of Muslim students holding a prayer before a rally against Islamophobia at San Diego State University in San Diego, California, November 23, 2015. REUTERS/Sandy Huffaker
December 4, 2015
By Ben Klayman
DEARBORN, Mich. (Reuters) � Muslim Americans fear their religion will be demonized and Islamophobia will spread after a young Muslim couple was accused of carrying out one of the bloodiest mass killings in the United States.
Across the country, Muslim Americans responded with shock and outrage after a shooting in which authorities said Syed Rizwan Farook, 28, and Tashfeen Malik, 27, stormed a holiday party attended by San Bernardino County employees in California on Wednesday, killing 14 people and wounding 21. [nL1N13S0HF]
�I was at the gym yesterday while the shooting was taking place and all the TVs were showing that footage and all I could keep thinking to myself is �God, I hope they don�t have any Eastern descent, not just Middle Eastern, anything we�d associate with a Muslim�,� said Adam Hashem, 32, in Dearborn, a Detroit suburb with one of the country�s largest Muslim populations.
�We�re all worried. We�re all concerned,� he said.
It was the deadliest U.S. mass shooting since the Sandy Hook Elementary School massacre three years ago. While the motivation remained unclear as authorities investigated the attack, details of Farook and Malik began to emerge. Farook was described as a second-generation American born in Illinois and raised by Pakistani parents. Malik was born in Pakistan and lived in Saudi Arabia until she was introduced to Farook.[nL1N13S0B3]
San Bernardino police said they found pipe bombs and several thousands rounds of ammunition at the residence of the couple, who died in a shoot-out with police.
In Milwaukee, Wisconsin, the Attari Supermarket bustled on Thursday with customers shopping for Middle Eastern products.
�In every culture and in every religion there are bad apples that will spoil the rest of the apples. That has happened toward us,� said Dawod Dawod, a 25-year-old Muslim American, who manages the store that his family has owned for a decade.
Between taking orders over the phone, Dawod said he was concerned that politicians will use the mass shooting as a way to further demonize Muslims. He noted Republican presidential candidate Donald Trump�s endorsement of the idea of creating a Muslim database. �It�s scary.� he said. �Ninety-nine percent of Muslims are hardworking, good people.�
Muslim community groups condemned the massacre and urged the public not to blame Islam or Muslims.
�The Muslim community stands shoulder to shoulder with our fellow Americans in repudiating any twisted mindset that would claim to justify such sickening acts of violence,� said Hussam Ayloush, an executive director at the Council on American-Islamic Relations.
Within hours of the shooting, his group had organized a news conference with Los Angeles Muslim leaders and the brother of suspected shooter Malik to condemn the assault. The speed at which they went on live television underlined the depth of concerns in a community already buffeted by a rise in anti-Muslim rhetoric this year and increased public scrutiny after the Nov. 13 attacks in Paris that killed 130 people and were claimed by Islamic State militants.
Some Muslims say they have felt singled out during a U.S. presidential race that has tapped a vein of anger and bigotry � from comments by Trump to those by fellow Republican candidate Ben Carson, who said in September Muslims were unfit for the presidency of the United States. There are some 2.8 million Muslims in the country.
�HORRIFIED�
Some Muslims questioned whether this week�s shooting will embolden supporters of Trump, who is current front-runner to be his party�s nominee in the November 2016 election and who has backed the idea of requiring all Muslims living in the United States to register in a special database as a counter-terrorism measure.
Critics have also accused Trump of stirring resentment toward Muslims by asserting that he saw thousands of Muslims in New Jersey celebrating the destruction of the World Trade Center towers on Sept. 11, 2001. That claim has been disputed by public officials.
Faizul Khan, 74, an Imam at the Islamic Society of the Washington Area, said he was �horrified� by the San Bernadino shooting. �Unfortunately people don�t understand that we as Muslims, we basically want to promote what is good and just for the entire humanity.�
He said he feared the shooting would strengthen calls to increase surveillance on mosques.
Achraf Issam, 22, national spokesman for the Ahmadiyya Muslim Youth Association in Silver Spring, Maryland, said it makes no more sense to say that Islam led to the San Bernardino shootings than to say Christianity led to an attack on the Planned Parenthood clinic in Colorado last week by a suspect police have named as Robert Lewis Dear.
�No one should say that because this couple is Muslim that it led them to commit those acts,� he said.
That sentiment was echoed by Sara Nabhan, 20, a junior majoring in biology at the University of Houston who was born in Jordan and came to Texas when she was 2 years old.
�Two people�s actions do not constitute a whole population�s actions,� she said.
Jersey City real-estate agent Magdy Ali, 52 and of Egyptian descent, said he uses the name Alex when working to avoid conflict with people who distrust Islam. He said he expects Trump to use Wednesday�s massacre to push for anti-Muslim measures such as monitoring of U.S. mosques.
�We are in a jam right now,� he said.
(Additional reporting by Ruthy Munoz in Houston, Brendan O�Brien in Milwaukee, Ian Simpson in Washington, Mary Wisniewski in Chicago and Barbara Liston in Florida; Writing by Jason Szep; Editing by Frances Kerry)
