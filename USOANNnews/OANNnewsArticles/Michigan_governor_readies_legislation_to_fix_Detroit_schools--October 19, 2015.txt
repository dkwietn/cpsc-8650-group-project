Search for:
Michigan governor readies legislation to fix Detroit schools
Governor Rick Snyder of Michigan applauds at a meeting with Chinese President Xi Jinping and four other United States governors to discuss clean technology and economic development in Seattle, Washington September 22, 2015. REUTERS/Matt Mills McKnight
October 19, 2015
(Reuters) � Detroit�s public schools should be split between a new community school district and the current district to raise academic performance and avoid financial collapse, Michigan Governor Rick Snyder said on Monday.
The new district would be responsible for operations, while current district would be charged with eliminating a budget deficit.
The Republican governor warned that without his plan, which he expects to be introduced in the state legislature this month, the school system risked a financial crisis that would not necessarily lead to bankruptcy but could result in a debt default that would have repercussions for the state and its other school districts.
The city of Detroit shed about $7 billion of its $18 billion of debt and obligations when it exited the biggest-ever U.S. municipal bankruptcy last December, but the city�s school district has been unable to shake off a state-declared financial emergency dating back to 2008.
�The city is coming back and we want to see that sustained for generations to come. To do that you need a good education system,� Snyder told reporters at a briefing in the state capital of Lansing.
The plan calls for transferring the school system�s 47,000 students, its employees and their benefits, contracts and assets to a new Detroit Community School District. The current Detroit Public Schools would remain in existence until an operating deficit, expected to be $515 million next June, is eliminated over several years using local property tax revenue.
Michigan would replace the school system�s tax revenue dedicated to the deficit elimination with state funding going to the new school operating entity. That entity would need an additional $200 million to cover capital and other costs, bringing the total state contribution to $715 million, according to a Snyder
A property tax levy earmarked for the district�s outstanding bonds would continue to pay off that debt, said spokeswoman Sara Wurfel.
The Detroit Financial Review Commission, which was created as part of the city�s bankruptcy exit plan, would oversee the finances of both school entities until the deficit is erased.
Snyder�s proposal also creates an initially appointed school board that will transition to an elected board, as well as a Detroit Education Commission to oversee the hiring of a new chief education officer. That official would be tasked with creating a plan to boost the district�s sagging enrollment and its academic performance.
(Reporting by Karen Pierog; Editing by Steve Orlofsky)
