Search for:
CNBC sets qualification rules for prime-time U.S. Republican debate
Members of the news media watch on television monitors in the media center as Republican U.S. presidential candidates businessman Donald Trump and former Florida Governor Jeb Bush debate during the second official Republican presidential candidates debate of the 2016 U.S. presidential campaign at the Ronald Reagan Presidential Library in Simi Valley, California, September 16, 2015. REUTERS/Mario Anzuoni
October 1, 2015
WASHINGTON (Reuters) � U.S. Republican presidential candidates polling an average of at least 2.5 percent in designated national opinion polls will qualify for a prime-time debate in Colorado on Oct. 28, the debate�s host, CNBC, said on Wednesday.
Candidates below that level, but scoring at least 1 percent in any of the six designated polls, will earn a place in the second-tier debate immediately preceding the main event, the cable news network said.
The two-hour prime-time debate, the third for the crowded Republican field, will start at 8 p.m. EDT at the University of Colorado Boulder and focus on topics including the economy, taxes and jobs, CNBC said.
While 3 percent was set as the official threshold for the main debate, the network said the average of the polls conducted between Sept. 17 and Oct. 21 would be rounded up to that figure for any candidate with at least 2.5 percent.
Nine candidates would currently qualify for the main debate, according to a Real Clear Politics average of polls, which does not include several of the six polls that CNBC will use as its criteria. That group would consist of front-runner Donald Trump, retired physician Ben Carson, former businesswoman Carly Fiorina, Florida Senator Marco Rubio, former Florida Governor Jeb Bush, Texas Senator Ted Cruz, Ohio Governor John Kasich, former Arkansas Governor Mike Huckabee and New Jersey Governor Chris Christie.
Kentucky Senator Rand Paul would also qualify, according to his standing in the CBS, NBC, ABC, CNN, Fox and Bloomberg polls designated by CNBC.
The main CNN Republican debate on Sept. 16 featured 11 candidates, including Fiorina, who did not qualify for the first prime-time debate on Fox News on Aug. 6 that involved 10 candidates.
For more on the 2016 presidential race, see the Reuters blog, �Tales from the Trail� (http://blogs.reuters.com/talesfromthetrail/)
(Reporting by Peter Cooney; Editing by Sandra Maler and Eric Beech)
