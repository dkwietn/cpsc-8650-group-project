Search for:
U.S. sues to block United Airlines from Newark expansion
People walk at the United Airlines terminal at the Newark Liberty International Airport in New Jersey, July 8, 2015. REUTERS/Eduardo Munoz
November 10, 2015
By Diane Bartz and Jeffrey Dastin
WASHINGTON/CHICAGO (Reuters) � The U.S. Justice Department on Tuesday sued to stop United Continental Holdings Inc <UAL.N> from taking over 24 take-off and landing slots at Newark Liberty International Airport, an action that could thwart United�s expansion at its New Jersey hub.
In what was essentially a slot swap, United took over Newark slots controlled by Delta Air Lines Inc <DAL.N> in October, while Delta took United slots at New York�s John F. Kennedy International Airport.
But the Justice Department, which has recently taken other actions to try to rein in the largest U.S. air carriers, said that United�s expansion in Newark, where it is already dominant, would mean higher prices and fewer choices for travelers in New York, New Jersey and parts of Pennsylvania who use the airport.
It asked a court to declare the Newark slots deal illegal.
�There are 35 million air passengers who fly into and out of Newark every year. And we know that air fares at Newark are among the highest in the country while United�s service at Newark ranks among the worst,� said Assistant Attorney General Bill Baer on a call with reporters.
United said it will �vigorously defend our ability to operate effectively, efficiently and competitively at Newark.�
The Justice Department has been active in aviation, recently seeking to curtail the consolidation of power among the top carriers. Following a series of mega mergers, American Airlines Group Inc <AAL.O>, Delta, United and Southwest Airlines Co <LUV.N> control some 80 percent of domestic air travel.
The department sued to stop the most recent deal, the merger of US Airways and American, but settled and allowed the companies to close on the transaction last year.
The department is also investigating whether U.S. airlines worked together illegally to keep airfares high by signaling plans to limit flights.
United shares dipped on news of the lawsuit but recovered to close slightly down.
�DOJ (the Justice Department) believes that it has a plausible case here. A whole slew of people signed that complaint. This is not animosity,� said Bruce McDonald, who worked at the department under President George W. Bush and is now with law firm Jones Day.
UNITED�S BIG PLANS FOR NEWARK
The suit could set back plans by United, the second-largest U.S. airline by capacity, to raise the profile of the New Jersey airport.
United has marketed the hub as a closer alternative to JFK for professionals in New York City�s financial district. It has invested $2 billion in terminal renovation to add new restaurants and has pushed for the extension of a subway line to Newark from downtown Manhattan.
The department�s lawsuit against United, which was filed in U.S. district court in Newark, said that United already has 902 take-off and landing slots at Newark while American, in second place, has just 70 slots. The Justice Department said that United failed to use as many as 82 slots daily.
United defended its leaving slots unused by saying that it varied flight schedules seasonally and by day of week to match passenger demand.
Diana Moss, president of the American Antitrust Institute, disagreed. �They are holding these slots and flying much smaller aircraft on the slots to keep them,� said Moss, who said that airlines would strategically use slots to foreclose competitors. �They call it babysitting.�
Delta, which began flights with the JFK slots on Nov. 1 under the swap deal with United, said the lawsuit against United did not affect its operations.
�Delta�s agreement to lease slots at Newark to United � does not affect Delta�s separate agreement to lease slots from United at New York-JFK Airport,� Delta spokesman Trebor Banstetter said in a statement.
(Reporting by Lisa Lambert in Washington; Editing by Marguerita Choy and Steve Orlofsky)
