Search for:
Jury deliberates in case of Oklahoma City ex-policeman accused of rape
Daniel Holtzclaw, 28, of Oklahoma City is pictured in this undated handout photo obtained by Reuters December 7, 2015. REUTERS/Oklahoma County Sheriff's Office/Handout via Reuters
December 9, 2015
By Heide Brandes
OKLAHOMA CITY (Reuters) � A jury deliberated on Tuesday without reaching a verdict in the case of a former Oklahoma City police officer accused of sexually assaulting and raping 13 women, who prosecutors said were targeted because they had troubles with the law.
Daniel Holtzclaw, 28, of Oklahoma City is charged with 36 counts of sexual assault, including six first-degree rape counts for attacks on 13 women. He could face life in prison if convicted of the most serious charges and was seen weeping when the judge read out the charges against him.
The jury began deliberations on Monday night and spent hours on Tuesday locked in discussion without reaching a decision. They would resume deliberations on Wednesday.
In closing arguments on Monday, prosecutors said Holtzclaw targeted his victims by singling out women he came across while on patrol. He ran background checks and went after those who had outstanding warrants or previous arrests or who carried drugs or drug paraphernalia, prosecutors said.
They said he did this because he did not think authorities would take the victims� word over his, if he had to defend himself against sexual assault allegations.
�He exercised authority on those society doesn�t care about. Convince these ladies that someone does care about them,� said�Assistant District Attorney Gayland Gieger.
The defense said in its closing arguments the victims provided testimony that was unreliable and dishonest.
Defense attorney Scott Adams painted a picture of an honorable police officer whose daily activities were suddenly made to appear evil and suspicious.
Thirteen of Holtzclaw�s alleged victims took the stand in the trial, which began more than a month ago, telling jurors of sexual assaults that ranged from touching over their clothing to forced oral sex and rape.
Holtzclaw was fired over the accusations in January 2015 after approximately three years on the job.
(Writing by Jon Herskovitz; Editing by Frances Kerry, Victoria Cavaliere and Michael Perry)
