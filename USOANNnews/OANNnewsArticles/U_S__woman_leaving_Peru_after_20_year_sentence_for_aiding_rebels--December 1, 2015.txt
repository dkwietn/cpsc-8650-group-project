Search for:
U.S. woman leaving Peru after 20-year sentence for aiding rebels
U.S. citizen Lori Berenson arrives at Newark Liberty international airport in New Jersey December 20, 2011. REUTERS/Eduardo Munoz
December 1, 2015
LIMA (Reuters) � Lori Berenson, a New Yorker once jailed in Peru for helping Marxist insurgents, is returning to the United States with her six-year-old son after finishing a 20-year sentence, her lawyer said Tuesday.
Berenson, 46, was making preparations to leave Peru �in coming hours,� said Anibal Apari, her attorney and the father of her son. He declined to be more specific.
Apari and Berenson met in prison in 1997. He was also an inmate at the time.
Berenson was on parole for the past 5 years after spending 15 years in prison.
In 1996, Berenson was found guilty of supporting the Tupac Amaru Revolutionary Movement, or MRTA, a guerilla group active in the 1980s and 90s. She was never convicted of participating in violent acts.
�She�s finished her sentence and is now a free person,� said Apari, a former member of MRTA.
Berenson is one of a growing number of people in Peru finishing sentences for crimes linked to the country�s civil war in the 80s and 90s, which left an estimated 69,000 people dead.
A military tribunal initially sentenced Berenson to life in prison using counterterrorism laws. She was retried in a civilian court and her sentence was reduced after pressure from her parents, human rights groups and the U.S. government.
(Reporting By Marco Aquino; Writing by Mitra Taj; Editing by Toni Reinhold)
