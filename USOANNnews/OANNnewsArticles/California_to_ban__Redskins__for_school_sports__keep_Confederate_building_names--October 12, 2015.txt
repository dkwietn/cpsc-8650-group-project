Search for:
California to ban �Redskins� for school sports, keep Confederate building names
California Governor Jerry Brown speaks at a meeting with Chinese President Xi Jinping and five United States governors to discuss clean technology and economic development in Seattle, Washington September 22, 2015. REUTERS/Matt Mills McKnight/Pool
October 12, 2015
By Sharon Bernstein
SACRAMENTO, Calif. (Reuters) � California will ban public schools from naming their sports teams �Redskins,� a name seen as a slur against Native Americans, but will not stop municipalities from naming parks and buildings for Confederate heroes, Governor Jerry Brown said on Sunday.
Brown�s decision to sign a bill ending the use of �Redskins� yet veto legislation banning Confederate names comes amid controversy around the country over the racial implications of team names, display of the Confederate flag and the naming of public places.
Advocates for Native Americans welcomed the decision to ban the term �Redskins.�
�The most populous state in the country has now taken a stand against the use of this insidious slur in its schools,� activists from the group Change the Mascot said in a statement on Sunday. California is �standing on the right side of history by bringing an end to the use of the demeaning and damaging R-word slur in the state�s schools.�
California is the first in the nation to enact a statewide ban on the term, although individual school districts, including Houston, Texas, and Madison, Wisconsin have already done so, said Joel Barkin, spokesman for the Oneida Indian Nation in New York state, which has backed the Change the Mascot campaign.
Controversy over whether the Washington, D.C. National Football League football team should continue to be named the Redskins has spilled over into the race for the Republican presidential nomination. Former Florida Governor Jeb Bush and billionaire Donald Trump have said they do not see a need to change the name.
In another racially sensitive area, however, Brown vetoed a bill to ban naming public property after Confederate heroes. Such names are considered racist by many people because they honor those who fought for the slave-owning South in the U.S. Civil War.
But Brown, who last year signed a bill outlawing the sale of faux Confederate currency at the state Capitol gift shop, said local decision-makers should choose names for schools and parks.
�Recently we saw a national movement to remove the Confederate flag from state capitols in the South � a long overdue action,� said Brown, a former mayor. �This bill, however, strikes me as different and an issue quintessentially for local decision-makers.�
(Editing by Jonathan Oatis)
