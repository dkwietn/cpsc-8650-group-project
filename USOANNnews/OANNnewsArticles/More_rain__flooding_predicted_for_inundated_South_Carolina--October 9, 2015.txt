Search for:
More rain, flooding predicted for inundated South Carolina
Cars wait in line to pass a flooded section of Dunbar Road in Georgetown, South Carolina October 8, 2015. REUTERS/Randall Hill
October 9, 2015
By Randall Hill
GEORGETOWN, S.C. (Reuters) � As state and local officials warned on Friday of the possibility of more flooding in eastern South Carolina , Georgetown County resident Wilma Green said she would heed their advice to evacuate.
�I�m waiting on rescue people to come pick me up,� said Green, 70, who planned to leave her home in the vulnerable Dunbar community and stay with her daughter until the floodwaters subsided.
Concerns about additional inundation in four coastal counties and more rain had officials on guard Friday, nine days after a state of emergency was declared because of historic rains that washed out roads, swamped hundreds of homes and killed 17 people in the state.
Emergency management officials in several areas were encouraging residents to leave their homes as a precaution as floodwaters flow south into already-swollen rivers and tributaries toward the Atlantic Ocean.
In Jamestown, about 140 households along the Santee River were being urged to evacuate, said Berkeley�County spokesman Michael Mule.
The National Weather Service in Charleston predicted a half-inch of rain would fall across much of the state starting Saturday morning, with some places getting up to an inch.
That comes after record rainfall of more than 2 feet (60 cm) in parts of South Carolina.
�It�s not a lot of additional rain, but after all that we�ve had, this is definitely what we don�t need,� said weather service meteorologist Steve Rowley.
Governor Nikki Haley said emergency responders would continue monitoring water levels and go door-to-door to let residents know if they were in danger in the coming days.
A Reuters photographer accompanied South Carolina National Guard members on Friday as they evacuated 23 people whose homes were surrounded by flooded roads in the Dunbar community.
Haley met on Friday with U.S. Homeland Security Secretary Jeh Johnson, who traveled to Columbia and Charleston to take stock of the flooding response and recovery efforts.
The governor would not estimate the financial toll from the record rainfall and flooding.
�This is damage at levels I�ve never seen before,� she told a news conference.
(Additional reporting by Rich McKay in Columbia, South Carolina; Writing by Colleen Jenkins; Editing by Steve Orlofsky)
