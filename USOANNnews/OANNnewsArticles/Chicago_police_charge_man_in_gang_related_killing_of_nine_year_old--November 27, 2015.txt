Search for:
Chicago police charge man in gang-related killing of nine-year-old
Corey Morgan, 27, is shown in this booking photo taken and provided by the Cook County Sheriff's Office in Chicago, Illinois, November 27, 2015. REUTERS/Cook County Sheriff's Office/Handout via Reuters
November 27, 2015
By Mary Wisniewski
CHICAGO (Reuters) � A man has been arrested and charged in the gang-related murder of a 9-year-old boy who was lured from a park into an alley earlier this month and shot several times, Chicago Police said on Friday.
The killing of Tyshawn Lee on Nov. 2 cast a spotlight on a jump in violent crime in the city of 2.7 million people, the third-largest in the United States.
Corey Morgan, 27, of Lansing, Illinois, has been charged with first-degree murder, police said. They said at least two others had also been involved. Morgan was denied bond on Friday, prosecutors said.
Police said they also have a warrant for a second suspect, Kevin Edwards, 22, of Chicago, and have in custody a third suspect being held on another charge.
Police said the crime was linked to a rivalry between Lee�s father, a suspected gang member, and another group, and was connected to at least two other murders. The boy�s father, Pierre Stokes, has denied being involved in a gang.
Chicago Police Superintendent Garry McCarthy called Lee�s shooting �an act of barbarism� and said Morgan and the other suspects were all members of the same gang.
�That gang just signed its own death warrant,� McCarthy said. �We�re going to destroy this gang.�
Police said Lee had been playing basketball in a park when gang members approached him, engaged in conversation, and walked Lee into an alley, where he was assassinated.
Morgan had been arrested this month on a gun charge. Edwards is believed to be in the area, McCarthy said.
McCarthy said despite a high level of fear, police received �an awful lot of intelligence from the community,� unlike in other cases in which people have been unwilling to come forward.
As is the case in a number of other U.S. cities, violence has risen in Chicago. Authorities reported 411 murders from Jan. 1 to Nov. 15, up 14 percent from the same period of 2014. Some experts attributed the violence to the ready availability of guns or a growing heroin trade.
The arrest was announced on the same day that activists protested over the shooting death of a black teenager by a white Chicago patrolman in October 2014 that was filmed on a police dashboard camera. The video was not released until this week after the police officer was charged with murder.
(Additional reporting by Laila Kearney and Jon Herskovitz; editing by Steve Orlofsky and Grant McCool)
