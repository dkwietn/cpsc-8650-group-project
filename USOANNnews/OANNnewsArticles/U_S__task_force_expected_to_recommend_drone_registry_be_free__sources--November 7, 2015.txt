Search for:
U.S. task force expected to recommend drone registry be free: sources
The DJI Phantom 3, a consumer drone, takes flight after it was unveiled at a launch event in Manhattan, New York April 8, 2015. REUTERS/Adrees Latif
November 7, 2015
WASHINGTON (Reuters) � A federal task force is expected to recommend a Web-based registry for drone owners that would impose no registration fees, two people familiar with the matter said on Friday.
The 25-member task force has reached no final decisions after three days of meetings this week, the sources said. Discussions are expected to continue via teleconference over the next two weeks before the committee delivers formal recommendations to the Federal Aviation Administration by Nov. 20.
Owners of small unmanned aerial vehicles would likely have to sign up on a website or use a smartphone app and put a visible registration number on the drone.
The process would provide a digital alternative to the more onerous paper-based registration process for manned aircraft. Responsibility for registering is expected to fall to the owner rather than the manufacturer or retail vendor, but the task force could recommend no penalties for first-time noncompliance, the sources said.
The sources said this week�s meetings focused largely on top-line issues, leaving details on specific requirements and privacy questions for later discussions.
U.S. aviation regulators, who expect to begin implementing registration in December, say a registry would help authorities combat a surge in rogue drone flights near airports and other public sites. The flights have raised concerns about safety and security risks including possible collisions with commercial aircraft.
David Vos, the leader for Google�s Project Wing and co-chairman of the task force, told a conference this week that registration should be seen as the first step toward incorporating drones into U.S. airspace. He predicted that other steps, including air traffic control systems for low-altitude drone flights, could follow within the next 12 months.
The FAA is crafting final regulations that would allow companies to use drones as part of their business operations. Those rules are expected early next year.
(Reporting by David Morgan; Editing by Lisa Shumaker)
