Search for:
Chicago man arrested for gun threat linked to black teen�s killing
A member of the University of Chicago Police patrols the campus in Chicago, Illinois, United States, November 30, 2015. REUTERS/Jim Young
November 30, 2015
By Suzannah Gonzales and Mary Wisniewski
CHICAGO (Reuters) � A 21-year-old Chicago man was arrested on Monday for threatening to kill 16 white male students or staff on the University of Chicago in retaliation for the shooting last year of black teenager Laquan McDonald by a white police officer, according to law enforcement officials.
Jabari R. Dean was charged with transmitting a threat in interstate commerce and, if convicted, could face up to five years in prison, the U.S. Attorney in Chicago said in a statement.
The University of Illinois at Chicago said one of its students, living off campus, was arrested for making the threat, which caused the University of Chicago to cancel classes on Monday.
UIC confirmed that Dean, who wore a red school sweatshirt at his initial court appearance, according to a Chicago Sun-Times reporter, is an undergraduate studying electrical engineering and has been enrolled since fall 2015. Dean was ordered held in custody until bond conditions could be determined, likely tomorrow.
When investigators searched the off-campus residence of the UIC student, they found no gun, according to the Sun-Times, which cited a source familiar with the investigation.
According to a criminal complaint filed by the FBI, someone called the agency to report a comment he saw on a website that the Chicago Tribune reported as www.worldstarhiphop.
The writer threatened to shoot and kill students, staff and police on the University of Chicago�s campus at 10 a.m. on Monday and then kill himself, citing the fatal shooting last year of 17-year-old Laquan McDonald by a white Chicago police officer, the newspaper said.
The officer, who shot McDonald 16 times, was charged last week with first-degree murder. A judge on Monday set a $1.5 million bond for the officer, Jason Van Dyke.
�This is my only warning. At 10 a.m. on Monday morning, I am going to the campus quad of the University of Chicago. I will be armed with a M-4 carbine and 2 Desert Eagles, all fully loaded. I will execute approximately 16 white male students and or staff, which is the same number of time McDonald was killed,� said the commenter, who posted with the initials �JRD� and a Chicago Bulls logo, according to the criminal complaint.
�I then will die killing any number of white policeman that I can in the process. This is not a joke. I am to do my part to rid the world of the white devils. I expect you do the same.�
The FBI official who signed the criminal complaint said Dean said he posted the threat from a phone but took it down shortly after posting it, according to the criminal complaint.
University of Chicago President Robert Zimmer cited �recent tragic events on campuses across the country� in the decision to cancel Monday classes. After the arrest, he said the decision to cancel classes would remain in effect for the day and later added normal operations would resume on Tuesday.
College campuses have been the sites of several shootings this year, including an Oct. 1 massacre at Umpqua Community College in Oregon that left 10 dead including the shooter.
The University of Chicago, a private school with about 15,000 graduate and undergraduate students, is south of downtown Chicago.
(Additional reporting by Melissa Fares and Angela Moon in New York; Editing by Ben Klayman and Steve Orlofsky)
