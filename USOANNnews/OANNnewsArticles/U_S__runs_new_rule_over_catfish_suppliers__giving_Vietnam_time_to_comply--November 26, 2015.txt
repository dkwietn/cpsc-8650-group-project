Search for:
U.S. runs new rule over catfish suppliers, giving Vietnam time to comply
A noodler competing in the Okie Noodling Tournament carries his catfish in Pauls Valley, Oklahoma, June 30, 2007. REUTERS/Jessica Rinaldi
November 26, 2015
By David Lawder
WASHINGTON (Reuters) � The U.S. Department of Agriculture issued new rules for catfish suppliers on Wednesday, requiring on-site inspections of farms and processing plants for both domestic and foreign producers, mostly from Vietnam, to ensure they meet the same standards.
The long-awaited change puts in place rules similar to decades-old safety requirements for beef, pork and poultry, and shifts responsibility for the inspections to the USDA from the Food and Drug Administration, which regulates all other seafood imports.
The rule is expected to become effective in March 2016, and will be phased in over 18 months, giving foreign suppliers time to make changes needed to comply with the USDA�s requirements.
U.S. catfish farmers, concentrated in Mississippi and Alabama, have been battered in recent years by fast-growing imports of Vietnamese pangasius catfish, now the sixth most popular seafood in the United States.
The U.S. producers had accused the FDA of failing to halt imports of pangasius containing banned antibiotics and other chemicals, and persuaded lawmakers to pass legislation authorizing the new program, first in 2008 and reaffirmed in 2014.
�The point of this process has been to ensure that the farm-raised catfish served to American families is safe and nutritious,� said Senator Thad Cochran of Mississippi, a major proponent of the program. �The USDA is in the best position to get this done.�
Vietnamese producers and trade negotiators had expressed concerns that pangasius imports would be effectively banned until producers could meet the new USDA requirements, causing major disruptions to a significant export sector.
During the 18-month transition period, foreign supplies wishing to export catfish to the United States will be subject o at least quarterly inspections and residue sampling by the USDA�s Food Safety and Inspection Service.
�FSIS is committed to a smooth and gradual introduction to the new inspection program,� USDA Deputy Undersecretary for Food Safety Al Almanza said in a statement. �The agency will conduct extensive outreach to domestic industry and international partners so that they fully understand FSIS� requirements prior to full implementation.�
But some U.S. lawmakers repeated their objections to the catfish inspection program, calling it wasteful, redundant and a trade barrier aimed at protecting a small industry mainly located in Mississippi.
�As a result of this protectionist program, an estimated $15 million per year will be spent on enabling government bureaucrats to impose barriers on foreign catfish importers, which will in turn increase the price of catfish for American consumers, restaurants, and seafood processors,� Republican Senator John McCain said in a statement.
The final USDA catfish inspection rule can be read here:
http://www.fsis.usda.gov/wps/wcm/connect/878aa316-a70a-4297-b352-2d41becc8f73/2008-0031F.pdf?MOD=AJPERES
