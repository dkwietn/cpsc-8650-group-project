Search for:
El Faro owner seeks protection from death claims in ship�s sinking
A life preserver ring from the cargo ship El Faro is pictured in this still image from a October 4, 2015 U.S. Coast Guard handout video. REUTERS/U.S. Coast Guard/Handout via Reuters
November 1, 2015
By Barbara Liston
ORLANDO, Fla. (Reuters) � The owner of the U.S. cargo ship El Faro that sank in a powerful hurricane off the Bahamas nearly a month ago has filed for protection in Florida federal court from claims it is liable for the deaths of its 33 crew members.
Tote Services Inc filed for exoneration from or limitation of liability in U.S. district court in Jacksonville, Florida, on Friday, citing U.S. maritime law and saying El Faro �was in all respects seaworthy and properly manned� and that it bears no responsibility for its loss.
If it prevails, the company�s liability could top out at $1 million, or about $30,000 per lost crew member, Kurt Arnold, a lawyer for one of the victim�s families, said on Saturday.
The sinking of El Faro, and loss of everyone aboard, was the worst cargo shipping disaster involving a U.S.-flagged ship in more than three decades.
Arnold, of the Houston firm Arnold & Itkin, filed the fourth lawsuit against the company by families of its crew members on Wednesday.
�If Tote is telling the families and the press they want to do the right thing, then they should do it,� Arnold told Reuters in an emailed statement.
Tote�s spokesman would not comment on the litigation on Saturday.
El Faro disappeared while en route from Jacksonville to Puerto Rico after its captain reported a hull breach and�loss of propulsion as the vessel sailed into the path of Hurricane Joaquin.
Provisions of U.S. maritime law can limit a ship owner�s financial liability to either the value of the vessel and cargo after a disaster or to a value based on the tonnage of the vessel, Arnold said.
In its court filing, Tote asked for maximum liability of no more than about $15 million. It also asked the court for an injunction against the opening or prosecution of any legal actions taken against it over the loss of El Faro.
(Additional reporting by David Adams in Miami; Editing by Frank McGurty and Tom Brown)
