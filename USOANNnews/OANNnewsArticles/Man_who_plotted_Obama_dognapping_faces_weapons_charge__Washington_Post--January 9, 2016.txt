Search for:
Man who plotted Obama dognapping faces weapons charge: Washington Post
Bo (L) and Sunny, the Obama family's new puppy, are pictured on the South Lawn of the White House in Washington in this photo released on August 19, 2013 by the White House. REUTERS/Pete Souza/The White House/Handout via Reuters
January 9, 2016
WASHINGTON (Reuters) � A North Dakota man who allegedly plotted to kidnap one of the Obama family�s pet Portuguese water dogs was arrested with guns and ammunition at a downtown Washington hotel and is facing a weapons charge, The Washington Post reported on Friday.
The man, Scott D. Stockert, 49, of Dickinson, North Dakota, first came to the attention of the Minnesota field office of the U.S. Secret Service, which learned of his intention to kidnap Bo or Sunny, the Obamas� two dogs, the Post said, citing a court filing.
When Stockert drove his pickup truck from North Dakota to Washington, Secret Service officers located him at a Hampton Inn near the Convention Center in downtown Washington, close to both the White House and the Capitol building, the filing said.
Agents found a 12-gauge shotgun and a .22-caliber rifle in Stockert�s vehicle, along with 350 rounds of ammunition, a 12-inch (30-cm) machete and a billy club, the Post reported. The court documents said Stockert was not registered to own a gun and was arrested.
The Secret Service said in a court document that when he was arrested, Stockert made a series of outlandish claims to the agents, including that he was the son of John F. Kennedy and Marilyn Monroe and that he planned to run for president.
He also acknowledged being in Washington to kidnap Bo or Sunny. Stockert was charged with carrying a rifle or shotgun outside of his home or place of business, which is generally illegal in the District of Columbia, the Post said.
At his preliminary hearing on Friday, Stockert was released into an intensive supervision program pending a future court date on the charge. He also was ordered not to possess any weapons or go near the White House or Capitol, the Post said.
(Writing by David Alexander; Editing by Eric Walsh and Sandra Maler)
