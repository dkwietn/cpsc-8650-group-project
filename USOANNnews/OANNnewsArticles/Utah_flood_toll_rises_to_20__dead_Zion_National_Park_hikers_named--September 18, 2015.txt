Search for:
Utah flood toll rises to 20; dead Zion National Park hikers named
Residents walk across one of the many flooded streets after a flash flood in Hildale, Utah September 15, 2015. REUTERS/David Becker
September 18, 2015
(Reuters) � Seven hikers swept to their deaths in a rain-swollen canyon in Utah�s Zion National Park were warned in advance that flash floods were forecast as probable but obtained a park permit for the outing anyway, park officials say.
The overall death toll from Monday�s flash floods in southern Utah rose to 20 on Thursday with the recovery of the seventh hiker�s remains and the body of a man reported missing by his parents in the small town of Hurricane.
Officials named the seven deceased hikers on Friday and published a photo the group took of themselves before venturing into the narrow and challenging Keyhole Canyon in the eastern part of the park.
The smiling hikers, who were all in their 50s, are clad in hard hats and backpacks, and several are wearing wet suits to tackle the route, which involves swimming through pools of water and rappelling down steep gorges.
Park officials said six were from the Los Angeles area and one was from Mesquite, Nevada.
The group�s leader had experience exploring Keyhole and other canyons, said Cindy Purcell, Zion�s chief park ranger.
She said the group obtained its permit for the trip early on Monday, even though they were advised the National Weather Service had forecast a 40 percent chance of rain and the probability of flash flooding in the park.
�The ranger who handed that permit to that man said, �I would not go today,'� Purcell said on Thursday. �However, the people who go make the choice, they sign the paper that says that it is their safety and their responsibility.�
She said rangers would only decline to issue a permit if an official flash flood warning was posted. The NWS did issue such an advisory that day, but only after the group had already set out, she said.
The 20th fatality from Monday�s floods, a 33-year-old man from Hurricane, Utah, was found in Arizona almost seven miles (11 km) from the Utah border, and six miles (10 km) from his car.
In Hildale, Utah, on the sparsely populated border, crews were still searching for a 6-year-old boy missing since a wall of water swept away two cars packed with occupants on Monday, killing at least a dozen people.
�Even though search areas have been covered, they need to be searched again multiple times,� Washington County officials said in a statement. �The goal: Bring Tyson Black home.�
(Reporting by Steve Gorman in Los Angeles, David Schwartz in Phoenix and Daniel Wallis in Denver; Editing by Eric Beech)
