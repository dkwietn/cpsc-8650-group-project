Search for:
California governor vetoes bill mandating campus sexual assault sanctions
California Governor Jerry Brown waits to speak during a news conference at the State Capitol in Sacramento, California March 19, 2015. REUTERS/Max Whittaker
October 12, 2015
By Sharon Bernstein
SACRAMENTO, Calif. (Reuters) � California Governor Jerry Brown on Sunday vetoed a bill that would have required most public and private colleges in the state to expel or suspend for a minimum of two years students found to have violated certain campus sexual assault policies.
The bill, which would have required colleges and universities that receive funds from the state to develop detailed sexual assault policies and report on the results, was submitted amid rising concern about sexual assaults on U.S. campuses.
It also mandated punishment for students found to have committed �egregious� violations of the policies, including expulsion, a two-year suspension, and the loss of financial aid and scholarships.
Brown said in his veto message that the measure would not allow professionals on campus to use discretion when investigating allegations of sexual assault or disciplining students for their actions.
The Democratic governor also said it could lead to an expectation that the state should play a role in developing penalties for violating university policies.
�I don�t think it�s necessary at this point for the state to directly insert itself into the disciplinary and governing processes of all private nonprofit and public colleges in California,� Brown said.
The bill also would have required private and public colleges and universities to issue a report every two years showing the number of allegations of campus sexual assault, the results of their investigations and any disciplinary action taken.
A previous version of the bill defined �egregious� offenses as including rape, forced sodomy and other acts, but that level of specificity was removed after both public and private institutions expressed concerns that the bill would require them to find students guilty of specific crimes.
The universities said such investigations and findings should be left to the judicial system.
Last year, Brown signed a bill aimed at reducing confusion over sexual assault on campus by redefining what it means for someone to consent to sexual activity.
That law, known colloquially as �Yes Means Yes,� requires both participants in a sexual act to affirmatively state that they want to take part.
(Editing by Jonathan Oatis)
