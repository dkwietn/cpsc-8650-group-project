Search for:
Students killed in Seattle bus crash were from four countries
Personnel remove the bodies of victims from the scene of a crash between a Ride the Ducks vehicle and a charter bus on Aurora Bridge in Seattle, Washington September 24, 2015. REUTERS/Jason Redmond
September 27, 2015
By Eric M. Johnson
SEATTLE (Reuters) � Four college students killed in the crash of their charter bus and an amphibious tour bus on a Seattle bridge were from Austria, China, Indonesia and Japan, their school said on Friday.
The dead were Claudia Derschmidt, 49, of Austria; Privando Putradanto, 18, of Indonesia; Mami Sato, 36, from Japan; and Runjie Song, a 17-year-old from China, North Seattle College said in a statement.
About 90 firefighters and medics responded to the scene of Thursday afternoon�s head-on collision on the busy Aurora Bridge, and about 50 people were treated at hospitals, authorities said.
The college said several students remained in critical condition, and other students and a North employee sustained serious injuries.
The crash left the charter bus caved in with glass and debris on the ground, and the Ride the Ducks amphibious bus also crumpled and missing a front wheel.
About 45 students and employees from North Seattle College�s international programs were on the bus. They were headed to a downtown stadium as part of a new student orientation program.
Investigators are reviewing witness reports of a possible mechanical issue on the duck vehicle, police said on Thursday.
The city was notifying victims� family and helping to coordinate travel to the United States.
Traffic safety on the bridge, which has no median barrier, has been a concern of state and local officials. The National Transportation Safety Board is investigating the crash.
The accident comes nearly five months after an amphibious sightseeing vehicle hit and killed a woman on a Philadelphia street. In 2010, two tourists were killed when a tugboat pushed a barge into a similar vehicle, also in Philadelphia.
(Reporting by Daniel Wallis in Denver and Eric M. Johnson in Seattle; Editing by Susan Heavey and Bill Trott)
