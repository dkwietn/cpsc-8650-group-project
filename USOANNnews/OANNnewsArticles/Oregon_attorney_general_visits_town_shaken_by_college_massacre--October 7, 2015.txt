Search for:
Oregon attorney general visits town shaken by college massacre
Melody Siewell leaves flowers at a memorial outside Umpqua Community College in Roseburg, Oregon, United States, October 3, 2015. REUTERS/Lucy Nicholson
October 7, 2015
By Eric M. Johnson and Emily Flitter
ROSEBURG, Ore. (Reuters) � Oregon�s top law enforcement officer paid a visit to the grief-stricken town of Roseburg on Tuesday to meet with authorities investigating last week�s massacre on a community college campus where a gunman killed his professor and eight classmates.
The trip by state Attorney General Ellen Rosenblum comes a day before Oregon state police planned to hold a news conference to give an update on the role played by officers who exchanged gunfire with the suspect before he committed suicide.
Authorities initially suggested the gunman had been killed in a shootout with two Roseburg police officers arriving on the scene of Thursday�s attack in a classroom building at Umpqua Community College in Roseburg.
But Douglas County Sheriff John Hanlin later revealed medical examiners had determined the assailant, Christopher Harper-Mercer, 26, died from a self-inflicted gunshot wound.
Hanlin also has said police were on the scene within five minutes of the first call for help, that they reported confronting the gunman two minutes later, and that their exchange of gunfire had �neutralized� the suspect.
Authorities have disclosed few details about the circumstances of the rampage itself. And they have revealed little of what they know about the motives driving Harper-Mercer � by all accounts a troubled, socially awkward loner with a passion for guns � to carry out the deadliest U.S. mass shooting in two years, and the bloodiest in modern Oregon history.
According to survivors� accounts, the suspect stormed into his introductory writing class to shoot his professor, then began gunning down cowering classmates one at a time as he questioned them about their religion.
Parents of two survivors revealed over the weekend that the assailant had handed an envelope containing a flash-drive to one of the male students in the class, whose life the suspect deliberately spared.
In addition to the nine people slain, nine others were wounded in the hail of gunfire.
One, Chris Mintz, 30, a U.S. Army combat veteran who served in Iraq, was credited with likely saving lives when he confronted the gunman outside another classroom before police arrived, drawing fire that left him with seven bullet wounds and two broken legs, according to his former girlfriend.
Rosenblum, a Democrat and former federal prosecutor who became the first woman elected Oregon attorney general in 2012, visited Roseburg at the invitation of District Attorney Rick Wesenberg.
Democratic President Barack Obama, who spoke out forcefully in favor of stricter gun control measures after the massacre, plans to visit Roseburg on Friday to meet privately with families of the victims, the White House said this week.
Roseburg Mayor Larry Rich, a Republican and self-described supporter of gun rights in the former timber community 180 miles (300 km) south of Portland, said he welcomed the Democratic president to visit when the White House called on Monday to ask whether Obama should make the trip.
(Writing by Steve Gorman; Editing by Lisa Shumaker)
