Search for:
Vigil honors California student slain in Paris attacks
A woman places flowers at a makeshift shrine in honor of Nohemi Gonzalez at California State University in Long Beach, California November 15, 2015. REUTERS/Jonathan Alcorn
November 16, 2015
By Tori Richards
LONG BEACH, Calif. (Reuters) � More than 1,000 people overflowed a ballroom at California State University, Long Beach, on Sunday to honor and remember an exchange student who was cut down indiscrimately by suspected Islamic State militants in Paris on Friday.
Nohemi Gonzalez, 23, was dining at a restaurant fired upon by gunmen as part of a coordinated assault that killed 132 people and wounded more than 300 in the French capital city.
Gonzalez, of El Monte, California, was a senior at CSULB just south of Los Angeles. She was studying for a semester at Strate College of Design in suburban Paris.
School officials passed out yellow and black ribbons to an estimated 700 people who filled every seat in the student union ballroom. An estimated 500 more people gathered outside, where a candlelight vigil was held after the 45-minute program.
Speakers described Gonzalez as a cheerful, hard-working student of industrial products design who loved pug dogs and was thrilled to be in Paris.
�Nohemi�s senseless murder is our worst nightmare,� university President Jane Close Conoley said. �Taking the life of an innocent is an assault on our hearts and on our world. Nohemi was an innocent of great promise, a light in our community.�
Gonzalez was among 17 CSULB students in France studying this semester, university officials said. All of the others have been accounted for, they said.
�She was so excited to be in Paris,� said Martin Herman, a professor and chairman of the university�s Department of Design. �She charged into that adventure with openness and wonder on her way to being a designer.�
Jose Hernandez, Gonzalez�s stepfather, referred to her by her nickname, Mimi, and said he doesn�t think of her as gone.
�Mimi is in our hearts,� he said. �Mimi is not dead, she is right here.�
Classmate Tanya Flores said Gonzalez was rarely in a bad mood.
�The only time I saw you angry was when you were hungry or needed catsup,� she said. �Don�t forget to design some badass stuff when you are in heaven.�
Christophe Lemoine, Consul General of France in Los Angeles, represented the mourning country at the service.
�We are extremely grateful of the support of the United States,� Lemoine said. �It�s very important for me to be here today to express my respects and all my condolences in my name and in the name of the French people.�
Vigils were held across the country on Sunday to honor Gonzalez and other victims of the attacks.
In Boston, several hundred people gathered in historic Bostom Common park to show solidarity with Paris. The consul general, Massachusetts governor and Boston mayor attended, as did Boston Marathon bombing witness Carlos Arredondo. Flowers were left at a monument to the Marquis de Lafayette.
In New York, the Coalition for Jewish Concerns-AMCHA held a memorial vigil at Riverdale Monument in the Bronx in memory of those who died in Paris and to demand an end to Islamic Terror, said organization President, Rabbi Avi Weiss.
An interfaith vigil with the French community was organized on Sunday evening in Carroll Park in Brooklyn. Those attending planned to walk carrying candles to an interfaith service at St. Agnes Catholic Church, which regularly offers mass in French.
(Reporting by Tori Richards in Long Beach. Additional reporting by Kevin Murphy in Kansas City and Brian Snyder in Boston)
