Search for:
Obama says U.S. is safe as millions set off on Thanksgiving travel
U.S. President Barack Obama holds a news conference at the conclusion of his ASEAN Summit and East Asia Summit (EAS) meetings in Kuala Lumpur, Malaysia November 22, 2015. REUTERS/Jonathan Ernst
November 26, 2015
By Roberta Rampton and Kylie Gumpert
WASHINGTON/NEW YORK (Reuters) � President Barack Obama sought to reassure Americans they were safe as millions of travelers set off for the long Thanksgiving weekend on Wednesday and authorities stepped up security at airports in response to the attacks in Paris two weeks ago.
In New York City, record-breaking crowds were expected on Thursday for the annual Macy�s Thanksgiving Day parade, and Police Commissioner William Bratton said the city was deploying more officers at the annual event than ever before.
�Right now, we know of no specific and credible intelligence indicating a plot on the homeland,� Obama told reporters at the White House, two weeks after suspected Islamist militants killed 130 people in a series of coordinated attacks in the French capital.
�We are taking every possible step to keep our homeland safe,� he said, flanked by his FBI director and other top security officials on the day before Thanksgiving, when many Americans travel to be with their extended families for a traditional turkey dinner.
Nearly 46.9 million Americans will travel over the long Thanksgiving weekend � the busiest U.S. travel holiday of the year � with 3.6 million going by plane, according to the AAA, a motorist advocacy group.
Most U.S. airports reported flights delays of less than 15 minutes, according to tracking websites. Passengers at airports from Washington to New York said they saw heavier than normal security, but that travel was flowing smoothly.
Americans have become more concerned about threats since the Paris attacks and now identify terrorism as the most important problem facing the nation, Reuters-Ipsos polling shows.
�We have to live our lives right? We�re having a good time. We did a cruise and now we�re doing New York City,� said Karen Damaschino, 47, of San Francisco after landing at John F. Kennedy International Airport to spend the holiday in New York.
The U.S. response to Islamic State has become a top issue in the race to succeed Obama in the November 2016 presidential election. In his statement, Obama tried to allay Americans� concerns.
�I know that families have discussed their fears about the threat of terrorism around the dinner table, many for the first time since September 11th,� he said, referring to the 2001 attacks by al Qaeda on New York and Washington.
But he told Americans they should �go about their usual Thanksgiving weekend activities� while remaining vigilant to any suspicious activities.
New York Mayor Bill de Blasio echoed that sentiment at a news conference on Manhattan�s Upper West Side, where crews were inflating the giant balloons that highlight the Macy�s parade, the traditional start to the holiday shopping season.
�One thing I always say, there are some people trying to intimidate New Yorkers. Well, New Yorkers don�t get intimidated,� he said. �They�ll be out tomorrow in droves.�
STAY VIGILANT
To underscore Obama�s message, his Homeland Security Secretary Jeh Johnson held a photo op at Washington�s Union Station just before boarding an Amtrak train to Newark, N.J., on the heavily traveled Northeast corridor, en route home for the holiday
�It should be obvious to the public that there is a heightened presence� of law enforcement officers at train stations, airports and other public places, Johnson said. �We are working overtime to protect the homeland.�
Some travel analysts expected airport delays as a result of heightened security.
But at Chicago�s O�Hare International Airport, one of the country�s busiest, Kirsten Bohling, 27, said she was pleasantly surprised by how painless her check-in went.
�Lines are way shorter than I thought they would be and moving 10 times faster than I thought,� she said. �I think they were really really prepared.�
The FBI sent a bulletin earlier this week to police departments across the country warning of possible copycat incidents after the Nov. 13 Paris attacks and sharing intelligence on how the attacks were carried out.
The U.S. State Department also issued a worldwide travel alert on Monday warning American travelers to remain vigilant, particularly when visiting foreign countries.
As many as 3.5 million people were expected to line the 2.5-mile (4 km) route of the Macy�s parade in New York, according to organizers. City officials have made numerous public appearances in recent days seeking to reassure New Yorkers and tourists.
(Additional reporting by Joseph Ax, Barbara Goldberg, Sarah Irwin and Kylie Gumpert in New York, Renita Young in Chicago, and Jeff Mason, Doina Chiacu, Megan Cassella and Mana Rabiee in Washington; Editing by Frank McGurty and Tom Brown)
