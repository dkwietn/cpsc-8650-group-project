Search for:
Florida circus elephants find second career in research
Trainer Jim Williams walks Asian elephants Icky (L) and Alana to a field at the Ringling Bros. and Barnum & Bailey Center for Elephant Conservation in Polk City, Florida September 30, 2015. REUTERS/Scott Audette
October 11, 2015
By Barbara Liston
POLK CITY, Fla. (Reuters) � At a Florida retirement home for former circus elephants, residents enjoy a steady diet of high-quality hay and local fruits and vegetables, as well as baths and occasional walks.
For these majestic beasts, this life of relative leisure at the 200-acre Center for Elephant Conservation comes after years on the road, entertaining America in �The Greatest Show on Earth��for Ringling Bros and Barnum & Bailey Circus. Under pressure of animal rights activists, the circus is now phasing the animals out of its show.
In March, the circus company announced with some reluctance that it would end its elephant acts by 2018. It said it wanted to use the retired animals to help save their endangered species through breeding and medical research.
�If we didn�t do it, the elephants would go extinct in North America, likely in 25 years or less,� said Kenneth Feld, chief executive officer of the circus� owner, Feld Entertainment.
Animal welfare groups, which for years have accused the circus of mistreating the elephants, are still not satisfied and have questioned the company�s current plan.
�Certainly it�s an improvement to take the elephants off the road, but Ringling should immediately retire all its elephants to real sanctuaries instead of a bogus breeding farm in Florida,� said Rachel Mathews, legal counsel to People for the Ethical Treatment of Animals. �They should be allowed to just be elephants.�
DWINDLING POPULATION
Asian elephants, which are smaller and rarer than the African variety, have long been an audience favorite under the big top.
Fewer than 40,000 Asian elephants remain in the wild. About 250 are in captivity in the United States, 26 of which were born over the past 20 years at Ringling facilities.
The ranch-like property in central Florida where 29 of the circus company�s 42 Asian elephants now live is about an hour�s drive south of The Villages, the sprawling human retirement destination and golfer�s paradise.
The elephants spend their days outdoors in fenced enclosures where they are in sight or earshot of one another and enjoy loaves of white bread as an occasional treat. At night they stay in large barns, with their feet often chained to keep them from stealing each other�s food.
But the animals have not left their former lives behind altogether. They continue to receive training to make them easier to manage, staff said.
The elephants are still trained to lie down, which allows workers scrub them, and to put a foot on a stool, which helps in grooming their toenails.
Their close interaction with trainers allows the staff to easily take weekly blood samples from the large veins behind their ears. The blood is used to monitor the elephants� health and provide specimens for research.A Ringling spokesman could not say how many, if any, elephants had been officially retired since the announcement, only that the company still has 13 performing in three units.
Feld said he was saddened by the decision to remove the elephants, which have been fixtures in circus shows for more than a century, but the company is committed to helping protect and preserve the species.
One example is the elephant conservation center�s work with the Smithsonian�s National Zoo. The aim is to improve the diagnosis and treatment of a herpes virus that kills 20 percent of elephant calves born in North America, said veterinarian Dennis Schmitt, Ringling�s director of research.
On reproduction of the species, Ringling has established a Genomic Resource Bank for Asian elephants and has achieved a live birth through artificial insemination.
The center, which is not open to the public, has also helped in cancer prevention efforts for humans.
Despite a far-greater body mass and far more cells than humans, the lifetime cancer mortality risk for elephants is less than 5 percent, compared with 11 percent to 25 percent in humans, according to a new study published Thursday in the Journal of the American Medical Association.
The study, whose lead author is Dr. Joshua Schiffman of the University of Utah�s Huntsman Cancer Institute, used blood samples from Ringling elephants.
Maintaining the elephants costs Feld�s company $69,000 per animal per year. Despite the expense, he said preserving the species both in its native habitat and in North America was important for future generations.
�We bring things to you that you normally can�t see,� said Feld, who is still considering how the retired elephants might interact with the public in a new way. �Most�people can�t afford to go to Sri Lanka.�
(Editing by David Adams, Frank McGurty and Lisa Von Ahn)
