Search for:
Obama praises Secret Service handling of Pope, foreign leader visits
U.S. President Barack Obama (R) meets with Pope Francis in the Oval Office of the White House in Washington September 23, 2015. REUTERS/Jonathan Ernst
September 29, 2015
By Julia Edwards
WASHINGTON (Reuters) � President Barack Obama praised the U.S. Secret Service for its work over the past week, which included visits to the White House by Pope Francis and Chinese President Xi Jinping as well as the United Nations General Assembly meeting in New York.
Speaking to reporters after returning from New York, Obama said the Secret Service had done its job protecting him, the public and foreign dignitaries last week �flawlessly.�
�They all deserve a huge round of applause for being such great hosts and keeping everybody safe,� the president said, offering praise to an agency that faced sharp criticism last year for a string of security breaches.
In an incident last September, a man jumped the White House fence and entered the front door carrying a knife. Just days before that, a security contractor with a gun got on an elevator with the president during a trip to Atlanta.
Secret Service Director Julia Pierson resigned under pressure from Congress several weeks after the fence-jumping incident and was replaced by Joseph Clancy, the former head of Obama�s protective detail, who came out of retirement to accept the post.
�When something goes wrong, when there�s a fence jumper, everybody reports on it,� Obama said, highlighting the recent accomplishments of the agency that protects him and his family.
The Secret Service has plans to hire 1,000 more staff over the next five years to combat its lapses in security.
(Reporting by Julia Edwards; Editing by Sandra Maler)
