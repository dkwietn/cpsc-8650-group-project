Search for:
Seattle tour operator in deadly crash broke safety laws � state
Personnel remove the bodies of victims from the scene of a crash between a Ride the Ducks vehicle and a charter bus on Aurora Bridge in Seattle, Washington September 24, 2015. REUTERS/Jason Redmond
December 15, 2015
SEATTLE (Reuters) � The operator of a fleet of amphibious tour buses involved in a crash in Seattle that left five foreign students dead violated safety regulations 442 times and could face penalties of as much as $1,000 per infraction, Washington state authorities said on Tuesday.
The Sept. 24 collision of a Ride the Ducks vehicle with a charter bus carrying international students on Seattle�s Aurora Bridge increased scrutiny of the duck boat-buses, which have been involved in a number of deadly crashes in recent years.
During a post-crash investigation into vehicle maintenance and safety practices of Ride the Ducks of Seattle LLC, state transportation safety officials found 442 violations of motor carrier safety rules or laws, the Utilities and Transportation Commission said in a statement, without specifying when the violations took place.
The commission also recommended monetary penalties against the company and said it was authorized to impose fines of as much as $1,000 per violation, it said.
Ride the Ducks of Seattle LLC did not immediately respond to a request for comment.
The commission suspended all Ride the Ducks operations in the state four days after the crash. The company had already stopped its tours.
The commission will hold a hearing on Monday to determine if the company will be allowed to return to service. It will address any penalties levied against the company at a later date.
The investigation resulted in a proposed �unsatisfactory safety� rating for the company based on seven violations under federal law, the commission said. Those violations are related to allowing a driver to operate a commercial vehicle without a proper license and failing to conduct the required number of random controlled-substances tests.
Officials also found two recordable accidents in 2015, it said. The company must correct these violations in 45 days, according to federal regulations, or be placed out of service.
The commission has the authority to regulate companies like Ride the Ducks, but the authority to determine the cause of the crash rests with federal regulators.
The U.S. National Transportation Safety Board has said the U.S. Army-surplus vehicle that crashed was built in 1945 and refurbished in 2005.
It also said the left front axle of the amphibious vehicle was sheared off, but it was unclear how that occurred, though early indications suggest the axle was sheared off before the collision.
(Reporting by Eric M. Johnson in Seattle; Editing by Matthew Lewis and Bill Rigby)
