Search for:
Witness disputes injury timing in Baltimore policeman�s trial
Baltimore Police officer William Porter approaches the court House in Baltimore, Maryland, November 30, 2015. REUTERS/Patrick Samansky/Pool
December 10, 2015
By Donna Owens
BALTIMORE (Reuters) � A neurosurgeon on Thursday testified in the trial of a Baltimore police officer charged in the death of a black man that a fatal spinal injury occurred later in a police van trip than prosecutors have asserted.
Dr. Matthew Ammerman, a defense witness, said during the trial of Officer William Porter that the injury to Freddie Gray took place between the fifth and sixth stops of the April 12 van ride.
Porter, who is black, faces manslaughter and other charges over Gray�s death, which triggered rioting in the largely black city and intensified a U.S. debate on police brutality.
Gray, 25, was arrested after he fled from officers. He was bundled into a transport van while shackled and handcuffed, and was not in a seat belt. He died a week later from a neck injury that Ammerman described as �catastrophic.�
A medical examiner has testified that Gray�s injury took place between the van�s second and fourth stops. Porter said on the witness stand on Wednesday that Gray asked for help at the fourth stop and he helped him onto a van bench.
�Would Mr. Gray have been able to sit erect on the bench at stop four?� defense lawyer Joseph Murtha asked Ammerman in Baltimore City Circuit Court.
�No,� said Ammerman, a Washington neurosurgeon. The defense contended that Porter did not believe Gray was seriously injured until the sixth and last stop, a police station.
Porter, 26, is the first of six officers, three of them black, to face trial. Charges against the other officers range from second-degree murder for the van�s driver to misconduct.
Porter testified that he did not seek help for Gray at the fourth stop because he did not see any injury and Gray did not tell him what was wrong.
His lawyers have suggested that the van driver and a supervising sergeant were responsible for seeking help.
Another defense witness, Timothy Longo, the police chief in Charlottesville, Virginia, was asked by Murtha which officer was responsible when transporting a detainee.
�The wagon driver has ultimate responsibility,� said Longo, an expert on police procedure.
Prosecutors contend that Porter ignored Gray�s pleas for medical aid and that his failure to secure him with a seat belt violated police policy.
Porter faces charges of involuntary manslaughter, second-degree assault, reckless endangerment and misconduct in office. He faces more than 25 years in prison if convicted on all counts.
(Writing by Ian Simpson; Editing by Andrew Hay and Jonathan Oatis)
