Search for:
Louisiana theater shooter thanked accused church attacker in journals
Investigators stand outside a movie theatre where a man shot and killed filmgoers Thursday night in Lafayette, Louisiana July 24, 2015. REUTERS/Lee Celano
January 13, 2016
By Bryn Stole
BATON ROUGE, La. (Reuters) � The man who killed two moviegoers and wounded nine in a Louisiana theater shooting last summer left behind notebooks in which he ranted against the United States and thanked Dylann Roof, the man accused of a church shooting in South Carolina, according to records released on Wednesday.
The rambling writings by John Russell Houser, 59, who shot himself to death moments after his deadly July 2015 rampage at a Lafayette, Louisiana, showing of �Trainwreck,� included references to the movie and his own death, according to records released by Lafayette police.
Houser had frequently railed against the U.S. government online and expressed an affinity for white supremacist ideology.
In the journals, Houser described Roof, who is charged with murder in the shooting deaths of nine people at a historically African-American church in Charleston, South Carolina, as �green but good.�
�Had Dylan (sic) Roof reached political maturity he would have seen the word is not n�-, but liberal,� Houser wrote. �But thank you for the wake up call Dylan.�
One journal was found open next to a pair of glasses on an unmade bed in Room 129 at a Motel 6 in Lafayette, where Houser was staying, according to 80 photos released by police.
The room was strewn with clutter, including disguises, an empty bottle of whisky and a box for a 40-caliber handgun, the same type used in the shooting.
Houser opined against America the �filth farm,� affirmative action, media �slimeballs,� abortion, pornography and homosexuality.
On a page titled �Signs of my radicalization,� Houser wrote that he had designed �a new logo for ISIS.�
Houser was from Phenix City, Alabama. The journals, which contained about 40 pages, do not say what had brought him to Lafayette and do not detail his attack plan.
Police said he planned to escape, but Houser made references to his own death and letting the journal speak for him.
�Therefore I leave it all, in hopes of truth, my death all but assured,� he wrote, noting the time as 6:14 p.m. on the day of the shooting.
At 6:33 p.m., he wrote: �Trainwreck, 7:15.�
(Reporting by Bryn Stole; Writing by Karen Brooks; Editing by Leslie Adler)
