Search for:
Baltimore mayor wants to make interim police chief�s role permanent
Baltimore Mayor Stephanie Rawlings-Blake speaks during a news conference where she announced the appointment of Deputy Commissioner Kevin Davis (L) as Interim Commission of Police in Baltimore, Maryland July 8, 2015. REUTERS/Bryan Woolston
September 14, 2015
By Donna Owens
BALTIMORE (Reuters) � Baltimore Mayor Stephanie Rawlings-Blake on Monday asked the City Council to confirm the city�s interim police commissioner as a permanent replacement for the official fired following racially fueled riots in April.
Details regarding a contract for Kevin Davis still need to be worked out after Rawlings-Blake, who last week said she would not seek re-election next year, asked the council to make him his appointment permanent.
�Those details will be finalized in the coming days and then submitted to the City Council for approval,� said city spokesman Kevin Harris.
Davis, 46, was a deputy commissioner overseeing investigations in July when Rawlings-Blake fired Anthony Batts as commissioner. Batts had come under heavy criticism because of the April death of an unarmed black man of injuries sustained while in police custody.
The death of Freddie Gray, which came after the United States already had seen months of protests over police use of force, sparked the worst rioting that Baltimore had experienced in half a century. The six police officers involved in Gray�s arrest are awaiting trial in the case.
Davis on Monday thanked the mayor for the vote of confidence.
� �We will meet the challenges of our times with determination, transparency and clarity of leadership,� he said.
Murders have soared in Baltimore in recent months to levels not seen since the 1970s. The city has had 235 murders this year, according to a police spokesperson, compared with 211 for all of 2014.
Data provided by City Hall shows that Baltimore paid out about $12 million from 2010 to 2014 to settle claims and judgments against police. The amount dropped from $4.4 million in 2010 to $1.6 million last year, and 2015 figures will be compiled when the year is over.
Last week, the city settled a civil claim with Gray�s family for $6.4 million dollars.
(Reporting by Donna Owens; Editing by Scott Malone and Bill Trott)
