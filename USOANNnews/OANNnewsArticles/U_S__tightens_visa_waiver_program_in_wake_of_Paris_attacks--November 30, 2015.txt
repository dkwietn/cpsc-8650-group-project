Search for:
U.S. tightens visa waiver program in wake of Paris attacks
The logo of the U.S. Department of Homeland Security is reflected in the spectacles of an analyst working in a watch and warning center of a cyber security defense lab at the Idaho National Laboratory in Idaho Falls, Idaho September 29, 2011. REUTERS/Jim Urquhart
November 30, 2015
By Roberta Rampton and Mark Hosenball
WASHINGTON (Reuters) � The White House announced changes to the U.S. visa waiver program on Monday so that security officials can more closely screen travelers from 38 countries allowed to enter the United States without obtaining visas before they travel.
Under the new measures, which were prompted by the Nov. 13 attacks in Paris by Islamic State militants, the Department of Homeland Security would immediately start to collect more information from travelers about past visits to countries such as Syria and Iraq, the White House said.
The changes will �enhance our ability to thwart terrorist attempts to travel on lost or stolen passports,� White House spokesman Josh Earnest told reporters in Paris, where President Barack Obama is attending U.N. talks on climate change.
The DHS would also look at pilot programs for collecting biometric information such as fingerprints from visa waiver travelers, the White House said.
The DHS would also ask Congress for additional powers, including the authority to increase fines for air carriers that fail to verify passport data, and the ability to require all travelers to use passports with embedded security chips, the White House said.
The White House also wants to expand the use of a �preclearance program� in foreign airports to allow U.S. border officials to collect and screen biometric information before visa waiver travelers can board airplanes to the United States.
The White House urged Congress to pass legislation before leaving Washington later in December for a holiday recess.
�Surely over the course of the next three weeks, they should be able to do something that actually would strengthen our national security,� Earnest told reporters.
A task force in the House of Representatives plans to meet on Tuesday to discuss the program and wants to craft legislation to pass �by the end of the year,� Republican Representative Kevin McCarthy, the House Majority Leader, said on Monday.
McCarthy told reporters that lawmakers were interested in requiring all countries in the waiver program to issue �e-passports� with chips and biometrics. One change would be to make sure that passengers were screened against a database of lost and stolen passports.
After the Paris attacks, the House passed a bill that would bar refugees from Syria and Iraq from entering the United States until security officials certify that they are not threats. The bill would cripple Obama�s plan to accept 10,000 refugees in the next year and he has vowed to veto it.
But the White House has decided to give regular updates to state governors about refugees who resettle in their states, Earnest said.
U.S. officials have quietly acknowledged that they are far more worried about the possibility that would-be attackers from the Islamic State or other militant groups could enter the United States as travelers from visa waiver countries rather than as Syrian refugees.
The U.S. government routinely takes 18 to 24 months to screen would-be Syrian refugees before they are allowed to board flights to the United States.
In contrast, an estimated 20 million people fly to the United States each year from visa waiver countries such as France and Britain.
Officials have acknowledged that a European traveling to Syria to train with a group like Islamic State might be able to later enter the United States without significant scrutiny, if they are not already known to U.S. intelligence or partners such as Britain�s domestic intelligence agency MI5 or France�s DGSI.
(Additional reporting by Jeff Mason in Paris and Susan Cornwell, Patricia Zengerle and Julia Edwards in Washington; Editing by Doina Chiacu and Grant McCool)
