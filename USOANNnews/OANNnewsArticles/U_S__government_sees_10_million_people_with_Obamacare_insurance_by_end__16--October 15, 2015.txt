Search for:
U.S. government sees 10 million people with Obamacare insurance by end �16
A pharmacy employee looks for medication as she works to fill a prescription while working at a pharmacy in New York December 23, 2009. REUTERS/Lucas Jackson
October 15, 2015
By Caroline Humer
WASHINGTON (Reuters) � U.S. health officials said they expect 10 million people to be enrolled in healthcare plans through state-based insurance exchanges by the end of 2016, with more than one-quarter of eligible uninsured Americans signing up during this fall�s open enrolment period.
In estimates released on Thursday, the U.S. Department of Health and Human Services forecast that 7.3 million to 8.8 million individuals who currently have insurance through the marketplaces will re-enroll for next year.
Another 2.8 million to 3.9 million new customers will buy insurance on the exchanges when enrolment begins on Nov. 1, the agency said.
The 2016 forecast represents an increase of about 900,000 people next year. The government has said it expects that about 9.1 million people would have effectuated coverage at the end of 2015 through the state-based exchanges created through President Barack Obama�s national healthcare reform law. Such plans first took effect at the beginning of 2014.
Health and Human Services Secretary Sylvia Burwell said in prepared remarks that the estimate for 2016 effectuated coverage of 10 million people is a target within an estimated range of 9.4 million to 11.4 million.
Effectuated coverage means those people have an active policy at that date.
About 17.6 million people have gained insurance coverage through the exchanges and through the expansion of Medicaid plans for the poor since early 2014.
The exchange-based plans are sold by private insurers including Aetna Inc, Anthem Inc and UnitedHealth Group Inc. More than 85 percent of individuals with these plans receive a government subsidy based on their income, aimed at making them more affordable.
(Reporting by Caroline Humer in New York and Lisa Lambert in Washington D.C.; Editing by Susan Heavey and Matthew Lewis)
