Search for:
New York City ramps up security for New Year�s Eve celebration
New York Mayor Bill de Blasio attends a news conference about the two NYPD police officers who were fatally shot in the Brooklyn borough of New York, December 22, 2014 . REUTERS/Carlo Allegri
December 29, 2015
By Laila Kearney
NEW YORK (Reuters) � A six-ton crystal ball is ready to be lowered above New York City, colorful paper has been ground into confetti that will rain on Times Square and about 6,000 police officers will guard revelers on New Year�s Eve at one of the city�s highest-profile events.
�We are ready,� Mayor Bill de Blasio told a news conference on Tuesday. �We are the best prepared city in the country, the best prepared city to prevent terrorism and to deal with any event that could occur.�
An estimated one million peo ple are expected in the vicinity of One Times Square on Thursday for the New Year�s Eve dropping of the ball, a tradition begun in 1907 and broken only during wartime blackouts.
In the aftermath of Islamist-inspired attacks in San Bernardino, California, and Paris, the city will draw on its new Critical Response Command counterterrorism unit, which includes more heavily armed officers, to patrol Times Square. The unit, trained to detect and respond to attack plots, was commissioned days before the Macy�s Thanksgiving Day Parade.
Officers communicating with smart phones and loud speakers will be empowered to shut down the event at any time and evacuate the area, Police Commissioner William Bratton said, adding that there were no known credible threats against New York City.
In November after attacks in Paris that left 130 people dead, the Islamic State militant group, also known as ISIS, released a video that showed a glimpse of Times Square and then a suicide bomber.
Early on Thursday morning police will start closing off major thoroughfares, using barricades to contain celebrants and detour traffic. The city will also deploy about 6,000 uniformed and undercover officers, some 500 more than last year, to patrol Times Square.
Backpacks and large bags have been barred from the area. All other bags will be searched and metal detectors will be used to screen everyone in the crowd.
Bomb-sniffing dogs and radiation detectors will also be employed during the event.
�We are very confident that New Year�s Eve in New York City will be the safest place in the world to be,� said Chief of Counterterrorism James Waters.
(Reporting by Laila Kearney; Additional reporting by Joseph Ax; Editing by Toni Reinhold)
