Search for:
Number of confirmed E. coli cases linked to Chipotle rises to 40
A sign is seen at a Chipotle Mexican Grill restaurant in San Francisco, California, in this file photo taken July 21, 2015. REUTERS/Robert Galbraith/Files
November 5, 2015
LOS ANGELES (Reuters) � The number of confirmed E. coli food poisoning cases linked to Chipotle Mexican Grill Inc <CMG.N> restaurants in Washington state and Oregon rose to 40 on Thursday, as health safety officials continued searching for the source of the contamination.
Oregon increased its confirmed case count to 12 from 10 on Wednesday.
Washington lowered its count to 28 after additional testing ruled out one case that was suspected to be linked to the outbreak.
Eight Chipotle restaurants in the greater Seattle and Portland areas are linked to the outbreak. As a precaution all of the chain�s 43 restaurants in those markets have been closed since Oct. 31.
Investigators suspect that contaminated produce is the source of the outbreak, but they have not yet pinpointed the culprit.
(Reporting by Lisa Baertlein in Los Angeles; Editing by Tom Brown)
