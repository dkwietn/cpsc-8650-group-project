Search for:
Talks to reform Ferguson, Missouri police make progress
Police officers point their weapons at demonstrators protesting against the shooting death of Michael Brown in Ferguson, Missouri August 18, 2014. REUTERS/Joshua Lott
December 16, 2015
By Julia Edwards and Mary Wisniewski
WASHINGTON/CHICAGO (Reuters) � The U.S. Department of Justice and the mayor of Ferguson, Missouri on Wednesday said that they have made progress on a deal to overhaul the city�s police department following the 2014 fatal shooting of unarmed black teenager Michael Brown.
Justice Department spokeswoman Dena Iverson said that while the department could not comment on the content of negotiations, �the talks with the City of Ferguson to develop a monitored consent decree have been productive.�
Iverson added that �an agreement needs to be reached without delay.�
Ferguson Mayor James Knowles III said the two sides had made �a tremendous amount of progress from where we started � we were worlds apart.� When reached, an agreement will be filed in federal court.
Ferguson, a mostly black suburb of St. Louis, became the focus of national attention after sometimes violent protests following Brown�s shooting by a white police officer in August 2014. The officer was not charged.
Brown�s death was one of a series of highly publicized killings of black men mostly by white police officers that have set off a national debate about the proper use of police force, especially against minorities. The Justice Department is also investigating the police departments of Baltimore and Chicago.
Knowles said there were still some outstanding points that needed to be worked out, but the two sides agreed in principle on many issues, such as community policing reforms.
The Justice Department issued a scathing report earlier this year that documented discriminatory actions by Ferguson police and the community�s municipal court system.
Knowles said the city has already made many reforms to its police and court system, including requiring all officers to use body cameras.
Donald McCullin, a municipal judge appointed this past June, has ordered sweeping changes in court practices, including creating alternatives to fines for people who struggle to pay. McCullin also lifted the threat of jail for minor traffic violations.
Knowles said a consultant has been working with police on a community-oriented policing strategy. That means getting officers out of their cars, and going to neighborhood events and meetings, as well as having some officers focused on specific problems, Knowles said.
�Ultimately a lot of that stuff that will end up in the consent decree, we already have been doing,� said Knowles.
Citing officials, the New York Times reported that the agreement would require new training for police officers and improved record keeping. (http://nyti.ms/1m84nIm)
(Reporting by Julia Edwards in Washington, D.C. and Mary Wisniewski in Chicago; Editing by Cynthia Osterman and Bill Rigby)
