Search for:
Oregon sheriff�s corporal charged with coercing sex from colleague
Jonathan Christensen, a former Corporal at the Washington County Sheriff's Office is shown in this handout photo provided by the Washington Country Sheriff's Office in Portland, Oregon, December 13, 2015. REUTERS/Washington County Sheriff's Office/Handout via Reuters
December 14, 2015
By Shelby Sebens
PORTLAND, Ore. (Reuters) � A former sheriff�s corporal in Oregon was arrested on Sunday on charges of coercion, assault and other crimes over allegations that he forced a co-worker into continuing a sexual relationship, authorities said.
Jonathan Christensen, who spent 16 years at the Washington County Sheriff�s Office in Oregon, turned himself in to authorities on Sunday, the sheriff�s office said.
A Washington County Grand Jury indicted Christensen on Friday on charges of coercion, official misconduct, assault and strangulation, the sheriff�s office said.
Christensen was fired from the sheriff�s office in August after a months-long investigation into whether he had engaged in misconduct, the sheriff�s office said.
In his termination letter to Christensen, Sheriff Pat Garrett said evidence showed he had posed a threat to his then-colleague, at one point pushing the woman up against a wall, grabbing her hair and neck and making her promise to continue a sexual relationship.
Christensen was also served with a restraining order in June after the woman alleged he had physically abused her.
An attorney representing Christensen did not immediately respond to a request for comment.
(Editing by Eric M. Johnson; Editing by Paul Tait)
