Search for:
U.S. cracks down on Americans� intake of sugar, saturated fat
An overweight woman sits on a chair in Times Square in New York, May 8, 2012. REUTERS/Lucas Jackson
January 7, 2016
NEW YORK (Reuters) � The U.S. government issued new dietary guidelines Thursday, saying Americans should curb their intake of sugar and saturated fat to less than 10 percent of daily calories, in the latest blow to a sugar industry that has faced heightened criticism by health advocates.
The push to encourage Americans to eat less added sugar as they consume more fruit and vegetables marks a shift, as the U.S. government had previously offered more vague recommendations on limiting sugar consumption.
If followed, the advice would translate to a sharp reduction in consumption of sugary drinks, snacks and sweets for many Americans. That�s especially true for teenagers age 14-18, who on average consume about 17 percent of their calories in added sugar, according to the 2015-2020 Dietary Guidelines for Americans issued by the U.S. Department of Health and Human Services and the U.S. Department of Agriculture.
The 2015-2020 Dietary Guidelines for Americans also advises people aged 14 and younger to consume less than 2,300 milligrams of sodium per day.
Health advocates had mixed views on the new restrictions, with several lauding them.
�If Americans ate according to that advice, it would be a huge win for the public�s health,� said Michael F. Jacobson, president of the Center for Science in the Public Interest.
The guidelines are updated every five years and aim to reduce obesity and prevent chronic diseases like Type 2 diabetes, hypertension and heart disease, the government said in a statement.
(Reporting by Caroline Humer and Chris Prentice; Editing by Bernadette Baum)
