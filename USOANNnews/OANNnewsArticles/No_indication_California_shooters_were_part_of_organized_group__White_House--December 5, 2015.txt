Search for:
No indication California shooters were part of organized group: White House
Law enforcement officers look over the evidence near the remains of a SUV involved in the Wednesday's attack is shown in San Bernardino, California, December 3, 2015. REUTERS/Mario Anzuoni
December 5, 2015
WASHINGTON (Reuters) � President Barack Obama�s team has not yet found evidence that the shooters in the California shooting were part of an organized group or a broader terrorist cell, the White House said in a statement.
�The President�s team also affirmed that they had as of yet uncovered no indication the killers were part of an organized group or formed part of a broader terrorist cell,� the statement said.
The statement added, however, that the team had highlighted �several pieces� of information that �point to the perpetrators being radicalized to violence.�
The statement said the President met in the morning with FBI Director James Comey, Attorney General Loretta Lynch, and Secretary of Homeland Security Jen Johnson.
(Reporting by Idrees Ali; Editing by Chris Reese)
