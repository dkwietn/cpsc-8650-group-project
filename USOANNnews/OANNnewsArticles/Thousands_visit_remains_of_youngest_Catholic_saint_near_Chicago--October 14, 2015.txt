Search for:
Thousands visit remains of youngest Catholic saint near Chicago
The Knights of Columbus, a lay Catholic men's group, stand guard over the remains of Maria Goretti, an 11-year-old Italian girl who is the youngest Roman Catholic saint in a ceremony officiated by Father Artur Sowa in Orland Park, Illinois October 14, 2015. REUTERS/Nikitta Foston
October 14, 2015
By Nikitta Foston
ORLAND PARK, Ill. (Reuters) � Thousands gathered at a church in suburban Chicago on Wednesday to visit a display holding the remains of Maria Goretti, an 11-year-old Italian girl who became the Roman Catholic Church�s youngest saint.
The skeleton of Goretti, who was stabbed to death during an attempted rape in 1902, was encased in a wax statue lying inside a clear glass coffin. Viewers waited for hours to approach the display, and many were moved to tears. Goretti is seen as a symbol of forgiveness and mercy in a violent world.
The display at the St. Francis of Assisi church in Orland Park, southwest of Chicago, is part of a first-ever visit to the United States, and will take in 16 states. The remains also drew huge crowds at a viewing in Chicago on Monday.
Visitor Kathy McNolte, 43, said Goretti was an example of how violence could be met with mercy � a message she said was especially needed in Chicago, which saw 435 homicides in 2014, according to the Chicago Tribune.
�She showed mercy and we need to show that,� said McNolte, who has three sons. �I�m here for my boys. They need someone to emulate. We need people who are kind and loving and forgiving to look up to.�
Goretti was stabbed 14 times near Anzio, Italy, but asked forgiveness for her 20-year-old attacker before her death, according to the church.
�I forgive Alessandro Serenelli � and I want him with me in heaven forever,� she said in the hospital. After Serenelli�s release from prison, he became a lay brother in a monastery.
The tour is a prelude to the �Year of Mercy,� set to begin Dec. 8. It was declared by Pope Francis, leader of the world�s 1.2 billion Catholics.
The display arrived early on Wednesday in a motorcade escorted by the Knights of Columbus, a Catholic lay group.
�Her presence here reaffirms the Christian faith in that there is forgiveness,� said Patrick Allen, national coordinator for the tour. �She endured such agony and pain and yet had mercy.�
Visitor Lorraine Kowalkowski said that while she was glad to see Goretti, violence in the city would not stop without parents teaching �care and respect� to their children.
(Reporting by Nikitta Foston; Editing by Mary Wisniewski and Peter Cooney)
