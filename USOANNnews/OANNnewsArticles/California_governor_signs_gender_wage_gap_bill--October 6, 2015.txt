Search for:
California governor signs gender wage-gap bill
California Governor Jerry Brown speaks during a news conference at the State Capitol in Sacramento, California March 19, 2015. REUTERS/Max Whittaker
October 6, 2015
By Curtis Skinner
SAN FRANCISCO (Reuters) � California Governor Jerry Brown signed legislation on Tuesday aimed at closing the wage gap between men and women, a law that supporters say is among the strongest in the country.
Brown signed the California Fair Pay Act in the San Francisco Bay Area city of Richmond at the Rosie the Riveter National Historic Park, which honors women who worked in factories during World War Two.
�The inequities that have plagued our state and have burdened women forever are slowly being resolved with this kind of bill,� Brown, a Democrat, said at the signing event.
The bill, authored by Democratic state Senator Hannah-Beth Jackson, prohibits employees from facing retaliation for discussing their pay rates at work.
It also allows workers to challenge disparities in pay between people doing similar jobs or doing the same jobs at different work sites for the same company.
�Today is a momentous day for California, and it is long overdue. Equal pay isn�t just the right thing for women, it�s the right thing for our economy and for California,� Jackson said.
Brown�s office said in a statement the bill was �among the strongest in the nation� and received bipartisan support.
The California Chamber of Commerce initially opposed the bill, but the business group said last week it ultimately came around because the legislation created a �fair balance� for workers and employers.
In 2013, a woman working full time in California made about 84 cents for every dollar a man earned, according to Equal Rights Advocates, a gender justice group. Disparities were particularly stark for Latina and African-American women, according to the group.
It said roughly 1.75 million households in California are headed by women, adding that the wage gap between the sexes costs families in the state some $39 billion annually.
�The win here is undeniable. We think of 2015 as the year of fair pay,� Equal Rights Advocates Executive Director Noreen Farrell said in a statement.
The bill was one of a package of reforms pushed by the state�s Legislative Women�s Caucus, which also aimed to make workplace scheduling more accommodating to families and increase aid to infants and children. Those bills have not been passed.
President Barack Obama�s administration has also made gender pay discrimination a priority, signing in 2009 the Lilly Ledbetter Fair Pay Act and taking executive actions on the issue last year.
(Reporting by Curtis Skinner; Editing by Dan Whitcomb and Eric Beech)
