Search for:
Sixth body found in areas scorched by massive California wildfires
A burned out truck is seen among scorched trees after the Valley Fire raged near Cobb, California September 14, 2015. REUTERS/Noah Berger
September 23, 2015
By Curtis Skinner
SAN FRANCISCO (Reuters) � Authorities have discovered the body of a sixth person who died in areas ravaged by a pair of devastating Northern California wildfires, officials said on Wednesday.
The body, which was found on Tuesday evening in the fire-ravaged town of Cobb, is presumed to be 66-year-old Robert Taylor Fletcher, the Lake County Sheriff�s Office said in a statement that did not give further details.
One person, 61-year-old Robert Edward Litchman, remained missing on Wednesday, according to authorities.
Cobb is among the communities hardest hit by the so-called Valley Fire, which has scorched 76,067 acres (28,355 hectares) across Lake, Napa and Sonoma counties, about 70 miles (110 km) north of San Francisco.
Containment of the blaze, a measure of how much of its perimeter has been enclosed within buffer lines carved through vegetation by ground crews, stood at 80 percent on Wednesday, according to the California Department of Forestry and Fire Protection (Cal Fire).
Three other people have been killed in the Valley Fire, including an elderly disabled woman who was trapped in her home.
U.S. President Barack Obama on Tuesday signed a disaster declaration to make federal aid available to residents impacted by the fire.
Authorities said two people were killed by the 70,868-acre (28,679-hectare) Butte Fire raging in the historic Gold Rush country of the Sierra Nevada foothills about 115 miles (185 km) to the southeast. That blaze was 84 percent contained on Wednesday, Cal Fire said.
Together the two fires, which rank as the most destructive among thousands of blazes that have raged across the drought-stricken western United States this summer, displaced some 23,000 people and leveled thousands of homes and other buildings.
The current fire season has been especially intense, as California struggles through its fourth year of a devastating drought.
Authorities on Wednesday said a wildfire that started on Saturday in the Carmel Valley of Monterey County, 125 miles (200 km) south of San Francisco, was sparked by a person committing suicide.
No further details were available on the death, the only one linked with the blaze so far.
That wildfire, the so-called Tassajara Fire, has blackened more than 1,000 acres (400 hectares) and destroyed or damaged about two dozen homes and outbuildings. Cal Fire said it was 81 percent contained as of Wednesday.
(Reporting by Curtis Skinner in San Francisco; Editing by Dan Whitcomb and Eric Beech)
