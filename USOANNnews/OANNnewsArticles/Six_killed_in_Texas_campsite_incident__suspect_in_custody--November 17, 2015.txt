Search for:
Six killed in Texas campsite incident, suspect in custody
William Hudson is shown in this booking photo provided by Anderson County Sheriff's Office in Palestine, Texas. REUTERS/Anderson County Sheriff's Office
November 17, 2015
AUSTIN, Texas (Reuters) � Six people were killed over the weekend at an east Texas campsite and a suspect has been taken into custody for suspected murder, the Anderson County Sheriff�s office said on Monday.
A man and a woman were found dead in a travel trailer at the crime scene near Palestine, about 170 miles northeast of Austin.
The bodies of four men were found on Monday in a pond behind the suspect�s residence, the office said. The four were thought to have been at the campsite over the weekend and were initially considered missing.
No motive has been released for the incident. The names of those killed have not yet been made public.
William Hudson, 33, was arrested without incident on Sunday and charged with one count of murder. Bond has been set at $2.5 million, it said.
The victims were members of two different families from out of the area who were staying in a travel trailer belonging to one of the victims, local TV broadcaster KYTX reported.
The suspect is not related to the victims of the incident that took place in the campsite northwest of Palestine, it said.
(Reporting by Jon Herskovitz; Additional reporting by Lisa Maria Garza in Dallas; Editing by Dan Grebler and James Dalgleish)
