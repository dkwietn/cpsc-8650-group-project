Search for:
McCarthy seeks to convince US conservatives he�s not like Boehner
House Majority Leader Kevin McCarthy (R-CA) speaks at the John Hay Initiative in Washington September 28, 2015. REUTERS/Gary Cameron
October 7, 2015
WASHINGTON (Reuters) � U.S. House of Representatives Majority Leader Kevin McCarthy sought to convince conservative lawmakers on Tuesday that he will be different from retiring Speaker John Boehner if McCarthy is elected as Boehner�s successor this month.
�I think McCarthy�s pitch was, �I�m not John Boehner, I�m going to run things differently, I�m my own man�,� Representative Blake Farenthold, a Texas Republican, told reporters after House conservatives interviewed McCarthy and two other contenders to be the next speaker.
The others were Representatives Jason Chaffetz and Daniel Webster.
McCarthy, a genial Californian, is considered the favorite to replace Boehner, who announced on Sept. 25 he is resigning effective Oct. 30 after a series of battles with conservatives in the House Republican caucus.
But many of the 30 to 50 hardline conservative members who repeatedly fought with the more moderate Boehner are suspicious of McCarthy, who has been part of Boehner�s leadership team since House Republicans got the majority in 2011.
And it was not clear whether McCarthy had convinced conservatives in the Tuesday evening meeting � which included members of the Tea Party and Freedom caucuses � that he could take the House in a different direction.
�To a degree he can. It�s more difficult for him because he�s been there with John Boehner each step of the way,� said Representative Steve King, another conservative who attended the meeting and said he was undecided about who to support.
King said McCarthy, who was majority whip before he was majority leader, knows each one of the members and has built relationships with every member of the conference. �That�s a pretty good tool to have.�
On Thursday, House Republicans will meet privately to choose their party�s nominee for speaker. The entire House � both Republicans and Democrats � will then vote in an open session three weeks later, on Oct. 29.
Republican divisions could make it difficult for any one candidate to win the majority in the House. Conservative Republicans do not have the numbers to elect their choice, but they may be able to block candidates they oppose.
Chaffetz, chairman of the Oversight committee, said he thought he might have won some supporters at the meeting Tuesday by speaking about �how we should fundamentally change the way we do business around here.�
He said conservatives were most concerned about �making sure all members are valued� and being respected, even if they vote differently.
(Reporting by Susan Cornwell; Editing by Robert Birsel)
