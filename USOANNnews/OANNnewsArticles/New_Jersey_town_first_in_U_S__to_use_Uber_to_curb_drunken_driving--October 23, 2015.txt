Search for:
New Jersey town first in U.S. to use Uber to curb drunken driving
An Uber sign is seen in a car in New York June 30, 2015. REUTERS/Eduardo Munoz
October 23, 2015
By Katie Reilly
NEW YORK (Reuters) � A New Jersey town on track for a record-breaking number of drunken driving cases has become the first U.S. municipality to partner with ride service app Uber [UBER.UL] to keep inebriated residents from getting behind the wheel.
To keep the roads safe in Evesham Township, a town of 45,000 people in southern New Jersey, anyone drinking in at least 19 alcohol-serving establishments can now get a free ride home from Uber in a program funded by donors and started last week.
Donations from area nonprofits and businesses are also funding a second free ride option that started on Friday: the mobile app BeMyDD, through which people can hire a driver to get both them and their car home.
�We�re dealing with people who might�ve had too much to drink, so we needed to make it so easy for them to open their iPhone and push a button,� Evesham Mayor Randy Brown said.
Evesham had been on track to reach 250 DUI arrests in 2015, a record for the town, Brown said.
The effort extends a pilot program tested during September, when town shuttles provided free rides to more than 350 people. The shuttles helped decrease DUI arrests to eight in September from a monthly average of 23 from January to August, a drop of 65 percent, Brown said.
Free rides are available from the designated bars and restaurants from 9 p.m. to 2 a.m. every night of the week. The partnership with both apps runs through Jan. 2.
�We began working with Mayor Brown through our national partner, Mothers Against Drunk Driving, and realized it was the perfect opportunity to use our technology to help take drunk drivers off the road,� Ana Mahony, general manager for Uber New Jersey, said in a statement.
Uber is testing the pilot locally and is considering working with other towns to create a similar partnership, a spokesman for the company said.
Alexa Milkovich, vice president of marketing for BeMyDD, said the company recruited drivers quickly to make sure the area would have enough to meet demand.
(Reporting by Katie Reilly)
