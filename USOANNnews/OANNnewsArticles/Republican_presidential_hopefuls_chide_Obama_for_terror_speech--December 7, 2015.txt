Search for:
Republican presidential hopefuls chide Obama for terror speech
U.S. President Barack Obama speaks about counter-terrorism and the United States fight against Islamic State during an address to the nation from the Oval Office of the White House in Washington, December 6, 2015. REUTERS/Yuri Gripas
December 7, 2015
By Ginger Gibson and Steve Holland
WASHINGTON (Reuters) � Republican presidential candidates roundly criticized President Barack Obama�s Sunday night address to the nation about the U.S. response to the threat of terror, arguing that he lacked a vision and was not aggressive enough.
Republicans have become increasingly critical of the president�s handling of Islamic State, or ISIS, arguing that his foreign policy has failed to address the risk from the militant group.
Republicans are also hopeful that by shifting the election discussion toward foreign policy and national security, voters will be more inclined to support their party. Historically, elections that focused on foreign policy and national security boded better for Republicans.
In a rare speech from the Oval Office, Obama warned Americans the terrorist threat to the United States had evolved into a dangerous �new phase� in the aftermath of a deadly California shooting rampage, but vowed to destroy Islamic State and other militant groups.
�President Obama has finally been forced to abandon the political fantasy he has perpetuated for years that the threat of terrorism was receding,� former Florida Governor Jeb Bush said in a statement. �We need to remove the self-imposed constraints President Obama has placed on our intelligence community and military, and we need to put in place an aggressive strategy to defeat ISIS and radical Islamic terrorism as I have proposed.�
After the speech, Republican hopeful Donald Trump tweeted, �Is that all there is? We need a new President � FAST!�
Senator Marco Rubio of Florida criticized Obama for saying that Americans should not fall into a trap of discriminating against Muslims.
�Where is there widespread evidence that we have a problem in America with discrimination against Muslims?� Rubio said on Fox News after the speech. �I think not only did the president not make things better tonight, I fear he may have made things worse in the minds of many Americans.�
Republicans also condemned Obama�s call for stricter gun laws after last week�s shooting rampage. A married couple, who might have been inspired by Islamic State, shot and killed 14 people and wounded 21 in the Southern California city of San Bernardino.
�Let me be clear: disarming more law-abiding citizens will not stop mass murderers and terrorists,� Senator Rand Paul, who is also running for president, said. �We should be advocating for more concealed-carry ability for law-abiding Americans and an end to unconstitutional gun-free zones.�
David Yepsen, director of the Paul Simon Public Policy Institute at Southern Illinois University, said the speech is not likely to silence Obama�s Republican critics.
�I think Democrats are going to be pleased,� Yepsen said. �He�s not going to go far enough to quiet the critics. This was no �day of infamy� speech.�
(Reporting by Ginger Gibson; Editing by Jonathan Oatis)
