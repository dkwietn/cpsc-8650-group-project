Search for:
Bowe Bergdahl arraigned on military charges, mulls options
U.S. Army Sergeant Bowe Berghdal is pictured in this undated handout photo provided by the U.S. Army and received by Reuters on May 31, 2014. REUTERS/U.S. Army/Handout via Reuters
December 22, 2015
By Colleen Jenkins
FORT BRAGG, N.C. (Reuters) � U.S. Army Sergeant Bowe Bergdahl, charged with deserting his combat outpost in Afghanistan before being captured by the Taliban in 2009, sought time on Tuesday to decide whether a military judge or jury of soldiers will decide his legal fate.
Bergdahl, who spent five years as a Taliban prisoner before gaining his release in a prisoner swap in 2014, faces a court-martial after being charged earlier this year with desertion and endangering U.S. troops. The latter offense carries a life sentence if he is convicted.
The soldier�s case has been controversial. Some fellow troops resented the military resources devoted to searching for him. Republicans criticized the Obama administration for the deal that freed him in a prisoner swap with the Taliban.
He was arraigned on Tuesday on the charges but did not enter a plea during a brief hearing at Fort Bragg in North Carolina. A military judge granted Bergdahl, 29, permission to postpone a decision on whether a jury or a judge alone will hear his case.
�The accused wishes to defer for reflection,� said Lieutenant Colonel Franklin Rosenblatt, Bergdahl�s lawyer.
Bergdahl, wearing a blue dress uniform, said little, offering �Yes, sir� or �No, sir� answers to the judge�s questions or allowing his lawyer to speak on his behalf.
Bergdahl is now stationed at Fort Sam Houston in San Antonio, Texas, near the hospital where he has been treated since his release from captivity.
He disappeared on June 30, 2009, from Combat Outpost Mest-Malak in Paktika Province, Afghanistan, and was captured by Taliban forces who subjected him to torture and neglect.
He walked off his post to draw attention to �leadership failure� in his unit, Bergdahl said on the popular podcast Serial.
In ordering the court-martial last week, Army General Robert Abrams did not follow the recommendation of a preliminary hearing officer who, according to Bergdahl�s lawyer, called for him to face a proceeding that could impose a potential maximum penalty of a year in confinement.
Army Colonel Jeffery Nance has been assigned as the judge in Bergdahl�s case. Nance presided over the military�s case against Staff Sergeant Robert Bales, who pleaded guilty to killing 16 Afghan civilians in March 2012.
Bergdahl�s next hearing is set for Jan. 12.
Geoffrey Corn, a retired Army lieutenant colonel who teaches at the South Texas College of Law, said he would not be surprised if a panel or judge decides Bergdahl should not go to prison for his alleged military crimes.
�These are people who are able to sort out the difference between extremely aggravated offenses and offenses committed by people who just make really stupid decisions,� Corn said.
(Additional reporting by Jim Forsyth in San Antonio; Editing by Dan Grebler and Alan Crosby)
