Search for:
Texas woman arrested for �blood ritual� kidnapping
Mercedes Salazar, 32, of San Antonio,Texas is pictured in this undated handout photo obtained by Reuters October 28, 2015. REUTERS/Bexar County Sheriff's Office/Handout via Reuters
October 29, 2015
By Jim Forsyth
SAN ANTONIO (Reuters) � A Texas woman who believed that the girlfriend of her dead brother played a role in his demise is suspected of kidnapping, drugging and performing a �blood ritual� on her, authorities said on Wednesday.
Mercedes Salazar, 32, has been arrested on suspicion of holding the girlfriend, who has not been identified, hostage for three days in a San Antonio home, binding her to a chair with zip ties, drugging her with what is believed to be heroin and subjecting her to a type of witchcraft where blood is drawn.
She is one of four people suspected of participating in the crime, according to James Keith, a spokesman for the Bexar County Sheriff.
Keith said the victim, 25, was rescued when deputies forced their way into the home and found her tied to a chair and being threatened with a knife by another woman, who has been charged in the case.
The decomposing body of Angel Salazar, 28, Mercedes� brother, was found in August in an abandoned apartment in San Antonio. Salazar had been beaten and tortured, and five suspects have been charged.
Sheriff�s investigators say there is no indication that the victim had any role in Salazar�s death, but his sister was apparently so convinced of her involvement that she and the others kidnapped her on Sept. 6 and�forced her into the home where she was held for three days.
When two of the suspects left to run errands three days later, she asked one of her captors to untie her so she could use the bathroom. She then managed to call her mother, who then called authorities, police said.
Salazar was arrested on Sunday�on aggravated kidnapping charges, police said. The three other suspects have also been arrested.
(Writing by Jon Herskovitz; Editing by Eric Walsh)
