Search for:
Zika virus case confirmed in Texas; person traveled to Latin America
Male Aedes albopictus mosquitoes are seen in this picture. Zika virus is among the viruses spead by the species.REUTERS/Ma Qiang/Southern Metropolis Daily
January 12, 2016
(Reuters) � A traveler who recently returned to the Houston area from El Salvador has a confirmed case of Zika, a virus borne by mosquitoes, health officials in Harris County, Texas, said on Tuesday.
The U.S. Centers for Disease Control said it has confirmed 22 cases of the disease among returning U.S. travelers since it was first reported in 2007, and is still receiving specimens for testing from travelers who recently became ill.
There is no indication that mosquitoes in the continental United States are spreading Zika. But in December Puerto Rico, a U.S. territory, confirmed the first locally acquired case of Zika virus in a person who had not traveled outside the island.
The Zika virus has gained attention recently because Brazil is investigating a possible link between the infection and cases of infants born with microcephaly, abnormally small head size associated with incomplete brain development, the CDC said.
Harris County health officials said they were urging travelers to take protective measures against mosquitoes, such as netting and repellent, if they travel to areas where the infection is present.
Zika virus outbreaks have occurred in Africa, Southeast Asia and the Pacific Islands, and have been reported in some countries in the Americas, the CDC said. It is transmitted by Aedes species mosquitoes, which also spread dengue and chikungunya viruses and are common in Texas, Florida and elsewhere in the United States.
Zika is usually a mild illness with fever, rash and joint pain. There is no preventive vaccine, according to the CDC.
(Reporting by Fiona Ortiz in Chicago; Editing by Dan Grebler and Matthew Lewis)
