import sys
import os
import re

def printProgress( current, count ):
	sys.stdout.write("\r" + str(current) + "/" + str(count))
	sys.stdout.flush()

if len(sys.argv) < 3:
	print("usage: " + sys.argv[0] + " inputFolder/" + " outputFile")
	exit()
	
inputFolder = sys.argv[1]
outputFileName = sys.argv[2]

articles = []
	
# process all files
files = os.listdir(inputFolder)
i = 0
for fileName in files:
	try:
		with open(inputFolder + fileName, 'r') as inputFile:
			title = re.search(r"SANITIZED_(.*?)\-\-", fileName)
			time = re.search(r"\-\-(.*?20[0-9][0-9])\.", fileName)
			if time == None or title == None:
				continue
			title = title.group(1).lstrip('_').rstrip('_')
			time = time.group(1)
			data = inputFile.read().replace("\n"," ").rstrip()
			articles.append([time, title, data])
		i += 1
		printProgress( i, len(files) )
	except:
		print("Error with" + fileName)

with open(outputFileName,'w') as outFile:
	for x in articles:
		line = ""
		for y in x:
			line += y + "\t"
		outFile.write(line.rstrip() + "\n")