import sys
import os
import re

if len(sys.argv) < 3:
	print("usage: " + sys.argv[0] +" inputFolder/ outputFolder/")
	exit()
	
inputFolder = sys.argv[1]
outputFolder = sys.argv[2]

# read list of stopwords
stopWords = {}
with open('stop-word-list.txt', 'r') as inFile:
	for stopword in inFile:
		stopWords[stopword.rstrip()] = ""
		
# process all files
files = os.listdir(inputFolder)
i = 0
for fileName in files:
	try:
		inFile = open(inputFolder + fileName, 'r')
		outFile = open(outputFolder + "SANITIZED_" + fileName,'w')
		for line in inFile:
			for word in line.split():
				for wordPart in re.split("[_\-\/]", word):
					outWord = re.sub("[^a-z0-9]","", wordPart.lower())
					if len(outWord) > 1 and outWord not in stopWords:
						outFile.write( outWord )
						outFile.write("\n")
		inFile.close()
		outFile.close()
		i += 1
		sys.stdout.write("\r" + str(i) + "/" + str(len(files)))
		sys.stdout.flush()
	except:
		print("Error with" + fileName)