from nltk.stem import WordNetLemmatizer

def stemWord( word):
	wordnet_lemmatizer = WordNetLemmatizer()
	nword = wordnet_lemmatizer.lemmatize(word, pos='n')
	vword = wordnet_lemmatizer.lemmatize(word, pos='v')
	adjword = wordnet_lemmatizer.lemmatize(word, pos='a')
	adj2word = wordnet_lemmatizer.lemmatize(word, pos='s')
	advword = wordnet_lemmatizer.lemmatize(word, pos='r')
	
	if word != vword:
		rWord = vword
	elif word != nword:
		rWord = nword
	elif word != advword:
		rWord = advword
	elif word != adjword:
		rWord = adjword
	elif word != adj2word:
		rWord = adj2word
	else:
		rWord = word
	
	#if rWord != word:
	#	print(word +" --> " + rWord)
	
	return rWord