import numpy
from sklearn import mixture

def check(clusters):
	clusterNumbers = {}
	for cluster in clusters:
		clusterNumbers[cluster] = clusterNumbers.setdefault( cluster, 0 ) + 1
	print(clusterNumbers)

x = numpy.loadtxt("wordMatrix.txt")
dpgmm = mixture.DPGMM(n_components = 25, covariance_type='spherical',  n_iter=100)
dpgmm.fit(x)
clusters = dpgmm.predict(x)
check(clusters)

numpy.savetxt("clusterOut.txt", clusters)