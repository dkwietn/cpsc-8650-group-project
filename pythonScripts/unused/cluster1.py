from dpcluster import *
data = np.loadtxt("wordMatrix.txt")
vdp = VDP(GaussianNIW(2))
vdp.batch_learn(vdp.distr.sufficient_stats(data))
plt.scatter(data[:,0],data[:,1])
vdp.plot_clusters(slc=np.array([0,1]))
plt.show()