import sys
import csv
import copy

def printProgress( current, count ):
	sys.stdout.write("\r" + str(current) + "/" + str(count))
	sys.stdout.flush()

if len(sys.argv) < 3:
	print("usage: " + sys.argv[0] +" inputFile outputFile")
	exit()

outputWordList = "wordFrequencyDictionary.txt"
inputFileName = sys.argv[1]
outputFileName = sys.argv[2]

# create dictionary
print("Creating Dictionary")
dictionary = {}
i = 0
with open(inputFileName, "r") as inputFile:
	lineReader = csv.reader(inputFile, delimiter = "\t")
	for line in lineReader:
		text = line[3]
		for word in text.split():
			word = word.rstrip()
			dictionary[word] = dictionary.setdefault( word, 0 ) + 1
	i += 1
	printProgress( i, "?" )

# condense dictionary
weightedDictionary = {}
i = 0
print("Condensing Dictionary of length " + str(len(dictionary)))
for key in list(dictionary.keys()):
	if dictionary[key] <= 5:
		del dictionary[key]
	else:
		i += dictionary[key]
		
for key in list(dictionary.keys()):
	weightedDictionary[key] = i/dictionary[key]

print("Dictionary Condensed to length " + str(len(dictionary)))

# create a universal dictionary vector format
print("Creating word frequency list")
wordList = []
with open(outputWordList, "w") as outputFile:
	i = 0
	for key in dictionary.keys():
		key = str(key)
		outputFile.write(key + "\t" + str(dictionary[key]) + "\n")
		dictionary[key] = i
		wordList.append(key)
		i += 1

# Create Matrix
def createVector( text ):
	vector = [1 for x in range(0, len(wordList))]
	for word in text.split():
		if word in dictionary:
			vector[dictionary[word]] += (1 * weightedDictionary[word] * 100)
	return vector

print("Creating Data Matrix")
with open(inputFileName, 'r') as inputFile:
	with open(outputFileName,'w') as outFile:
		i = 0
		lineReader = csv.reader(inputFile, delimiter = "\t")
		lineWriter = csv.writer(outFile, delimiter="\t", lineterminator='\n')
		for line in lineReader:
			vector = createVector(line[3])
			lineWriter.writerow(vector)
			i += 1
			printProgress( i, "?" )
print("finished")