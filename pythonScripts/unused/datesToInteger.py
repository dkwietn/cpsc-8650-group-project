import sys
import os
import re
import datetime
import csv

def printProgress( current, count ):
	sys.stdout.write("\r" + str(current) + "/" + str(count))
	sys.stdout.flush()

if len(sys.argv) < 3:
	print("usage: " + sys.argv[0] + " inputFile" + " outputFile")
	exit()
	
inputFileName = sys.argv[1]
outputFileName = sys.argv[2]

def dateToInteger( time ):
	date = datetime.datetime.strptime(time, "%B %d, %Y")
	return date.strftime("%Y%m%d") #output is year month day
	
with open(inputFileName, 'r') as inputFile:
	with open(outputFileName,'w') as outFile:
		i = 0
		lineReader = csv.reader(inputFile, delimiter = "\t")
		lineWriter = csv.writer(outFile, delimiter=" ", lineterminator='\n')
		for line in lineReader:
			date = dateToInteger(line[0])
			lineWriter.writerow([date] + line)
			i += 1
			printProgress( i, "?" )