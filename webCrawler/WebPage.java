/*
 * David Kwietniewski
 * CpSc 2150
 * Project 01: WebCrawler
 * WebPage
 */

package cu.cs.cpsc215.project1;
import java.io.IOException;
import java.util.Iterator;
import java.util.ListIterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WebPage implements WebElement
{
	private String URL;
	private int depth;
	private DownloadRepository repository;
	private Elements links;
	private Elements imagelist;
	private boolean crawled;

	// simple WebPage Constructor
	public WebPage(String URL)
	{
		this(URL, 1);
	}

	// WebPage constructor
	public WebPage(String URL, int depth)
	{
		//replace spaces in URL with HTML code for space
		this.URL = URL.replaceAll(" ", "%20");
		this.depth = depth;
		repository = DownloadRepository.getInstance();
		crawled = false;
	}

	// parse the WebPage
	public void crawl()
	{
		Document page = null;

		try
		{
			page = Jsoup.connect(URL).get();
			links = page.select("a[href]"); // get all links off web page
			imagelist = page.select("img[src~=(?i)\\.(png|jpeg|jpg|bmp|gif)]"); // get all images off web page
		}
		catch (IOException e)
		{
			System.out.println("Unable to Connect to " + URL);
		}
		catch(Exception e)
		{
			System.out.println("Error Occurred with " + URL);
		}

		crawled = true;
	}

	// add images from WebPage to DownloadRepository
	public void getImages()
	{
		if(imagelist != null)
			for (Element image : imagelist)
			{
				repository.addWebImage(new WebImage(image.attr("abs:src")));
			}
	}

	// add files from WebPage to DownloadRepository
	public void getFiles()
	{
		if(links != null)
		{
			String address;
			int index;
			String extension;

			for(Iterator<Element> link = links.iterator(); link.hasNext();)
			{
				if(link != null)
				{
					address = link.next().attr("abs:href");
					index = address.lastIndexOf('.');
					if(index > 0)
					{
						extension =  address.substring(index+1).toLowerCase();
						if(    extension.equals("gz")
							|| extension.equals("jar")
							|| extension.equals("rar")
							|| extension.equals("zip")
							|| extension.equals("mdb")
							|| extension.equals("doc")
							|| extension.equals("docx")
							|| extension.equals("odt")
							|| extension.equals("pdf")
							|| extension.equals("ppt")
							|| extension.equals("pptx")
							|| extension.equals("wks"))
						{
								repository.addWebFile(new WebFile(address));
								link.remove();
						}
					}
				}
			}
		}
	}

	// add pages from WebPage to DownloadRepository
	public void getWebPages( ListIterator<WebPage> iterator )
	{
		if(links != null)
		{
			String address;
			for (Element link : links)
			{
				address = link.attr("abs:href");
				repository.addWebPage(new WebPage(address,(depth+1)), iterator);
			}
		}
	}

	// implement Save function
	public void Save(String Destination)
	{
		// does not save WebPage objects
		System.out.println("THIS PROGRAM DOES NOT SAVE WEBPAGES");
	}

	// returns depth of WebPage and its URL
	public String toString()
	{
		return "WebPage - Depth = " + depth + ": " + URL;
	}

	// returns depth of WebPage
	public int getDepth()
	{
		return depth;
	}

	// returns whether WebPage was crawled
	public boolean crawled()
	{
		return crawled;
	}

	// returns URL
	public String getURL()
	{
		return URL;
	}

	// check if two WebPages are the Same
	@Override
	public boolean equals(Object element)
	{
		return this.URL.equalsIgnoreCase(((WebElement) element).getURL());
	}

	@Override
	public int hashCode()
	{
		return URL.hashCode();
	}
}