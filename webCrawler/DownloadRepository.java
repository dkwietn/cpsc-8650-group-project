/*
 * David Kwietniewski
 * CpSc 2150
 * Project 01: WebCrawler
 * DownloadRepository
 */

package cu.cs.cpsc215.project1;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.ListIterator;

public class DownloadRepository implements Repository
{
	private static DownloadRepository firstInstance = null;
	private int maxdepth;
	private String SaveDestination;
	private LinkedList<WebImage> images;
	private LinkedList<WebFile> files;
	private LinkedList<WebPage> pages;
	private Hashtable<Integer, WebElement> urlHashtable;

	// DownloadRepository constructor
	private DownloadRepository()
	{
		images = new LinkedList<WebImage>();
		files = new LinkedList<WebFile>();
		pages = new LinkedList<WebPage>();
		urlHashtable = new Hashtable<Integer, WebElement>(1000);
	}

	// returns DownloadRepository Instance
	public static DownloadRepository getInstance()
	{
		if(firstInstance == null)
			firstInstance = new DownloadRepository();
		return firstInstance;
	}

	// set maximum crawl depth and save destination variables
	public void setVariables( int maxdepth, String Destination )
	{
		this.maxdepth = maxdepth;
		SaveDestination = Destination;
	}

	// begins crawling web at specified URL and continues until reaching depth specified
	public void Crawl(String URL)
	{
		//crawl starting page
		WebPage page = new WebPage(URL);
		pages.add(page);
		System.out.println(page);
		page.crawl();
		page.getFiles();
		page.getImages();
		page.getWebPages(pages.listIterator());

		// iterate through all WebPages
		for(ListIterator<WebPage> iterator = pages.listIterator(); iterator.hasNext();)
		{
			page = iterator.next();
			if(!page.crawled())
			{
				page.crawl();
				page.getFiles();
				page.getImages();
				page.getWebPages(iterator);
			}

		}
	}

	// save all images and files found
	public void saveAll()
	{
		System.out.println("Saving Images and Files");

		for (WebElement image : images)
			image.Save(SaveDestination);

		for (WebElement file : files)
			file.Save(SaveDestination);
	}

	// add WebPage to be crawled
	public void addWebPage(WebPage page, ListIterator<WebPage> iterator)
	{
		//check for URL, page depth <= max crawl depth, and page is not already in list of WebPages
		if(!page.getURL().isEmpty() && page.getDepth() <= maxdepth && !urlHashtable.contains(page))
		{
			urlHashtable.put(page.hashCode(), page);
			iterator.add(page);
			iterator.previous();
			System.out.println(page);
		}
	}

	// add WebFile to DownloadRepository
	public void addWebFile(WebFile file)
	{
		//check for URL and make sure file is not already in list of WebFiles
		if( !file.getURL().isEmpty() && !urlHashtable.contains(file))
		{
			urlHashtable.put(file.hashCode(), file);
			files.add(file);
			System.out.println(file);
		}
	}

	// add WebImage to DownloadRepository
	public void addWebImage(WebImage image)
	{
		//check for URL and make sure image is not already in list of WebImages
		if(!image.getURL().isEmpty() && !urlHashtable.contains(image))
		{
			urlHashtable.put(image.hashCode(),image);
			images.add(image);
			System.out.println(image);
		}
	}
}