/*
 * David Kwietniewski
 * CpSc 2150
 * Project 01: WebCrawler
 * Repository
 */

package cu.cs.cpsc215.project1;

import java.util.ListIterator;

public interface Repository
{
	public void setVariables( int maxdepth, String Destination );
	public void Crawl(String URL);
	public void saveAll();
	public void addWebPage(WebPage page, ListIterator<WebPage> iterator);
	public void addWebFile(WebFile file);
	public void addWebImage(WebImage image);
}