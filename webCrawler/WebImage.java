/*
 * David Kwietniewski
 * CpSc 2150
 * Project 01: WebCrawler
 * WebImage
 */

package cu.cs.cpsc215.project1;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

public class WebImage implements WebElement
{
	private String URL;

	// WebImage constructor
	public WebImage( String URL)
	{
		// replace spaces in URL with HTML code for space
		this.URL = URL.replaceAll(" ", "%20");
	}

	// returns URL
	public String toString()
	{
		return "WebImage: " + URL;
	}

	// implement Save function
	public void Save(String Destination)
	{
		String imageName = Destination;
		try
		{
			imageName = Destination + URL.substring(URL.lastIndexOf('/'));
			imageName = imageName.replaceAll("\\?", "_");
			int index = imageName.lastIndexOf('.');
			String extension = imageName.substring(index);
			String name = imageName.substring(0, index);

			// rename to adjust for duplicate file names if necessary
			for(int i = 1; new File(imageName).exists(); i++)
			{
				imageName = name + "_" + i + extension;
			}

			InputStream in= (new URL(URL)).openStream();
			OutputStream out = new BufferedOutputStream(new FileOutputStream(imageName));

			 for (int b; (b = in.read()) != -1;)
				 out.write(b);

			System.out.println("Saved " + imageName);
			in.close();
			out.close();
		}
		catch(StringIndexOutOfBoundsException e)
		{
			System.out.println("Unable to Convert " + URL + " to Image Name");
		}
		catch(IOException e)
		{
			System.out.println("Unable to Save " + URL + " to " + imageName);
		}
		catch(Exception e)
		{
			System.out.println("Error Occurred with " + URL);
		}
	}

	// return URL
	public String getURL()
	{
		return URL;
	}

	// check if two WebImages are the Same
	@Override
	public boolean equals(Object element)
	{
		return this.URL.equalsIgnoreCase(((WebElement)element).getURL());
	}

	@Override
	public int hashCode()
	{
		return URL.hashCode();
	}
}