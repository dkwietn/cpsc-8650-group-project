/*
 * David Kwietniewski
 * CpSc 2150
 * Project 01: WebCrawler
 * WebCrawler
 */

package cu.cs.cpsc215.project1;
import org.jsoup.Jsoup;

public class WebCrawler
{
	public static void main(String[] args)
	{
		if(args.length < 3) // make sure correct number of arguments were supplied
		{
			System.out.println("Provide Web Address, Maximum Crawl Depth, and Save Directory");
			System.exit(1);
		}

		try // make sure arguments are valid
		{
			Jsoup.connect(args[0]).get();
			Integer.parseInt(args[1]);
		}
		catch( NumberFormatException e )
		{
			System.out.println("Unable to Convert " + args[1] + " to Integer");
			System.exit(1);
		}
		catch( Exception e )
		{
			System.out.println("Unable to Connect to " + args[0]);
			System.exit(1);
		}

		// initialize DownloadRepository
		DownloadRepository repository = DownloadRepository.getInstance();
		repository.setVariables( Integer.parseInt(args[1]), args[2]);

		try// crawl WebPages from starting URL then save
		{
			repository.Crawl(args[0]);
		}
		catch (Exception e)
		{
			System.out.println("Unexpected Error Occurred");
			e.printStackTrace();
		}
		finally
		{
			repository.saveAll();
		}
		System.out.println("Done");
	}
}