/*
 * David Kwietniewski
 * CpSc 2150
 * Project 01: WebCrawler
 * WebFile
 */

package cu.cs.cpsc215.project1;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

public class WebFile  implements WebElement
{
	private String URL;

	// WebFile constructor
	public WebFile( String URL)
	{
		// replace spaces in URL with HTML code for space
		this.URL = URL.replaceAll(" ", "%20");
	}

	// returns URL
	public String toString()
	{
		return "WebFile: " + URL;
	}

	// implement Save function
	public void Save(String Destination)
	{
		String fileName = Destination;
		try
		{
			fileName = Destination + URL.substring(URL.lastIndexOf('/'));
			fileName = fileName.replaceAll("\\?", "_");
			String extension = fileName.substring(fileName.lastIndexOf('.'));
			String name = fileName.substring(0, fileName.lastIndexOf('.'));

			// rename to adjust for duplicate file names if necessary
			for(int i = 1; new File(fileName).exists(); i++)
			{
				fileName = name + "_" + i + extension;
			}

			InputStream in = (new URL(URL)).openStream();
			OutputStream out = new BufferedOutputStream(new FileOutputStream(fileName));

			 for (int b; (b = in.read()) != -1;)
			 	         out.write(b);

			 System.out.println("Saved " + fileName);
			in.close();
			out.close();

		}
		catch(StringIndexOutOfBoundsException e)
		{
			System.out.println("Unable to Convert " + URL + " to File Name");
			return;
		}
		catch(IOException e)
		{
			System.out.println("Unable to Save " + URL  + " to " + fileName);
		}
		catch(Exception e)
		{
			System.out.println("Error Occurred with " + URL);
		}
	}

	// return URL
	public String getURL()
	{
		return URL;
	}

	// check if two WebFiles are the Same
	@Override
	public boolean equals(Object element)
	{
		return this.URL.equalsIgnoreCase(((WebElement) element).getURL());
	}

	@Override
	public int hashCode()
	{
		return URL.hashCode();
	}
}