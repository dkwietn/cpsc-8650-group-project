/*
 * David Kwietniewski
 * CpSc 2150
 * Project 01: WebCrawler
 * WebElement
 */

package cu.cs.cpsc215.project1;

public interface WebElement
{
	public void Save( String Destination );
	public String toString();
	public String getURL();
	public boolean equals( Object element );
	public int hashCode();
}